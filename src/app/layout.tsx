import './styles.css';

import { Metadata } from 'next';
import { headers } from 'next/headers';
import ExplorerHeading from 'src/components/header/ExplorerHeading';

import ClientLayout from './clientLayout';

export function generateMetadata(): Metadata {
    const headersList = headers();
    // TODO: THIS IS A TEMPORARILY WORKAROUND FOR THIS ISSUE: https://github.com/vercel/next.js/issues/43704
    const header_url = headersList.get('url') || '';
    return {
        title: 'DerivaDEX | Explorer',
        viewport: {
            width: 'device-width',
            initialScale: 1,
        },
        icons: {
            icon: '/icons/ddx_logo.png',
            shortcut: '/icons/ddx_logo.png',
        },
        openGraph: {
            type: 'website',
            title: 'DerivaDEX | Explorer',
            siteName: 'DerivaDEX | Explorer',
            url: header_url,
            description:
                'The DerivaDEX Explorer allows you to explore and search the DerivaDEX custom L2 network for transactions, addresses, orders, and other market activities taking place on the network',
            images: { url: './assets/default_social_card.png', alt: `Visit ${header_url}` },
        },
        twitter: {
            card: 'summary_large_image',
            site: '@DDX_Official',
            images: './assets/default_social_card.png',
        },
        metadataBase: new URL(header_url),
    };
}

export default function RootLayout({ children }: { children: React.ReactNode }) {
    return (
        <html lang="en" data-theme="dark" style={{ colorScheme: 'dark' }}>
            <body>
                <ClientLayout>
                    <ExplorerHeading />
                    {children}
                </ClientLayout>
            </body>
        </html>
    );
}
