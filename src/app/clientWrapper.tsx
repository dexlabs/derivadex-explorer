'use client';
import { Box, ColorModeScript } from '@chakra-ui/react';

import { Providers } from './providers';

export function ClientWrapper({ children }: { children: React.ReactNode }) {
    return (
        <Box px={{ base: '5vw', sm: '5vw', md: '8vw', lg: '10vw', '2xl': '12vw' }}>
            <ColorModeScript initialColorMode="dark" />
            <Providers>{children}</Providers>
        </Box>
    );
}
