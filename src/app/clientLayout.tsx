'use client';

import { ColorModeScript } from '@chakra-ui/react';
import { usePathname } from 'next/navigation';
import { useEffect, useState } from 'react';
import ReactGA from 'react-ga4';
import { getDeploymentEnvironmentName } from 'src/data/utils';
import { useNavigationEvent } from 'src/hooks/useNavigationEvent';

import { Providers } from './providers';

// Chakra Provider requires to use client context
export default function ClientLayout({ children }: { children: React.ReactNode }) {
    const [isAnalyticsEnabled, setIsAnalyticsEnabled] = useState(false);
    const pathname = usePathname();
    useNavigationEvent((path) => {
        if (isAnalyticsEnabled) {
            ReactGA.send({ hitType: 'pageview', page: path });
        }
    });

    useEffect(() => {
        const googleAnalyticsID = process.env.NEXT_PUBLIC_GOOGLE_ANALYTICS_ID;
        const environment = getDeploymentEnvironmentName(location.host);
        if (
            !googleAnalyticsID ||
            (environment !== 'mainnet' &&
                environment !== 'staging' &&
                environment !== 'testnet' &&
                environment !== 'beta')
        ) {
            return;
        }
        setIsAnalyticsEnabled(true);

        ReactGA.initialize(googleAnalyticsID);
        ReactGA.send({ hitType: 'pageview', page: pathname });
    }, []);

    return (
        <>
            <ColorModeScript initialColorMode="dark" />
            <Providers>{children}</Providers>
        </>
    );
}
