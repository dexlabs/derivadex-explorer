import { headers } from 'next/headers';
import { Suspense } from 'react';
import RecentTransactionsWidget from 'src/components/views/dashboard/RecentTransactionsWidget';
import WidgetSkeleton from 'src/components/widgets/WidgetSkeleton';
import WidgetTabbedContainer from 'src/components/widgets/WidgetTabbedContainer';
import { getRealtimeApiURLWithToken, getStatsApiURLWithToken } from 'src/utils/actions';

import MarketsWidget from '../components/views/dashboard/MarketsWidget';
import Top20TradersByVolumeTableWidget from '../components/views/dashboard/Top20TradersByVolumeTableWidget';
import WidgetFlexContainer from '../components/widgets/WidgetFlexContainer';
import { ClientWrapper } from './clientWrapper';

// Dashboard Page
export default async function Page() {
    const headersList = headers();
    const host = headersList.get('host') || '';
    // Used for the previous tx log widget feed. Not being used now.
    //const websocketUrl = await getRealtimeApiURLWithToken(host);
    //const txLogUrl = await getStatsApiURLWithToken('/tx_logs?order=desc&limit=20', host);
    return (
        <ClientWrapper>
            <WidgetFlexContainer>
                <Suspense fallback={<WidgetSkeleton />}>
                    {/* @ts-expect-error Server Component */}
                    <RecentTransactionsWidget />
                </Suspense>
            </WidgetFlexContainer>
            <WidgetTabbedContainer
                tabNames={['Markets', 'Top Traders']}
                tabPanels={[
                    <Suspense fallback={<WidgetSkeleton />}>
                        {/* @ts-expect-error Server Component */}
                        <MarketsWidget />
                    </Suspense>,
                    <Suspense fallback={<WidgetSkeleton />}>
                        {/* @ts-expect-error Server Component */}
                        <Top20TradersByVolumeTableWidget />
                    </Suspense>,
                ]}
            />
        </ClientWrapper>
    );
}
