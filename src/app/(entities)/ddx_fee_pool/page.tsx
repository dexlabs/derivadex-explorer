import { Suspense } from 'react';
import { ClientWrapper } from 'src/app/clientWrapper';
import ViewHeading from 'src/components/ViewHeading';
import DDXFeePoolWidget from 'src/components/views/ddx_fee_pool';
import DDXFeePoolHistoryWidget from 'src/components/views/ddx_fee_pool/DDXFeePoolHistory';
import WidgetFlexContainer from 'src/components/widgets/WidgetFlexContainer';
import WidgetSkeleton from 'src/components/widgets/WidgetSkeleton';
import WidgetTabbedContainer from 'src/components/widgets/WidgetTabbedContainer';

export default async function Page() {
    return (
        <ClientWrapper>
            <ViewHeading title={'DDX Fee Pool View'} />
            <WidgetFlexContainer>
                <Suspense fallback={<WidgetSkeleton title={'DDX Fee Pool Details'} />}>
                    {/* @ts-expect-error Server Component */}
                    <DDXFeePoolWidget />
                </Suspense>
            </WidgetFlexContainer>
            <WidgetTabbedContainer
                tabNames={['DDX Fee Pool History']}
                tabPanels={[
                    <Suspense fallback={<WidgetSkeleton />}>
                        {/* @ts-expect-error Server Component */}
                        <DDXFeePoolHistoryWidget />
                    </Suspense>,
                ]}
            />
        </ClientWrapper>
    );
}
