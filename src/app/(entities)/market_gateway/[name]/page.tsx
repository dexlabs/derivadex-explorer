import { Metadata } from 'next';
import { headers } from 'next/headers';
import { Suspense } from 'react';
import { ClientWrapper } from 'src/app/clientWrapper';
import ViewHeading from 'src/components/ViewHeading';
import MarketGatewayWidget from 'src/components/views/market/MarketGatewayWidget';
import WidgetSkeleton from 'src/components/widgets/WidgetSkeleton';

type Props = {
    params: { name: string };
};

export async function generateMetadata({ params }: Props): Promise<Metadata> {
    const headersList = headers();
    const header_url = headersList.get('url') || '';
    const host = headersList.get('host') || '';
    const { name } = params;
    const imageURL = `/api/open_graph/market_gateway/${name}`;
    const currentTime = Date.now();
    return {
        title: `${name} Market Gateway - DerivaDEX | Explorer`,
        openGraph: {
            type: 'website',
            title: 'Market Gateway Details - DerivaDEX | Explorer',
            siteName: 'DerivaDEX | Explorer',
            url: header_url,
            description: `Detailed information about the ${name} market gateway.`,
            images: `https://${host}${imageURL}?nonce=${currentTime}`,
        },
        twitter: {
            card: 'summary_large_image',
            site: '@DDX_Official',
        },
    };
}

export default async function Page({ params }: Props) {
    const source_name = params.name.replace(/\_/g, '.');
    return (
        <ClientWrapper>
            <ViewHeading title={`Market Gateway View: ${source_name}`} />
            <Suspense fallback={<WidgetSkeleton title={'Market Details'} />}>
                {/* @ts-expect-error Server Component */}
                <MarketGatewayWidget name={source_name} />
            </Suspense>
        </ClientWrapper>
    );
}
