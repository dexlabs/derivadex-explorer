import { shortenHex, statsApiListFetcher } from '@utils';
import { Metadata } from 'next';
import { headers } from 'next/headers';
import { Suspense } from 'react';
import { ClientWrapper } from 'src/app/clientWrapper';
import ViewHeading from 'src/components/ViewHeading';
import TxAdlTableWidget from 'src/components/views/transaction/TxAdlTableWidget';
import TxFillDetailsWidget from 'src/components/views/transaction/TxFillDetailsWidget';
import TxFillMakersWidget from 'src/components/views/transaction/TxFillMakersWidget';
import TxLiquidatedStrategiesTableWidget from 'src/components/views/transaction/TxLiquidatedStrategiesTableWidget';
import TxLiquidationDetailsWidget from 'src/components/views/transaction/TxLiquidationDetailsWidget';
import TxOrderCancelDetailsWidget from 'src/components/views/transaction/TxOrderCancelDetailsWidget';
import TxOrderCancelWidget from 'src/components/views/transaction/TxOrderCancelWidget';
import TxOverviewWidget from 'src/components/views/transaction/TxOverviewWidget';
import TxPostDetailsWidget from 'src/components/views/transaction/TxPostDetailsWidget';
import TxPricesTableWidget from 'src/components/views/transaction/TxPricesTableWidget';
import TxStrategiesTableWidget from 'src/components/views/transaction/TxStrategiesTableWidget';
import TxStrategyUpdateDetailsWidget from 'src/components/views/transaction/TxStrategyUpdateDetailsWidget';
import TxTradersTableWidget from 'src/components/views/transaction/TxTradersTableWidget';
import TxTraderUpdateDetailsWidget from 'src/components/views/transaction/TxTraderUpdateDetailsWidget';
import ErrorWidget from 'src/components/widgets/ErrorWidget';
import WidgetFlexContainer from 'src/components/widgets/WidgetFlexContainer';
import WidgetSkeleton from 'src/components/widgets/WidgetSkeleton';
import WidgetTabbedContainer from 'src/components/widgets/WidgetTabbedContainer';
import { ParamEventTypes, TxLogResponseItem } from 'src/types/api';

type Props = {
    params: { epochId: string; txOrdinal: string };
};

export async function generateMetadata({ params }: Props): Promise<Metadata> {
    const headersList = headers();
    const header_url = headersList.get('url') || '';
    const host = headersList.get('host') || '';
    const { epochId, txOrdinal } = params;
    const imageURL = `/api/open_graph/epoch/${epochId}/txOrdinal/${txOrdinal}`;
    const currentTime = Date.now();
    return {
        title: `Tx Ordinal ${txOrdinal}, epoch ID ${epochId} - DerivaDEX | Explorer`,
        openGraph: {
            type: 'website',
            title: 'Transaction Details - DerivaDEX | Explorer',
            siteName: 'DerivaDEX | Explorer',
            url: header_url,
            description: `Detailed information about a transaction for epoch ID ${epochId}, tx ordinal ${txOrdinal}.`,
            images: `https://${host}${imageURL}?nonce=${currentTime}`,
        },
        twitter: {
            card: 'summary_large_image',
            site: '@DDX_Official',
        },
    };
}

function getTxDetailsWidget(txLogItem: TxLogResponseItem) {
    switch (txLogItem.eventKind) {
        case ParamEventTypes.PARTIAL_FILL:
        case ParamEventTypes.COMPLETE_FILL:
            return (
                /* @ts-expect-error Server Component*/
                <TxFillDetailsWidget
                    epochId={txLogItem.epochId}
                    txOrdinal={txLogItem.txOrdinal}
                    event={txLogItem.eventKind}
                />
            );
        case ParamEventTypes.CANCEL:
        case ParamEventTypes.CANCEL_ALL:
            return (
                /* @ts-expect-error Server Component*/
                <TxOrderCancelDetailsWidget
                    epochId={txLogItem.epochId}
                    txOrdinal={txLogItem.txOrdinal}
                    event={txLogItem.eventKind}
                />
            );
        case ParamEventTypes.POST:
            return (
                /* @ts-expect-error Server Component*/
                <TxPostDetailsWidget
                    epochId={txLogItem.epochId}
                    txOrdinal={txLogItem.txOrdinal}
                    event={txLogItem.eventKind}
                />
            );
        case ParamEventTypes.STRATEGY_UPDATE:
            return (
                /* @ts-expect-error Server Component*/
                <TxStrategyUpdateDetailsWidget
                    epochId={txLogItem.epochId}
                    txOrdinal={txLogItem.txOrdinal}
                    event={txLogItem.eventKind}
                />
            );
        case ParamEventTypes.TRADER_UPDATE:
            return (
                /* @ts-expect-error Server Component*/
                <TxTraderUpdateDetailsWidget
                    epochId={txLogItem.epochId}
                    txOrdinal={txLogItem.txOrdinal}
                    event={txLogItem.eventKind}
                />
            );
        case ParamEventTypes.WITHDRAW:
            return (
                /* @ts-expect-error Server Component*/
                <TxStrategyUpdateDetailsWidget
                    epochId={txLogItem.epochId}
                    txOrdinal={txLogItem.txOrdinal}
                    event={txLogItem.eventKind}
                />
            );
        case ParamEventTypes.WITHDRAW_DDX:
            return (
                /* @ts-expect-error Server Component*/
                <TxTraderUpdateDetailsWidget
                    epochId={txLogItem.epochId}
                    txOrdinal={txLogItem.txOrdinal}
                    event={txLogItem.eventKind}
                />
            );
        case ParamEventTypes.LIQUIDATION:
            return (
                /* @ts-expect-error Server Component*/
                <TxLiquidationDetailsWidget
                    epochId={txLogItem.epochId}
                    txOrdinal={txLogItem.txOrdinal}
                    event={txLogItem.eventKind}
                />
            );
        case ParamEventTypes.PRICE_CHECKPOINT:
        case ParamEventTypes.PNL_SETTLEMENT:
        case ParamEventTypes.FUNDING:
        case ParamEventTypes.TRADE_MINING:
            return null;
        default:
            return null;
    }
}

function getTxAffectedWidget(txLogItem: TxLogResponseItem) {
    switch (txLogItem.eventKind) {
        case ParamEventTypes.PARTIAL_FILL:
        case ParamEventTypes.COMPLETE_FILL:
            return {
                tabNames: ['Makers', 'Prices'],
                tabPanels: [
                    <Suspense fallback={<WidgetSkeleton />}>
                        {/* @ts-expect-error Server Component */}
                        <TxFillMakersWidget
                            epochId={txLogItem.epochId}
                            txOrdinal={txLogItem.txOrdinal}
                            event={txLogItem.eventKind}
                        />
                    </Suspense>,
                    <Suspense fallback={<WidgetSkeleton />}>
                        {/* @ts-expect-error Server Component */}
                        <TxPricesTableWidget epochId={txLogItem.epochId} txOrdinal={txLogItem.txOrdinal} />
                    </Suspense>,
                ],
            };
        case ParamEventTypes.CANCEL:
        case ParamEventTypes.CANCEL_ALL:
            return {
                tabNames: ['Cancelled Orders'],
                tabPanels: [
                    <Suspense fallback={<WidgetSkeleton />}>
                        {/* @ts-expect-error Server Component */}
                        <TxOrderCancelWidget
                            epochId={txLogItem.epochId}
                            txOrdinal={txLogItem.txOrdinal}
                            event={txLogItem.eventKind}
                        />
                    </Suspense>,
                ],
            };
        case ParamEventTypes.POST:
            return {
                tabNames: ['Prices'],
                tabPanels: [
                    <Suspense fallback={<WidgetSkeleton />}>
                        {/* @ts-expect-error Server Component */}
                        <TxPricesTableWidget epochId={txLogItem.epochId} txOrdinal={txLogItem.txOrdinal} />
                    </Suspense>,
                ],
            };
        case ParamEventTypes.LIQUIDATION:
            return {
                tabNames: ['Liquidated Strategies', 'Makers', 'Auto-Deleveraging', 'Prices'],
                tabPanels: [
                    <Suspense fallback={<WidgetSkeleton />}>
                        {/* @ts-expect-error Server Component */}
                        <TxLiquidatedStrategiesTableWidget
                            epochId={txLogItem.epochId}
                            txOrdinal={txLogItem.txOrdinal}
                            event={txLogItem.eventKind}
                        />
                    </Suspense>,
                    <Suspense fallback={<WidgetSkeleton />}>
                        {/* @ts-expect-error Server Component */}
                        <TxFillMakersWidget
                            epochId={txLogItem.epochId}
                            txOrdinal={txLogItem.txOrdinal}
                            event={txLogItem.eventKind}
                        />
                    </Suspense>,
                    <Suspense fallback={<WidgetSkeleton />}>
                        {/* @ts-expect-error Server Component */}
                        <TxAdlTableWidget
                            epochId={txLogItem.epochId}
                            txOrdinal={txLogItem.txOrdinal}
                            event={txLogItem.eventKind}
                        />
                    </Suspense>,
                    <Suspense fallback={<WidgetSkeleton />}>
                        {/* @ts-expect-error Server Component */}
                        <TxPricesTableWidget epochId={txLogItem.epochId} txOrdinal={txLogItem.txOrdinal} />
                    </Suspense>,
                ],
            };
        case ParamEventTypes.STRATEGY_UPDATE:
        case ParamEventTypes.TRADER_UPDATE:
        case ParamEventTypes.WITHDRAW:
        case ParamEventTypes.WITHDRAW_DDX:
        case ParamEventTypes.PRICE_CHECKPOINT:
            return {
                tabNames: ['Prices'],
                tabPanels: [
                    <Suspense fallback={<WidgetSkeleton />}>
                        {/* @ts-expect-error Server Component */}
                        <TxPricesTableWidget epochId={txLogItem.epochId} txOrdinal={txLogItem.txOrdinal} />
                    </Suspense>,
                ],
            };
        case ParamEventTypes.PNL_SETTLEMENT:
            return {
                tabNames: ['Strategies', 'Prices'],
                tabPanels: [
                    <Suspense fallback={<WidgetSkeleton />}>
                        {/* @ts-expect-error Server Component */}
                        <TxStrategiesTableWidget
                            epochId={txLogItem.epochId}
                            txOrdinal={txLogItem.txOrdinal}
                            event={txLogItem.eventKind}
                        />
                    </Suspense>,
                    <Suspense fallback={<WidgetSkeleton />}>
                        {/* @ts-expect-error Server Component */}
                        <TxPricesTableWidget epochId={txLogItem.epochId} txOrdinal={txLogItem.txOrdinal} />
                    </Suspense>,
                ],
            };
        case ParamEventTypes.FUNDING:
            return {
                tabNames: ['Strategies'],
                tabPanels: [
                    <Suspense fallback={<WidgetSkeleton />}>
                        {/* @ts-expect-error Server Component */}
                        <TxStrategiesTableWidget
                            epochId={txLogItem.epochId}
                            txOrdinal={txLogItem.txOrdinal}
                            event={txLogItem.eventKind}
                        />
                    </Suspense>,
                ],
            };
        case ParamEventTypes.TRADE_MINING:
            return {
                tabNames: ['Traders'],
                tabPanels: [
                    <Suspense fallback={<WidgetSkeleton />}>
                        {/* @ts-expect-error Server Component */}
                        <TxTradersTableWidget
                            epochId={txLogItem.epochId}
                            txOrdinal={txLogItem.txOrdinal}
                            event={txLogItem.eventKind}
                        />
                    </Suspense>,
                ],
            };
        default:
            return null;
    }
}

// TODO: basic layout just to display the event kind of the transaction
export default async function Page({ params }: { params: { epochId: string; txOrdinal: string } }) {
    const headersList = headers();
    const host = headersList.get('host') || '';

    const response = await statsApiListFetcher<TxLogResponseItem>(
        `/tx_logs?epoch=${params.epochId}&txOrdinal=${+params.txOrdinal + 1}&limit=1&order=desc`,
        host,
    );

    if (!response.success) {
        return <ErrorWidget title="Transaction" />;
    }

    const [txLog] = response.value;

    if (txLog === undefined) {
        return <ErrorWidget title="Transaction" />;
    }

    const activeDetails = getTxDetailsWidget(txLog);
    const activeAffected = getTxAffectedWidget(txLog);

    return (
        <ClientWrapper>
            <ViewHeading title={`Transaction View: Epoch ${params.epochId}, TxOrdinal ${params.txOrdinal}`} />
            <WidgetFlexContainer>
                <Suspense fallback={<WidgetSkeleton title={'Transaction Overview'} />}>
                    <TxOverviewWidget transaction={txLog} lastUpdatedTimestamp={response.timestamp} />
                </Suspense>
                {activeDetails !== null ? (
                    <Suspense fallback={<WidgetSkeleton title={'Transaction Details}'} />}>{activeDetails}</Suspense>
                ) : (
                    <></>
                )}
            </WidgetFlexContainer>
            {activeAffected !== null ? (
                <WidgetTabbedContainer tabNames={activeAffected.tabNames} tabPanels={activeAffected.tabPanels} />
            ) : (
                <></>
            )}
        </ClientWrapper>
    );
}
