import { Metadata } from 'next';
import { headers } from 'next/headers';
import { Suspense } from 'react';
import { ClientWrapper } from 'src/app/clientWrapper';
import ViewHeading from 'src/components/ViewHeading';
import EpochHistory from 'src/components/views/epoch/EpochHistory';
import EpochWidget from 'src/components/views/epoch/EpochWidget';
import WidgetFlexContainer from 'src/components/widgets/WidgetFlexContainer';
import WidgetSkeleton from 'src/components/widgets/WidgetSkeleton';
import WidgetTabbedContainer from 'src/components/widgets/WidgetTabbedContainer';

type Props = {
    params: { epochId: string };
};

export async function generateMetadata({ params }: Props): Promise<Metadata> {
    const headersList = headers();
    const header_url = headersList.get('url') || '';
    const { epochId } = params;
    return {
        title: `Epoch ${epochId} - DerivaDEX | Explorer`,
        openGraph: {
            type: 'website',
            title: 'Epoch Details - DerivaDEX | Explorer',
            siteName: 'DerivaDEX | Explorer',
            url: header_url,
            description: `Detailed information about epoch ${epochId}.`,
        },
        twitter: {
            card: 'summary_large_image',
            site: '@DDX_Official',
        },
    };
}

export default async function Page({ params }: Props) {
    return (
        <ClientWrapper>
            <ViewHeading title={'Epoch View'} />
            <WidgetFlexContainer>
                <Suspense fallback={<WidgetSkeleton title={'Epoch Details'} />}>
                    {/* @ts-expect-error Server Component */}
                    <EpochWidget epochId={params.epochId} />
                </Suspense>
            </WidgetFlexContainer>
            <WidgetTabbedContainer
                tabNames={['Epoch History']}
                tabPanels={[
                    <Suspense fallback={<WidgetSkeleton />}>
                        {/* @ts-expect-error Server Component */}
                        <EpochHistory epochId={params.epochId} />
                    </Suspense>,
                ]}
            />
        </ClientWrapper>
    );
}
