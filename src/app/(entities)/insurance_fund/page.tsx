import { Metadata } from 'next';
import { headers } from 'next/headers';
import { Suspense } from 'react';
import { ClientWrapper } from 'src/app/clientWrapper';
import ViewHeading from 'src/components/ViewHeading';
import InsuranceFundWidget from 'src/components/views/insurance_fund';
import InsuranceFundHistoryWidget from 'src/components/views/insurance_fund/InsuranceFundHistoryWidget';
import WidgetFlexContainer from 'src/components/widgets/WidgetFlexContainer';
import WidgetSkeleton from 'src/components/widgets/WidgetSkeleton';
import WidgetTabbedContainer from 'src/components/widgets/WidgetTabbedContainer';

export async function generateMetadata(): Promise<Metadata> {
    const headersList = headers();
    const header_url = headersList.get('url') || '';
    const host = headersList.get('host') || '';
    const imageURL = '/api/open_graph/insurance_fund';
    const currentTime = Date.now();
    return {
        title: 'Insurance Fund - DerivaDEX | Explorer',
        openGraph: {
            type: 'website',
            title: 'Insurance Fund Details - DerivaDEX | Explorer',
            siteName: 'DerivaDEX | Explorer',
            url: header_url,
            description: 'Detailed information about the DerivaDEX insurance fund.',
            images: `https://${host}${imageURL}?nonce=${currentTime}`,
        },
        twitter: {
            card: 'summary_large_image',
            site: '@DDX_Official',
        },
    };
}

export default async function Page() {
    return (
        <ClientWrapper>
            <ViewHeading title={'Insurance Fund View'} />
            <WidgetFlexContainer>
                <Suspense fallback={<WidgetSkeleton title={'Insurance Fund Details'} />}>
                    {/* @ts-expect-error Server Component */}
                    <InsuranceFundWidget />
                </Suspense>
            </WidgetFlexContainer>
            <WidgetTabbedContainer
                tabNames={['Insurance Fund History']}
                tabPanels={[
                    <Suspense fallback={<WidgetSkeleton />}>
                        {/* @ts-expect-error Server Component */}
                        <InsuranceFundHistoryWidget />
                    </Suspense>,
                ]}
            />
        </ClientWrapper>
    );
}
