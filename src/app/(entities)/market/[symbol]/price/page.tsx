import { Suspense } from 'react';
import { ClientWrapper } from 'src/app/clientWrapper';
import ViewHeading from 'src/components/ViewHeading';
import PriceWidget from 'src/components/views/price';
import PriceHistoryWidget from 'src/components/views/price/PriceHistory';
import WidgetFlexContainer from 'src/components/widgets/WidgetFlexContainer';
import WidgetSkeleton from 'src/components/widgets/WidgetSkeleton';
import WidgetTabbedContainer from 'src/components/widgets/WidgetTabbedContainer';

export default async function Page({ params }: { params: { symbol: string } }) {
    return (
        <ClientWrapper>
            <ViewHeading title={'Price View'} />
            <WidgetFlexContainer>
                <Suspense fallback={<WidgetSkeleton title={'Price Details'} />}>
                    {/* @ts-expect-error Server Component */}
                    <PriceWidget symbol={params.symbol} />
                </Suspense>
            </WidgetFlexContainer>
            <WidgetTabbedContainer
                tabNames={['Price History']}
                tabPanels={[
                    <Suspense fallback={<WidgetSkeleton />}>
                        {/* @ts-expect-error Server Component */}
                        <PriceHistoryWidget symbol={params.symbol} />
                    </Suspense>,
                ]}
            />
        </ClientWrapper>
    );
}
