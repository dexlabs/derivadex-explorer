import { Metadata } from 'next';
import { headers } from 'next/headers';
import { Suspense } from 'react';
import { ClientWrapper } from 'src/app/clientWrapper';
import ViewHeading from 'src/components/ViewHeading';
import MarketPriceFeedsWidget from 'src/components/views/market/MarketPriceFeedsWidget';
import MarketWidget from 'src/components/views/market/MarketWidget';
import PositionsWidget from 'src/components/views/market/PositionsWidget';
import WidgetFlexContainer from 'src/components/widgets/WidgetFlexContainer';
import WidgetSkeleton from 'src/components/widgets/WidgetSkeleton';
import WidgetTabbedContainer from 'src/components/widgets/WidgetTabbedContainer';

type Props = {
    params: { symbol: string };
};

export async function generateMetadata({ params }: Props): Promise<Metadata> {
    const headersList = headers();
    const header_url = headersList.get('url') || '';
    const host = headersList.get('host') || '';
    const { symbol } = params;
    const imageURL = `/api/open_graph/market/${symbol}`;
    const currentTime = Date.now();
    return {
        title: `${symbol} - DerivaDEX | Explorer`,
        openGraph: {
            type: 'website',
            title: 'Market Details - DerivaDEX | Explorer',
            siteName: 'DerivaDEX | Explorer',
            url: header_url,
            description: `Detailed information about the ${symbol} market. Volume, open interest, funding rate, and other market data are also shown.`,
            images: `https://${host}${imageURL}?nonce=${currentTime}`,
        },
        twitter: {
            card: 'summary_large_image',
            site: '@DDX_Official',
        },
    };
}

export default async function Page({ params }: Props) {
    return (
        <ClientWrapper>
            <ViewHeading title={`Market View: ${params.symbol}`} />
            <WidgetFlexContainer>
                <Suspense fallback={<WidgetSkeleton title={'Market Details'} />}>
                    {/* @ts-expect-error Server Component */}
                    <MarketWidget symbol={params.symbol} />
                </Suspense>
            </WidgetFlexContainer>
            <WidgetTabbedContainer
                tabNames={['Positions', 'Price Feeds']}
                tabPanels={[
                    <Suspense fallback={<WidgetSkeleton />}>
                        {/* @ts-expect-error Server Component */}
                        <PositionsWidget symbol={params.symbol} />
                    </Suspense>,
                    <Suspense fallback={<WidgetSkeleton />}>
                        {/* @ts-expect-error Server Component */}
                        <MarketPriceFeedsWidget symbol={params.symbol} />
                    </Suspense>,
                ]}
            />
        </ClientWrapper>
    );
}
