import { shortenHex } from '@utils';
import { Metadata } from 'next';
import { headers } from 'next/headers';
import { Suspense } from 'react';
import { ClientWrapper } from 'src/app/clientWrapper';
import ViewHeading from 'src/components/ViewHeading';
import StrategiesWidget from 'src/components/views/trader/StrategiesWidget';
import TraderHistoryWidget from 'src/components/views/trader/TraderHistoryWidget';
import TraderWidget from 'src/components/views/trader/TraderWidget';
import WidgetFlexContainer from 'src/components/widgets/WidgetFlexContainer';
import WidgetSkeleton from 'src/components/widgets/WidgetSkeleton';
import WidgetTabbedContainer from 'src/components/widgets/WidgetTabbedContainer';

type Props = {
    params: { trader: string };
};

export async function generateMetadata({ params }: Props): Promise<Metadata> {
    const headersList = headers();
    const header_url = headersList.get('url') || '';
    const host = headersList.get('host') || '';
    const { trader } = params;
    const imageURL = `/api/open_graph/account/${trader}`;
    const currentTime = Date.now();
    return {
        title: `Account ${shortenHex(trader)} - DerivaDEX | Explorer`,
        openGraph: {
            type: 'website',
            title: 'Account Details - DerivaDEX | Explorer',
            siteName: 'DerivaDEX | Explorer',
            url: header_url,
            description: `Detailed information about an account for trader address ${trader}. The owned strategies, DDX balance, and other account data are also shown.`,
            images: `https://${host}${imageURL}?nonce=${currentTime}`,
        },
        twitter: {
            card: 'summary_large_image',
            site: '@DDX_Official',
        },
    };
}

export default async function Trader({ params }: { params: { trader: string } }) {
    return (
        <ClientWrapper>
            <ViewHeading title={`Account View: ${shortenHex(params.trader)}`} />
            <WidgetFlexContainer>
                <Suspense fallback={<WidgetSkeleton title={'Account Details'} />}>
                    {/* @ts-expect-error Server Component */}
                    <TraderWidget trader={params.trader} />
                </Suspense>
                <Suspense fallback={<WidgetSkeleton title={'Owned Strategies}'} />}>
                    {/* @ts-expect-error Server Component */}
                    <StrategiesWidget trader={params.trader} />
                </Suspense>
            </WidgetFlexContainer>
            <WidgetTabbedContainer
                tabNames={['Account Updates']}
                tabPanels={[
                    <Suspense fallback={<WidgetSkeleton />}>
                        {/* @ts-expect-error Server Component */}
                        <TraderHistoryWidget trader={params.trader} />
                    </Suspense>,
                ]}
            />
        </ClientWrapper>
    );
}
