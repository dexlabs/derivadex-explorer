import { shortenHex } from '@utils';
import { Metadata } from 'next';
import { headers } from 'next/headers';
import { Suspense } from 'react';
import { ClientWrapper } from 'src/app/clientWrapper';
import Breadcrumbs from 'src/components/breadcrumbs';
import ViewHeading from 'src/components/ViewHeading';
import OrderDetailsWidget from 'src/components/views/order/OrderDetailsWidget';
import OrderHistoryWidget from 'src/components/views/order/OrderHistoryWidget';
import WidgetFlexContainer from 'src/components/widgets/WidgetFlexContainer';
import WidgetSkeleton from 'src/components/widgets/WidgetSkeleton';
import WidgetTabbedContainer from 'src/components/widgets/WidgetTabbedContainer';

type Props = {
    params: { trader: string; strategyId: string; symbol: string; orderHash: string };
};

export async function generateMetadata({ params }: Props): Promise<Metadata> {
    const headersList = headers();
    const header_url = headersList.get('url') || '';
    const host = headersList.get('host') || '';
    const { trader, strategyId, orderHash } = params;
    const imageURL = `/api/open_graph/account/${trader}/strategy/${strategyId}/order/${orderHash}`;
    const currentTime = Date.now();
    return {
        title: `Order Hash ${shortenHex(orderHash)} strategy ID ${strategyId}, account ${shortenHex(
            trader,
        )} - DerivaDEX | Explorer`,
        openGraph: {
            type: 'website',
            title: 'Order Details - DerivaDEX | Explorer',
            siteName: 'DerivaDEX | Explorer',
            url: header_url,
            description: `Detailed information about an order for trader address ${trader}, strategy ID ${strategyId} and order hash ${orderHash}. The orderType, amount, price, and other order data are also shown.`,
            images: `https://${host}${imageURL}?nonce=${currentTime}`,
        },
        twitter: {
            card: 'summary_large_image',
            site: '@DDX_Official',
        },
    };
}

export default async function Order({ params }: { params: { trader: string; strategyId: string; orderHash: string } }) {
    return (
        <ClientWrapper>
            <ViewHeading title={`Order View: ${shortenHex(params.orderHash)}`} />
            <Breadcrumbs entity="order" params={params} />
            <WidgetFlexContainer>
                <Suspense fallback={<WidgetSkeleton title={'Order Details'} />}>
                    {/* @ts-expect-error Server Component */}
                    <OrderDetailsWidget
                        trader={params.trader}
                        strategyId={params.strategyId}
                        orderHash={params.orderHash}
                    />
                </Suspense>
            </WidgetFlexContainer>
            <WidgetTabbedContainer
                tabNames={['Order History']}
                tabPanels={[
                    <Suspense fallback={<WidgetSkeleton />}>
                        {/* @ts-expect-error Server Component */}
                        <OrderHistoryWidget orderHash={params.orderHash} />
                    </Suspense>,
                ]}
            />
        </ClientWrapper>
    );
}
