import { shortenHex } from '@utils';
import type { Metadata } from 'next';
import { headers } from 'next/headers';
import { Suspense } from 'react';
import { ClientWrapper } from 'src/app/clientWrapper';
import Breadcrumbs from 'src/components/breadcrumbs';
import ViewHeading from 'src/components/ViewHeading';
import PositionHistoryWidget from 'src/components/views/position/PositionHistoryWidget';
import PositionWidget from 'src/components/views/position/PositionWidget';
import WidgetFlexContainer from 'src/components/widgets/WidgetFlexContainer';
import WidgetSkeleton from 'src/components/widgets/WidgetSkeleton';
import WidgetTabbedContainer from 'src/components/widgets/WidgetTabbedContainer';

type Props = {
    params: { trader: string; strategyId: string; symbol: string };
};

export async function generateMetadata({ params }: Props): Promise<Metadata> {
    const headersList = headers();
    const header_url = headersList.get('url') || '';
    const host = headersList.get('host') || '';
    const { symbol, trader, strategyId } = params;
    const imageURL = `/api/open_graph/account/${trader}/strategy/${strategyId}/position/${symbol}`;
    const currentTime = Date.now();
    return {
        title: `Position ${symbol} strategy ID ${strategyId}, account ${shortenHex(trader)} - DerivaDEX | Explorer`,
        openGraph: {
            type: 'website',
            title: 'Position Details - DerivaDEX | Explorer',
            siteName: 'DerivaDEX | Explorer',
            url: header_url,
            description: `Detailed information about a position for trader address ${trader}, strategy ID ${strategyId} and market symbol ${symbol}. The average entry price, position side, and other position data are also shown.`,
            images: `https://${host}${imageURL}?nonce=${currentTime}`,
        },
        twitter: {
            card: 'summary_large_image',
            site: '@DDX_Official',
        },
    };
}

export default async function Page({ params }: Props) {
    return (
        <ClientWrapper>
            <ViewHeading title={`Position View: ${params.symbol}`} />
            <Breadcrumbs entity="position" params={params} />
            <WidgetFlexContainer>
                <Suspense fallback={<WidgetSkeleton />}>
                    {/* @ts-expect-error Server Component */}
                    <PositionWidget trader={params.trader} strategyId={params.strategyId} symbol={params.symbol} />
                </Suspense>
            </WidgetFlexContainer>
            <WidgetTabbedContainer
                tabNames={['Position History']}
                tabPanels={[
                    <Suspense fallback={<WidgetSkeleton />}>
                        {/* @ts-expect-error Server Component */}
                        <PositionHistoryWidget
                            trader={params.trader}
                            strategyId={params.strategyId}
                            symbol={params.symbol}
                        />
                    </Suspense>,
                ]}
            />
        </ClientWrapper>
    );
}
