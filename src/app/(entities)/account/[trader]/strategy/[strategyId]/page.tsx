import { shortenHex } from '@utils';
import { Metadata } from 'next';
import { headers } from 'next/headers';
import { Suspense } from 'react';
import { ClientWrapper } from 'src/app/clientWrapper';
import Breadcrumbs from 'src/components/breadcrumbs';
import ViewHeading from 'src/components/ViewHeading';
import OpenOrdersWidget from 'src/components/views/order/OpenOrdersWidget';
import PositionsWidget from 'src/components/views/strategy/PositionsWidget';
import StrategyHistoryWidget from 'src/components/views/strategy/StrategyHistoryWidget';
import StrategyWidget from 'src/components/views/strategy/StrategyWidget';
import TradesWidget from 'src/components/views/trade/TradesWidget';
import WidgetFlexContainer from 'src/components/widgets/WidgetFlexContainer';
import WidgetSkeleton from 'src/components/widgets/WidgetSkeleton';
import WidgetTabbedContainer from 'src/components/widgets/WidgetTabbedContainer';

type Props = {
    params: { trader: string; strategyId: string };
};

export async function generateMetadata({ params }: Props): Promise<Metadata> {
    const headersList = headers();
    const header_url = headersList.get('url') || '';
    const host = headersList.get('host') || '';
    const { trader, strategyId } = params;
    const imageURL = `/api/open_graph/account/${trader}/strategy/${strategyId}`;
    const currentTime = Date.now();
    return {
        title: `Strategy ID ${strategyId}, account ${shortenHex(trader)} - DerivaDEX | Explorer`,
        openGraph: {
            type: 'website',
            title: 'Strategy Details - DerivaDEX | Explorer',
            siteName: 'DerivaDEX | Explorer',
            url: header_url,
            description: `Detailed information about a strategy for trader address ${trader}, strategy ID ${strategyId}. The owned positions, leverage, and other strategy data are also shown.`,
            images: `https://${host}${imageURL}?nonce=${currentTime}`,
        },
        twitter: {
            card: 'summary_large_image',
            site: '@DDX_Official',
        },
    };
}

export default async function Strategy({ params }: { params: { trader: string; strategyId: string } }) {
    return (
        <ClientWrapper>
            <ViewHeading title={`Strategy View: ${params.strategyId}`} />
            <Breadcrumbs entity="strategy" params={params} />
            <WidgetFlexContainer>
                <Suspense fallback={<WidgetSkeleton title={'Strategy Details'} />}>
                    {/* @ts-expect-error Server Component */}
                    <StrategyWidget trader={params.trader} strategyId={params.strategyId} />
                </Suspense>
                <Suspense fallback={<WidgetSkeleton title={'Owned Positions}'} />}>
                    {/* @ts-expect-error Server Component */}
                    <PositionsWidget trader={params.trader} strategyId={params.strategyId} />
                </Suspense>
            </WidgetFlexContainer>
            <WidgetTabbedContainer
                tabNames={['Trades', 'Open Orders', 'Strategy History']}
                tabPanels={[
                    <Suspense fallback={<WidgetSkeleton />}>
                        {/* @ts-expect-error Server Component */}
                        <TradesWidget trader={params.trader} strategyId={params.strategyId} />
                    </Suspense>,
                    <Suspense fallback={<WidgetSkeleton />}>
                        {/* @ts-expect-error Server Component */}
                        <OpenOrdersWidget trader={params.trader} strategyId={params.strategyId} />
                    </Suspense>,
                    <Suspense fallback={<WidgetSkeleton />}>
                        {/* @ts-expect-error Server Component */}
                        <StrategyHistoryWidget trader={params.trader} strategyId={params.strategyId} />
                    </Suspense>,
                ]}
            />
        </ClientWrapper>
    );
}
