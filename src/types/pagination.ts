export type Pagination = {
    url: string;
    host: string;
    limit: number;
};

export interface TimeseriesPagination extends Pagination {
    nextEpoch: number | null;
    nextTxOrdinal: number | null;
    trader: string | null;
}
