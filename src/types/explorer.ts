import { ParamEventTypes } from './api';

export interface SearchResponse {
    traders: string[];
    markets: string[];
    epochs: string[];
    transactions: Array<{
        epoch: string;
        txOrdinal: string;
        eventKind: ParamEventTypes;
    }>;
}
