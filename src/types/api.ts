import BigNumber from 'bignumber.js';

export type TradersResponseItem = {
    trader: string;
    availDdx: BigNumber;
    lockedDdx: BigNumber;
    payFeesInDdx: boolean;
};

export type TopTradersResponseItem = {
    trader: string;
    volume: BigNumber;
    realizedPnl: BigNumber;
};

export type MarketsAggregationResponseItem = {
    market: string;
    volume: BigNumber;
    price: BigNumber;
    fundingRate: BigNumber;
    openInterest: BigNumber;
};

export type PositionResponseItem = {
    trader: string;
    symbol: string;
    strategyIdHash: string;
    side: ParamPositionSide;
    balance: BigNumber;
    avgEntryPrice: BigNumber;
    lastModifiedInEpoch: null | number;
};

export type MarketResponseItem = {
    market: string;
    volume: BigNumber;
    price: BigNumber;
    fundingRate: BigNumber;
    openInterest: BigNumber;
};

export type TickerResponseItem = {
    symbol: string;
    high: BigNumber;
    low: BigNumber;
    volumeWeightedAveragePrice: BigNumber;
    open: BigNumber;
    close: BigNumber;
    previousClose: BigNumber;
    change: BigNumber;
    percentage: BigNumber;
    baseVolume: BigNumber;
    notionalVolume: BigNumber;
};

export type MarketSpecsResponseItem = {
    kind: BigNumber;
    name: string;
    expr: string;
    value: {
        tickSize: BigNumber;
        allocation: { [key: string]: string };
        minOrderSize: BigNumber;
        maxOrderNotional: BigNumber;
        initialIndexPrice: string;
        rebalanceInterval: number;
        maxTakerPriceDeviation: BigNumber;
        kind: ParamSpecsKind;
    };
};

export type PriceFeedSpecsResponseItem = {
    kind: BigNumber;
    name: string;
    expr: string;
    value: {
        port: string;
        quote: string;
        timeOp: {
            query: {
                Literal: string;
            };
        };
        symbols: string[];
        hostname: string;
        transforms: string;
        spotPriceOp: {
            query: {
                Transform: {
                    Format: string;
                };
            };
        };
        httpTemplate: string;
    };
};

export type StrategyResponseItem = {
    trader: string;
    strategyId: string;
    strategyIdHash: string;
    maxLeverage: number;
    availCollateral: BigNumber;
    lockedCollateral: number;
    frozen: boolean;
};

export type TxLogResponseItem = {
    epochId: string;
    txOrdinal: string;
    requestIndex: string;
    batchId: string;
    timeValue: string;
    timeStamp: string;
    stateRootHash: string;
    eventKind: ParamEventTypes;
    event: string;
};

export enum ParamEventTypes {
    PARTIAL_FILL = 0,
    COMPLETE_FILL = 1,
    POST = 2,
    CANCEL = 3,
    CANCEL_ALL = 30,
    LIQUIDATION = 4,
    STRATEGY_UPDATE = 5,
    TRADER_UPDATE = 6,
    WITHDRAW = 7,
    WITHDRAW_DDX = 8,
    PRICE_CHECKPOINT = 9,
    PNL_SETTLEMENT = 10,
    FUNDING = 11,
    TRADE_MINING = 12,
    SPECS_UPDATE = 13,
    ADVANCE_EPOCH = 100,
    INSURANCE_FUND_UPDATE = 14,
    INSURANCE_FUND_WITHDRAW = 15,
    DISASTER_RECOVERY = 16,
    SIGNER_REGISTERED = 60,
    FEE_DISTRIBUTION = 70,
    DRAIN_PRICES = 230,
    NO_TRANSITION = 999,
}

export enum ParamSpecsKind {
    SingleNamePerpetual = 'SingleNamePerpetual',
    IndexFundPerpetual = 'IndexFundPerpetual',
}
export type TraderUpdateResponseItem = {
    epochId: string;
    txOrdinal: string;
    ordinal: string;
    trader: string;
    amount: BigNumber;
    newDdxBalance: BigNumber;
    kind: ParamTraderUpdateKind;
    payFeesInDdx: boolean;
    blockNumber: string;
    txHash: string;
    createdAt: string;
};

export enum ParamTraderUpdateKind {
    Deposit = 0,
    Withdraw_DDX = 1,
    Claim_DDX_Withdrawal = 2,
    Trade_Mining_Reward = 3,
    Update_Profile = 4,
}

export type StrategyUpdateResponseItem = {
    epochId: string;
    txOrdinal: string;
    ordinal: string;
    trader: string;
    strategyId: string;
    strategyIdHash: string;
    collateralAddress: string;
    amount: BigNumber;
    newAvailCollateral: BigNumber;
    newLockedCollateral: BigNumber;
    kind: ParamStrategyUpdateKind;
    blockNumber: string;
    txHash: string;
    newAvgEntryPrices: string;
    fundingPayments: string;
    createdAt: string;
};

export enum ParamStrategyUpdateKind {
    Deposit = 0,
    Withdraw = 1,
    Withdraw_Intent = 2,
    Funding_Payment = 3,
    Realized_Pnl = 4,
}

export enum ParamPositionSide {
    None = 0,
    Long = 1,
    Short = 2,
}

export type PositionDetailsItem = {
    trader: string;
    symbol: string;
    strategyIdHash: string;
    side: string | ParamPositionSide;
    balance: string;
    avgEntryPrice: string;
    lastModifiedInEpoch: null | number;
};

export type PositionDetailsMoreInfoItem = {
    percentPnL: string;
    currentLeverage: string;
    notionalValue: string;
    markPrice: string;
};

export type LiquidationsResponseItem = {
    epochId: string;
    txOrdinal: string;
    ordinal: string;
    symbol: string;
    trader: string;
    strategyIdHash: string;
    triggerPriceHash: string;
    markPrice: BigNumber;
    insuranceFundCapitalization: BigNumber;
    createdAt: Date;
};

export type AdlResponseItem = {
    epochId: string;
    txOrdinal: string;
    ordinal: string;
    amount: BigNumber;
    symbol: string;
    adlTrader: string;
    adlStrategyIdHash: string;
    liquidatedTrader: string;
    liquidatedStrategyIdHash: string;
    createdAt: Date;
    side: ParamPositionSide;
};

export type InsuranceFundResponseItem = {
    epochId: string;
    txOrdinal: string;
    symbol: string;
    totalCapitalization: BigNumber;
    kind: ParamInsuranceFundEvent;
    createdAt: Date;
};

export enum ParamInsuranceFundEvent {
    Fill = 0,
    Liquidation = 1,
    Deposit = 2,
    Withdrawal = 3,
}

export type EpochResponseItem = {
    epochId: string;
    startTime: Date;
    endTime: Date | null;
};

export type DdxFeePoolResponseItem = {
    epochId: number;
    txOrdinal: number;
    totalCapitalization: BigNumber;
    createdAt: Date;
};

export type MarkPricesResponseItem = {
    epochId: string;
    txOrdinal: string;
    indexPriceHash: string;
    symbol: string;
    indexPrice: BigNumber;
    markPrice: BigNumber;
    time: number;
    ema: BigNumber;
    priceOrdinal: string;
    createdAt: Date;
};
