import { Jsonify } from 'src/data/types';

import { TxLogResponseItem } from './api';

export enum RealtimeFeed {
    TX_LOG = 'TX_LOG',
}

export type RealtimeFeedTxLogParams = {
    eventKind?: number;
};

export type RealtimeFeedTxLogSubscription = {
    feed: RealtimeFeed.TX_LOG;
    params: RealtimeFeedTxLogParams;
};

export type RealtimeFeedSubscription = RealtimeFeedTxLogSubscription;

export enum RealtimeFeedMessageType {
    PARTIAL = 'partial',
    UPDATE = 'update',
}

export type RealtimeFeedOutgoingMessageContents<T> = {
    messageType: RealtimeFeedMessageType;
    data: Jsonify<T> | null;
    ordinal: number;
};

export type RealtimeFeedTxLogOutgoingMessage = {
    feed: RealtimeFeed.TX_LOG;
    params: RealtimeFeedTxLogParams;
    contents: RealtimeFeedOutgoingMessageContents<TxLogResponseItem[]>;
};

export type RealtimeFeedOutgoingMessage = RealtimeFeedTxLogOutgoingMessage;
