import BigNumber from 'bignumber.js';

export type TraderHistoryEntity = {
    epochId: number;
    txOrdinal: number;
    event: ParamTraderHistoryEvent;
    amount: BigNumber;
    collateralBalance: BigNumber;
    createdAt: Date;
};

export enum ParamTraderHistoryEvent {
    Deposit,
    Withdraw,
    Withdraw_Intent,
    Trade_Mining,
    Fees,
}
