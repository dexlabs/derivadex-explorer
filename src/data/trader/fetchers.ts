import { statsApiListFetcher } from '@utils';
import BigNumber from 'bignumber.js';

import { ParamTraderUpdateKind, TraderUpdateResponseItem } from '@/types/api';

import { FillsResponseItem } from '../fill/types';
import { ParamTraderHistoryEvent, TraderHistoryEntity } from './types';

export async function statsApiTraderHistoryFetcher(
    trader: string,
    limit: number,
    host: string,
    pagination?: { epoch: string; txOrdinal: string },
): Promise<TraderHistoryEntity[] | null> {
    const paginationUrl = pagination ? `&epoch=${pagination.epoch}&txOrdinal=${pagination.txOrdinal}` : '';
    const [updatesResponse, fillsResponse] = await Promise.all([
        statsApiListFetcher<TraderUpdateResponseItem>(
            `/account/${trader}/trader_updates?order=desc&limit=${limit}${paginationUrl}`,
            host,
        ),
        // TODO: need a way to fetch all fills at trader level
        statsApiListFetcher<FillsResponseItem>(
            `/account/${trader}/order_updates?order=desc&limit=${limit}&fillReason=0${paginationUrl}`,
            host,
        ),
    ]);

    if (!updatesResponse.success || !fillsResponse.success) {
        return null;
    }

    const list: TraderHistoryEntity[] = [];

    for (let i = 0; i < updatesResponse.value.length; i++) {
        const updateItem = updatesResponse.value[i];
        if (updateItem.kind !== ParamTraderUpdateKind.Update_Profile) {
            const strategyHistoryEntity = {
                epochId: +updateItem.epochId,
                txOrdinal: +updateItem.txOrdinal,
                event: updateItem.kind.valueOf() as ParamTraderHistoryEvent,
                amount: new BigNumber(updateItem.amount),
                collateralBalance: new BigNumber(updateItem.newDdxBalance),
                createdAt: new Date(updateItem.createdAt),
            };
            list.push(strategyHistoryEntity);
        }
    }

    for (let i = 0; i < fillsResponse.value.length; i++) {
        const fillsItem = fillsResponse.value[i];
        // Only takers pay fees, for trader history only ddx fees are relevant
        if (fillsItem.takerOrderTrader.endsWith(trader.substring(2)) && fillsItem.takerFeeDDX) {
            const strategyHistoryEntity = {
                epochId: +fillsItem.epochId,
                txOrdinal: +fillsItem.txOrdinal,
                event: ParamTraderHistoryEvent.Fees,
                amount: new BigNumber('-' + fillsItem.takerFeeDDX),
                collateralBalance: new BigNumber(0),
                createdAt: new Date(fillsItem.createdAt),
            };
            list.push(strategyHistoryEntity);
        }
    }

    const response = list
        .sort((a: TraderHistoryEntity, b: TraderHistoryEntity) => {
            if (b.epochId === a.epochId) {
                return b.txOrdinal - a.txOrdinal;
            }
            return b.epochId - a.epochId;
        })
        .slice(0, limit);

    return response;
}
