import { statsApiListFetcher } from '@utils';
import BigNumber from 'bignumber.js';

import { DdxFeePoolResponseItem, TxLogResponseItem } from '@/types/api';

export async function statsApiDdxPoolHistoryFetcher(
    limit: number,
    host: string,
    pagination?: { epoch: string; txOrdinal: string },
): Promise<{ value: DdxFeePoolResponseItem[]; previousCapitalizationValue: string } | null> {
    const paginationUrl = pagination ? `&epoch=${pagination.epoch}&txOrdinal=${pagination.txOrdinal}` : '';
    const [ddxResponse, txLogsResponse] = await Promise.all([
        statsApiListFetcher<DdxFeePoolResponseItem>(
            `/ddx_fee_pool?order=desc&limit=${limit + 1}${paginationUrl}`,
            host,
        ),
        statsApiListFetcher<TxLogResponseItem>(
            `/tx_logs?order=desc&eventKind=70&limit=${limit + 1}${paginationUrl}`,
            host,
        ),
    ]);

    if (!ddxResponse.success) {
        return null;
    }

    let poolItems: DdxFeePoolResponseItem[] = !txLogsResponse.success
        ? []
        : txLogsResponse.value.map((it) => ({
              totalCapitalization: new BigNumber('0'),
              epochId: +it.epochId,
              txOrdinal: +it.txOrdinal,
              createdAt: new Date(+it.timeStamp),
          }));

    poolItems = poolItems.concat(
        ddxResponse.value.map((it) => ({
            totalCapitalization: new BigNumber(it.totalCapitalization),
            epochId: +it.epochId,
            txOrdinal: +it.txOrdinal,
            createdAt: new Date(it.createdAt),
        })),
    );

    const response = poolItems
        .sort((a: DdxFeePoolResponseItem, b: DdxFeePoolResponseItem) => {
            if (b.epochId === a.epochId) {
                return b.txOrdinal - a.txOrdinal;
            }
            return b.epochId - a.epochId;
        })
        .slice(0, limit);

    return {
        value: response,
        previousCapitalizationValue:
            poolItems.length > limit ? poolItems[poolItems.length - 1].totalCapitalization.toString() : '-1',
    };
}
