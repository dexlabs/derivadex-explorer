import { ParamEventTypes } from '@/types/api';

export function getTransactionEventType(eventKind: ParamEventTypes) {
    const indexOf = Object.values(ParamEventTypes).indexOf(eventKind as unknown as ParamEventTypes);
    return Object.keys(ParamEventTypes)[indexOf].toString();
}
