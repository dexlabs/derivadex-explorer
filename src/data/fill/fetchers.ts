import { statsApiListFetcher } from '@utils';
import BigNumber from 'bignumber.js';

import { statsApiOrderDetailsFetcher } from '../order/fetchers';
import {
    OrderEntity,
    OrderIntentResponseItem,
    OrderRejectReason,
    ParamOrderSide,
    ParamOrderType,
} from '../order/types';
import { FillsResponseItem, TxFillAffectedMakerEntity, TxFillTakerEntity } from './types';

export async function statsApiTxFillTakerFetcher(
    epochId: string,
    txOrdinal: string,
    host: string,
): Promise<{ timestamp: number; value: TxFillTakerEntity } | null> {
    const fillsResponse = await statsApiListFetcher<FillsResponseItem>(
        `/order_updates?order=desc&limit=50&epoch=${epochId}&txOrdinal=${+txOrdinal + 1}`,
        host,
    );

    if (!fillsResponse.success) {
        return null;
    }

    // response will contain all transactions previous to epoch, txOrdinal passed in params
    const filteredFills = fillsResponse.value.filter(
        (it) => it.epochId === epochId && it.txOrdinal === txOrdinal && it.reason !== 2,
    );

    if (filteredFills.length === 0) {
        return null;
    }

    const [fill] = filteredFills;

    if (fill === undefined) {
        return null;
    }

    const orderResponse = await statsApiOrderDetailsFetcher(fill.takerOrderTrader, fill.takerOrderHash, host);

    if (!orderResponse.success) {
        return null;
    }

    const takerOrder: OrderEntity = {
        orderHash: fill.takerOrderHash,
        epochId: fill.epochId,
        txOrdinal: fill.txOrdinal,
        symbol: fill.symbol,
        side: orderResponse.value.side,
        amount: filteredFills.reduce((prev, curr) => prev.plus(curr.amount), new BigNumber(0)),
        price: orderResponse.value.price,
        traderAddress: fill.takerOrderTrader,
        strategyId: orderResponse.value.strategyId,
        orderType: orderResponse.value.orderType,
        remainingAmount: orderResponse.value.remainingAmount,
        rejectedAmount: new BigNumber(0),
        rejectedReason: OrderRejectReason.NoOrderRejection,
        createdAt: orderResponse.value.createdAt,
    };
    const aggregationValues = filteredFills.reduce(
        (prev, curr) => {
            prev.sumPrice = prev.sumPrice.plus(curr.price);
            prev.feesUsdcPaid = prev.feesUsdcPaid.plus(curr.takerFeeUSDC ?? 0);
            prev.feesDdxPaid = prev.feesDdxPaid.plus(curr.takerFeeDDX ?? 0);
            return prev;
        },
        { sumPrice: new BigNumber(0), feesUsdcPaid: new BigNumber(0), feesDdxPaid: new BigNumber(0) },
    );
    return {
        timestamp: fillsResponse.timestamp,
        value: {
            epochId: fill.epochId,
            txOrdinal: fill.txOrdinal,
            takerOrder: takerOrder,
            takerAvgPrice: aggregationValues.sumPrice.div(filteredFills.length),
            fees: aggregationValues.feesUsdcPaid.gt(0) ? aggregationValues.feesUsdcPaid : aggregationValues.feesDdxPaid,
            feesCurrency: aggregationValues.feesUsdcPaid.gt(0) ? 'USDC' : 'DDX',
        },
    };
}

export async function statsApiTxFillMakersFetcher(
    epochId: string,
    txOrdinal: string,
    host: string,
): Promise<{ timestamp: number; value: TxFillAffectedMakerEntity } | null> {
    const fillsResponse = await statsApiListFetcher<FillsResponseItem>(
        `/order_updates?order=desc&limit=50&epoch=${epochId}&txOrdinal=${+txOrdinal + 1}`,
        host,
    );

    if (!fillsResponse.success) {
        return null;
    }

    // response will contain all transactions previous to epoch, txOrdinal passed in params
    const filteredFills = fillsResponse.value.filter((it) => it.epochId === epochId && it.txOrdinal === txOrdinal);

    if (filteredFills.length === 0) {
        return null;
    }
    const orderHashes = filteredFills.map((it) => it.makerOrderHash);
    const orderIntentsUrl = `/order_intents?${orderHashes.map((o) => `orderHash=${o}`).join('&')}`;
    const ordersResponse = await statsApiListFetcher<OrderIntentResponseItem>(orderIntentsUrl, host);
    if (!ordersResponse.success) {
        return null;
    }

    if (ordersResponse.value.length === 0) {
        return null;
    }

    try {
        const affectedList: {
            traderAddress: string;
            strategyId: string;
            orderHash: string;
            symbol: string;
            price: BigNumber;
            amount: BigNumber;
            side: ParamOrderSide;
        }[] = filteredFills.map((it) => {
            const order = ordersResponse.value.find((o) => o.orderHash === it.makerOrderHash);
            if (order === undefined) {
                throw new Error('Could not find order');
            }
            return {
                traderAddress: it.makerOrderTrader,
                strategyId: order.strategyId,
                orderHash: it.makerOrderHash,
                symbol: it.symbol,
                price: new BigNumber(it.price ? it.price : order.price),
                amount: new BigNumber(it.amount),
                side: order.side,
            };
        });
        return {
            timestamp: fillsResponse.timestamp,
            value: {
                epochId,
                txOrdinal,
                affected: affectedList,
            },
        };
    } catch (error) {
        return null;
    }
}
