import BigNumber from 'bignumber.js';

import { FillsResponseItem, ParamFillReason } from './types';

export const convertItemToFillResponse = (it): FillsResponseItem => {
    return {
        ...it,
        amount: new BigNumber(it.amount ?? 0),
        fill: new BigNumber(it.fill ?? 0),
        price: new BigNumber(it.price ?? 0),
        makerFeeUSDC: new BigNumber(it.makerFeeUSDC ?? 0),
        makerFeeDDX: new BigNumber(it.makerFeeDDX ?? 0),
        takerFeeUSDC: new BigNumber(it.takerFeeUSDC ?? 0),
        takerFeeDDX: new BigNumber(it.takerFeeDDX ?? 0),
        reason: it.reason as ParamFillReason,
        makerRealizedPnl: new BigNumber(it.makerRealizedPnl ?? 0),
        takerRealizedPnl: new BigNumber(it.takerRealizedPnl ?? 0),
    };
};
