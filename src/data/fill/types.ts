import BigNumber from 'bignumber.js';

import { OrderEntity, ParamOrderSide } from '../order/types';

export type TxFillTakerEntity = {
    epochId: string;
    txOrdinal: string;
    takerOrder: OrderEntity;
    takerAvgPrice: BigNumber;
    fees: BigNumber;
    feesCurrency: string;
};

export type TxFillAffectedMakerEntity = {
    epochId: string;
    txOrdinal: string;
    affected: {
        traderAddress: string;
        strategyId: string;
        orderHash: string;
        symbol: string;
        price: BigNumber;
        amount: BigNumber;
        side: ParamOrderSide;
    }[];
};

export type FillsResponseItem = {
    epochId: string;
    txOrdinal: string;
    ordinal: string;
    makerOrderHash: string;
    makerOrderTrader: string;
    makerOrderStrategyIdHash: string;
    amount: BigNumber;
    symbol: string;
    price: BigNumber;
    makerFeeUSDC: BigNumber;
    makerFeeDDX: BigNumber;
    makerRealizedPnl: BigNumber;
    reason: ParamFillReason;
    takerOrderHash: string;
    takerOrderTrader: string;
    takerOrderStrategyIdHash: string;
    takerFeeUSDC: BigNumber;
    takerFeeDDX: BigNumber;
    takerRealizedPnl: BigNumber;
    liquidatedTrader: string;
    liquidatedStrategyIdHash: string;
    createdAt: string;
};

export enum ParamFillReason {
    Fill = 0,
    Liquidation = 1,
    Cancel = 2,
}
