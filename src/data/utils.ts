import { FetcherOptions } from '@utils';

import { Jsonify, StatsAPIResponse } from './types';

export function getDeploymentEnvironmentName(host: string): string {
    const match = host.match(/^(.*?)-/);
    const environment = match ? match[1] : '';
    if (environment === 'derivadex' || host.startsWith('localhost:')) {
        return 'testnet';
    }
    return environment === '' ? 'mainnet' : environment;
}

export function getDeploymentEnvironmentDomain(host: string): string {
    const environment = getDeploymentEnvironmentName(host);
    const statsSubdomain = environment === 'mainnet' ? 'exchange' : environment;
    const topLevelDomain = environment === 'mainnet' ? 'com' : 'io';
    return `${statsSubdomain}.derivadex.${topLevelDomain}`;
}

export function getStatsApiBaseUrl(host: string): string {
    return `https://${getDeploymentEnvironmentDomain(host)}/stats/api/v1`;
}

/**
 * This function requires the HMAC_SECRET env var in production environments,
 * so that the requests don't get blocked
 * (we have ip blocking policy in place to limit accessing from certain countries).
 */
export async function statsApiFetcherBase<T>(
    path: string,
    host: string,
    options?: FetcherOptions,
): Promise<StatsAPIResponse<Jsonify<T>>> {
    // This request should be cached with a lifetime of 30 seconds.
    let url = `${getStatsApiBaseUrl(host)}${path}`;

    if (process.env.HMAC_SECRET) {
        const urlBuilder = new URL(url);
        const message = urlBuilder.pathname + urlBuilder.search;
        const unixTimestamp = Math.floor(Date.now() / 1000);

        const encoder = new TextEncoder();
        const key = await crypto.subtle.importKey(
            'raw',
            encoder.encode(process.env.HMAC_SECRET),
            { name: 'HMAC', hash: 'SHA-256' },
            false,
            ['sign', 'verify'],
        );
        const signature = await crypto.subtle.sign('HMAC', key, encoder.encode(message + unixTimestamp));
        const digest = Buffer.from(String.fromCharCode(...new Uint8Array(signature)), 'binary').toString('base64');

        urlBuilder.searchParams.append('verify', `${unixTimestamp}-${digest}`);
        url = urlBuilder.href;
    }

    return await apiFetcherBase<T>(url, options);
}

export async function localApiFetcherBase<T>(
    path: string,
    host: string,
    options?: FetcherOptions,
): Promise<StatsAPIResponse<Jsonify<T>>> {
    // This request should be cached with a lifetime of 30 seconds.
    let url = `${`https://${host}/api`}${path}`;
    if (host.includes('localhost')) {
        url = `${`http://${host}/api`}${path}`;
    }
    return await apiFetcherBase<T>(url, options);
}

export async function apiFetcherBase<T>(url: string, options?: FetcherOptions): Promise<StatsAPIResponse<Jsonify<T>>> {
    // This request should be cached with a lifetime of 30 seconds.
    const res = await fetch(url, { next: { revalidate: options?.revalidate ?? 30 } });
    if (!res.ok) {
        return { success: false };
    }
    const data = (await res.json()) as StatsAPIResponse<Jsonify<T>>;
    return data;
}

export async function internalFetcher<T>(path: string): Promise<T | null> {
    const res = await fetch(path);
    if (!res.ok) {
        return null;
    }
    return await res.json();
}

export function buildUrl(
    url: string,
    params: { [key: string]: string | number | string[] },
    paginationParams?: { [key: string]: string | number },
) {
    const finalParams = { ...params, ...paginationParams };
    const fullUrl =
        url +
        '?' +
        Object.entries(finalParams)
            .map(([key, value]) =>
                Array.isArray(value) ? value.map((it) => `${key}=${it}`).join('&') : `${key}=${value}`,
            )
            .join('&');
    return fullUrl;
}
