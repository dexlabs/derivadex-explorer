// See https://effectivetypescript.com/2020/04/09/jsonify/
/* eslint-disable  @typescript-eslint/ban-types */
export type Jsonify<T> = T extends { toJSON(): infer U }
    ? U
    : T extends object
    ? {
          [k in keyof T]: Jsonify<T[k]>;
      }
    : T;
/* eslint-enable  @typescript-eslint/ban-types */

export type StatsAPIResponse<T> = StatsAPISingleResponse<T> | StatsAPIListResponse<T> | StatsAPITimeseriesResponse<T>;

export type StatsAPIListResponse<T> = StatsAPISuccessListResponse<T> | StatsAPIErrorResponse;

export type StatsAPISuccessListResponse<T> = StatsAPISuccessResponse & {
    /**
     * The value of the response
     */
    value: Array<T>;
};

export type StatsAPISuccessResponse = {
    /**
     * Whether the request was successful
     * @const true
     */
    success: true;
    /**
     * The timestamp of the response
     * @examples [1673031089]
     */
    timestamp: number;
};

export type StatsAPIErrorResponse = {
    success: false;
    errorMsg?: string;
};

export type StatsAPISingleResponse<T> = StatsAPISuccessSingleResponse<T> | StatsAPIErrorResponse;

export type StatsAPISuccessSingleResponse<T> = StatsAPISuccessResponse & {
    value: T;
};

export type StatsAPISuccessTimeseriesResponse<T> = StatsAPISuccessListResponse<T> & {
    /**
     * Pointer for the epoch boundary of the next page. Will return null when
     * there is no more data left to fetch.
     * @examples [100, null]
     */
    nextEpoch: number | null;
    /**
     * Pointer for the txOrdinal boundary of the next page. Will return null when
     * there is no more data left to fetch.
     * @examples [3432, null]
     */
    nextTxOrdinal: number | null;
    /**
     * Pointer for the ordinal boundary of the next page. Will return null when
     * there is no more data left to fetch.
     * @examples [100, null]
     */
    nextOrdinal: number | null;
};

export type StatsAPITimeseriesResponse<T> = StatsAPISuccessTimeseriesResponse<T> | StatsAPIErrorResponse;
