import BigNumber from 'bignumber.js';

export type StrategyHistoryEntity = {
    epochId: number;
    txOrdinal: number;
    event: ParamStrategyHistoryEvent;
    amount: BigNumber;
    collateralBalance: BigNumber;
    createdAt: Date;
};

export enum ParamStrategyHistoryEvent {
    Deposit,
    Withdraw,
    Withdraw_Intent,
    Funding_Payment,
    Pnl_Settlement,
    Realized_Pnl,
    Fees,
    Liquidation,
    Auto_Deleveraging,
}
