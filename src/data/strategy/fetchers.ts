import { statsApiListFetcher } from '@utils';
import BigNumber from 'bignumber.js';

import { AdlResponseItem, LiquidationsResponseItem, StrategyUpdateResponseItem } from '@/types/api';

import { FillsResponseItem } from '../fill/types';
import { ParamStrategyHistoryEvent, StrategyHistoryEntity } from './types';

export async function statsApiStrategyHistoryFetcher(
    trader: string,
    strategyId: string,
    limit: number,
    host: string,
    pagination?: { epoch: string; txOrdinal: string },
): Promise<StrategyHistoryEntity[] | null> {
    const paginationUrl = pagination ? `&epoch=${pagination.epoch}&txOrdinal=${pagination.txOrdinal}` : '';
    const [updatesResponse, liquidationResponse, adlResponse, fillsResponse] = await Promise.all([
        statsApiListFetcher<StrategyUpdateResponseItem>(
            `/account/${trader}/strategy/${strategyId}/strategy_updates?order=desc&limit=${limit}${paginationUrl}`,
            host,
        ),
        statsApiListFetcher<LiquidationsResponseItem>(
            `/account/${trader}/strategy/${strategyId}/liquidations?order=desc&limit=${limit}${paginationUrl}`,
            host,
        ),
        statsApiListFetcher<AdlResponseItem>(
            `/account/${trader}/strategy/${strategyId}/adl?order=desc&limit=${limit}${paginationUrl}`,
            host,
        ),
        statsApiListFetcher<FillsResponseItem>(
            `/account/${trader}/strategy/${strategyId}/order_updates?order=desc&fillReason=0&limit=${limit}${paginationUrl}`,
            host,
        ),
    ]);
    if (!updatesResponse.success || !adlResponse.success || !liquidationResponse.success || !fillsResponse.success) {
        return null;
    }

    const list: StrategyHistoryEntity[] = [];

    for (let i = 0; i < updatesResponse.value.length; i++) {
        const updateItem = updatesResponse.value[i];
        const strategyHistoryEntity = {
            epochId: +updateItem.epochId,
            txOrdinal: +updateItem.txOrdinal,
            event: updateItem.kind.valueOf() as ParamStrategyHistoryEvent,
            amount: new BigNumber(updateItem.amount),
            collateralBalance: new BigNumber(updateItem.newAvailCollateral),
            createdAt: new Date(updateItem.createdAt),
        };
        list.push(strategyHistoryEntity);
    }

    for (let i = 0; i < liquidationResponse.value.length; i++) {
        const liquidationItem = liquidationResponse.value[i];
        const strategyHistoryEntity = {
            epochId: +liquidationItem.epochId,
            txOrdinal: +liquidationItem.txOrdinal,
            event: ParamStrategyHistoryEvent.Liquidation,
            amount: new BigNumber(0),
            collateralBalance: new BigNumber(0),
            createdAt: new Date(liquidationItem.createdAt),
        };
        list.push(strategyHistoryEntity);
    }

    for (let i = 0; i < adlResponse.value.length; i++) {
        const adlItem = adlResponse.value[i];
        // Only add adl entries if trader is the adl trader
        // Adl Responses caused by trader being the liquidated trader should be ignored,
        // as that is already considered via liquidations endpoint
        if (adlItem.adlTrader.endsWith(trader.substring(2))) {
            const strategyHistoryEntity = {
                epochId: +adlItem.epochId,
                txOrdinal: +adlItem.txOrdinal,
                event: ParamStrategyHistoryEvent.Auto_Deleveraging,
                // Whats the value of change? ignore for now
                amount: new BigNumber(0),
                collateralBalance: new BigNumber(0),
                createdAt: new Date(adlItem.createdAt),
            };
            list.push(strategyHistoryEntity);
        }
    }

    for (let i = 0; i < fillsResponse.value.length; i++) {
        const fillsItem = fillsResponse.value[i];
        // Only takers pay fees, only usdc fees for strategy history
        if (fillsItem.takerOrderTrader.endsWith(trader.substring(2)) && !fillsItem.takerFeeDDX) {
            const strategyHistoryEntity = {
                epochId: +fillsItem.epochId,
                txOrdinal: +fillsItem.txOrdinal,
                event: ParamStrategyHistoryEvent.Fees,
                amount: new BigNumber('-' + fillsItem.takerFeeUSDC),
                collateralBalance: new BigNumber(0),
                createdAt: new Date(fillsItem.createdAt),
            };
            list.push(strategyHistoryEntity);
        }
        // Create entries for Realized Pnl too, need to identify first which trader it it, the taker or the maker
        if (fillsItem.takerOrderTrader.endsWith(trader.substring(2)) && fillsItem.takerRealizedPnl !== '0') {
            const strategyHistoryEntity = {
                epochId: +fillsItem.epochId,
                txOrdinal: +fillsItem.txOrdinal,
                event: ParamStrategyHistoryEvent.Realized_Pnl,
                amount: new BigNumber(fillsItem.takerRealizedPnl),
                collateralBalance: new BigNumber(0),
                createdAt: new Date(fillsItem.createdAt),
            };
            list.push(strategyHistoryEntity);
        } else if (fillsItem.makerOrderTrader.endsWith(trader.substring(2)) && fillsItem.makerRealizedPnl !== '0') {
            const strategyHistoryEntity = {
                epochId: +fillsItem.epochId,
                txOrdinal: +fillsItem.txOrdinal,
                event: ParamStrategyHistoryEvent.Realized_Pnl,
                amount: new BigNumber(fillsItem.makerRealizedPnl),
                collateralBalance: new BigNumber(0),
                createdAt: new Date(fillsItem.createdAt),
            };
            list.push(strategyHistoryEntity);
        }
    }

    const response = list
        .sort((a: StrategyHistoryEntity, b: StrategyHistoryEntity) => {
            if (b.epochId === a.epochId) {
                return b.txOrdinal - a.txOrdinal;
            }
            return b.epochId - a.epochId;
        })
        .slice(0, limit);

    return response;
}
