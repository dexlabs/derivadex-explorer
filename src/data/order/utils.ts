import { FillsResponseItem, ParamFillReason } from '../fill/types';
import { Jsonify } from '../types';

export function orderSideEnumToString(side: number) {
    if (side === 1) {
        return 'Long';
    } else if (side === 2) {
        return 'Short';
    }
    return 'None';
}

export function orderTypeEnumToString(orderType: number) {
    if (orderType === 0) {
        return 'Limit';
    } else if (orderType === 1) {
        return 'Market';
    }
    return 'Stop';
}

export function fetchTradedBy(trader: string, trade: Jsonify<FillsResponseItem>) {
    if ([ParamFillReason.Cancel, ParamFillReason.Liquidation].includes(trade.reason)) {
        return undefined;
    }
    const takerOrderTraderWithoutDescriminant = `0x${trade.takerOrderTrader?.substring(4)}`;
    if (trader.toLowerCase() === takerOrderTraderWithoutDescriminant.toLowerCase()) {
        return `0x${trade.makerOrderTrader?.substring(4)}`;
    }
    return `0x${trade.takerOrderTrader?.substring(4)}`;
}

export function getTraderAndStrategyForOrderHash(orderHash: string, trade: Jsonify<FillsResponseItem>) {
    if (orderHash.toLowerCase() === trade.takerOrderHash.toLowerCase()) {
        return { trader: trade.takerOrderTrader, strategyId: 'main' };
    }
    return { trader: trade.makerOrderTrader, strategyId: 'main' };
}

export function orderRejectionReasonEnumToString(reason: number) {
    if (reason === 0) {
        return 'Self Match';
    } else if (reason === 1) {
        return 'Solvency Guard';
    } else if (reason === 2) {
        return 'Market Order Not Fully Filled';
    }
    return 'None';
}
