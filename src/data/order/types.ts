import BigNumber from 'bignumber.js';

export type OrderBookResponseItem = {
    orderHash: string;
    symbol: string;
    side: ParamOrderSide;
    originalAmount: BigNumber;
    amount: BigNumber;
    price: BigNumber;
    traderAddress: string;
    strategyId: string;
    bookOrdinal: string;
};

export enum ParamOrderSide {
    Bid = 0,
    Ask = 1,
}

export type OrderEntity = {
    orderHash: string;
    epochId: string;
    txOrdinal: string;
    symbol: string;
    side: ParamOrderSide;
    amount: BigNumber;
    price: BigNumber;
    traderAddress: string;
    strategyId: string;
    orderType: ParamOrderType;
    remainingAmount: BigNumber;
    rejectedAmount: BigNumber;
    rejectedReason: OrderRejectReason;
    createdAt: Date;
};

export type OrderHistoryItem = {
    epochId: string;
    txOrdinal: string;
    ordinal: string;
    symbol: string;
    traderAddress: string;
    tradedBy: string | undefined;
    event: ParamOrderHistoryEvent;
    filledAmount: BigNumber;
    remainingAmount: BigNumber;
    createdAt: Date;
};

export enum ParamOrderHistoryEvent {
    Fill = 0,
    Liquidation = 1,
    Cancel = 2,
    Placed = 3,
}

export type OrderIntentResponseItem = {
    epochId: string;
    txOrdinal: string;
    orderHash: string;
    symbol: string;
    side: ParamOrderSide;
    amount: BigNumber;
    price: BigNumber;
    traderAddress: string;
    strategyId: string;
    orderType: ParamOrderType;
    stopPrice: BigNumber;
    nonce: string;
    signature: string;
    createdAt: string;
};

export type OrderRejectionResponseItem = {
    epochId: string;
    requestIndex: string;
    orderHash: string;
    symbol: string;
    amount: BigNumber;
    reason: OrderRejectReason;
    traderAddress: string;
    strategyId: string;
};

export enum OrderRejectReason {
    SelfMatch = 0,
    Solvency = 1,
    MarketOrderNotFullyFilled = 2,
    NoOrderRejection = 3,
}

export enum ParamOrderType {
    Limit = 0,
    Market = 1,
    Stop = 2,
}
