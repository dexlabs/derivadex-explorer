import { statsApiListFetcher } from '@utils';
import BigNumber from 'bignumber.js';

import { FillsResponseItem } from '../fill/types';
import { StatsAPISingleResponse } from '../types';
import {
    OrderBookResponseItem,
    OrderEntity,
    OrderHistoryItem,
    OrderIntentResponseItem,
    OrderRejectionResponseItem,
    OrderRejectReason,
    ParamOrderHistoryEvent,
    ParamOrderSide,
    ParamOrderType,
} from './types';

export async function statsApiOrderDetailsFetcher(
    trader: string,
    orderHash: string,
    host: string,
): Promise<StatsAPISingleResponse<OrderEntity>> {
    const [orderIntents, orderRejections] = await Promise.all([
        statsApiListFetcher<OrderIntentResponseItem>(`/order_intents?orderHash=${orderHash}`, host),
        statsApiListFetcher<OrderRejectionResponseItem>(`/order_rejections?orderHash=${orderHash}`, host),
    ]);

    if (!orderIntents.success || !orderRejections.success) {
        return { success: false };
    }

    const [orderIntent] = orderIntents.value;
    const [orderRejection] =
        orderRejections.value.length > 0
            ? orderRejections.value
            : [{ reason: OrderRejectReason.NoOrderRejection, amount: new BigNumber(0) }];

    if (orderIntent === undefined) {
        return { success: false };
    }

    const orderBook = await statsApiListFetcher<OrderBookResponseItem>(
        `/account/${trader}/strategy/${orderIntent.strategyId}/order_book`,
        host,
    );
    if (!orderBook.success) {
        return { success: false };
    }

    const orderBookMatch = orderBook.value.find((it) => it.orderHash === orderIntent.orderHash);

    const orderDisplay: OrderEntity = {
        orderHash: orderIntent.orderHash,
        epochId: orderIntent.epochId,
        txOrdinal: orderIntent.txOrdinal,
        symbol: orderIntent.symbol,
        side: orderIntent.side as ParamOrderSide,
        amount: new BigNumber(orderIntent.amount),
        price: new BigNumber(orderIntent.price),
        traderAddress: orderIntent.traderAddress,
        strategyId: orderIntent.strategyId,
        orderType: orderIntent.orderType as ParamOrderType,
        remainingAmount: new BigNumber(orderBookMatch?.amount || '0'),
        rejectedAmount: new BigNumber(orderRejection?.amount || '0'),
        rejectedReason: orderRejection?.reason as OrderRejectReason,
        createdAt: new Date(orderIntent.createdAt),
    };

    const response = {
        success: true,
        timestamp: orderIntents.timestamp,
        value: orderDisplay,
    };

    return response;
}

export async function statsApiOrderHistoryFetcher(
    orderHash: string,
    limit: number,
    host: string,
    lookbackAmount?: string,
    pagination?: { epoch: string; txOrdinal: string; ordinal: string },
): Promise<OrderHistoryItem[] | null> {
    const paginationUrl = pagination
        ? `&epoch=${pagination.epoch}&txOrdinal=${pagination.txOrdinal}&ordinal=${pagination.ordinal}`
        : '';
    const fillsResponse = await statsApiListFetcher<FillsResponseItem>(
        `/order_updates?orderHash=${orderHash}&limit=${limit}${paginationUrl}`,
        host,
    );

    if (!fillsResponse.success) {
        return null;
    }

    const list: OrderHistoryItem[] = [];
    let amountLeft = new BigNumber(0);

    if (!pagination) {
        const orderIntentResponse = await statsApiListFetcher<OrderIntentResponseItem>(
            `/order_intents?orderHash=${orderHash}`,
            host,
        );
        if (!orderIntentResponse.success) {
            return null;
        }
        const [orderIntent] = orderIntentResponse.value;
        if (orderIntent === undefined) {
            return null;
        }
        list.push({
            epochId: orderIntent.epochId,
            txOrdinal: orderIntent.txOrdinal,
            ordinal: '0',
            symbol: orderIntent.symbol,
            traderAddress: orderIntent.traderAddress,
            tradedBy: undefined,
            event: ParamOrderHistoryEvent.Placed,
            filledAmount: new BigNumber(0),
            remainingAmount: new BigNumber(orderIntent.amount),
            createdAt: new Date(orderIntent.createdAt),
        });
        amountLeft = new BigNumber(orderIntent.amount);
    }

    if (lookbackAmount) {
        amountLeft = new BigNumber(lookbackAmount);
    }

    for (let i = 0; i < fillsResponse.value.length; i++) {
        const fillItem = fillsResponse.value[i];
        amountLeft = amountLeft.minus(fillItem.amount);
        const orderHistoryItem = {
            epochId: fillItem.epochId,
            txOrdinal: fillItem.txOrdinal,
            ordinal: fillItem.ordinal,
            symbol: fillItem.symbol,
            traderAddress: fillItem.makerOrderTrader,
            tradedBy: fillItem.takerOrderTrader,
            event: fillItem.reason.valueOf() as ParamOrderHistoryEvent,
            filledAmount: new BigNumber(fillItem.amount),
            remainingAmount: new BigNumber(amountLeft),
            createdAt: new Date(fillItem.createdAt),
        };
        list.push(orderHistoryItem);
    }

    return list;
}

export async function statsApiTxOrderCancelFetcher(
    epochId: string,
    txOrdinal: string,
    host: string,
): Promise<{ timestamp: number; value: OrderEntity[] } | null> {
    const fillsResponse = await statsApiListFetcher<FillsResponseItem>(
        `/order_updates?order=desc&limit=50&epoch=${epochId}&txOrdinal=${+txOrdinal + 1}`,
        host,
    );

    if (!fillsResponse.success) {
        return null;
    }

    const filteredFills = fillsResponse.value.filter((it) => it.epochId === epochId && it.txOrdinal === txOrdinal);

    const orderHashes = filteredFills.map((it) => it.makerOrderHash);
    const orderIntentsUrl = `/order_intents?${orderHashes.map((o) => `orderHash=${o}`).join('&')}`;
    const ordersResponse = await statsApiListFetcher<OrderIntentResponseItem>(orderIntentsUrl, host);

    if (!ordersResponse.success) {
        return null;
    }

    if (ordersResponse.value.length !== filteredFills.length) {
        return null;
    }

    try {
        const list: OrderEntity[] = filteredFills.map((it) => {
            const order = ordersResponse.value.find((o) => o.orderHash === it.makerOrderHash);
            if (order === undefined) {
                throw new Error('Could not find order');
            }
            return {
                epochId: it.epochId,
                txOrdinal: it.txOrdinal,
                orderHash: it.makerOrderHash,
                traderAddress: it.makerOrderTrader,
                price: new BigNumber(order.price),
                amount: new BigNumber(order.amount),
                remainingAmount: new BigNumber(it.amount),
                rejectedAmount: new BigNumber(0),
                rejectedReason: OrderRejectReason.NoOrderRejection,
                side: order.side,
                strategyId: 'main',
                symbol: it.symbol,
                orderType: order.orderType,
                createdAt: new Date(order.createdAt),
            };
        });
        return {
            timestamp: fillsResponse.timestamp,
            value: list,
        };
    } catch (error) {
        return null;
    }
}

export async function statsApiTxCancelDetailsFetcher(
    epochId: string,
    txOrdinal: string,
    host: string,
): Promise<{ timestamp: number; value: { traderAddress: string; strategyId: string; symbol: string } } | null> {
    const fillsResponse = await statsApiListFetcher<FillsResponseItem>(
        `/order_updates?order=desc&limit=50&epoch=${epochId}&txOrdinal=${+txOrdinal + 1}`,
        host,
    );

    if (!fillsResponse.success) {
        return null;
    }

    // response will contain all transactions previous to epoch, txOrdinal passed in params
    const filteredFills = fillsResponse.value.filter((it) => it.epochId === epochId && it.txOrdinal === txOrdinal);

    if (filteredFills.length === 0) {
        return null;
    }

    const [fill] = filteredFills;

    if (fill === undefined) {
        return null;
    }

    return {
        timestamp: fillsResponse.timestamp,
        value: {
            traderAddress: fill.makerOrderTrader,
            strategyId: 'main',
            symbol: fill.symbol,
        },
    };
}
