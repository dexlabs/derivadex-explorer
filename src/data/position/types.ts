import BigNumber from 'bignumber.js';

import { ParamPositionSide } from '@/types/api';

export enum ParamOrderHistoryEvent {
    Fill = 0,
    Liquidation = 1,
    Cancel = 2,
    Placed = 3,
}

export type PositionHistoryItem = {
    epochId: string;
    txOrdinal: string;
    ordinal: string;
    event: ParamPositionHistoryEvent;
    side: ParamPositionSide;
    amount: BigNumber;
    balance: BigNumber;
    createdAt: Date;
};

export enum ParamPositionHistoryEvent {
    Size_Increase = 0,
    Size_Decrease = 1,
    Crossover = 2,
    Close = 3,
    Open = 4,
}
