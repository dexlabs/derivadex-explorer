import BigNumber from 'bignumber.js';

import { ParamPositionSide } from '@/types/api';

import { FillsResponseItem, ParamFillReason } from '../fill/types';
import { OrderIntentResponseItem, ParamOrderSide } from '../order/types';
import { Jsonify } from '../types';
import { ParamPositionHistoryEvent } from './types';

export function isPositionAndOrderSameSide(positionSide: ParamPositionSide, orderSide: ParamOrderSide) {
    if (positionSide === ParamPositionSide.Long) {
        return orderSide === ParamOrderSide.Bid ? true : false;
    } else if (positionSide === ParamPositionSide.Short) {
        return orderSide === ParamOrderSide.Ask ? true : false;
    } else return false;
}

export function isFillAmountBiggerPositionBalance(balance: BigNumber, fillAmount: BigNumber) {
    if (balance.lt(fillAmount)) return true;
    return false;
}

export function invertPositionSide(positionSide: ParamPositionSide) {
    if (positionSide === ParamPositionSide.Long) return ParamPositionSide.Short;
    return ParamPositionSide.Long;
}

export function invertOrderSide(orderSide: ParamOrderSide) {
    if (orderSide === ParamOrderSide.Ask) return ParamOrderSide.Bid;
    return ParamOrderSide.Ask;
}

export function getOrderHashFromFill(trader: string, strategyIdHash, trade: Jsonify<FillsResponseItem>) {
    if (trade.reason === ParamFillReason.Liquidation) {
        return trade.makerOrderHash;
    }
    const takerOrderTraderWithoutDescriminant = `0x${trade.takerOrderTrader?.substring(4)}`;
    if (
        trader.toLowerCase() === takerOrderTraderWithoutDescriminant.toLowerCase() &&
        strategyIdHash === trade.takerOrderStrategyIdHash
    ) {
        return trade.takerOrderHash;
    }
    return trade.makerOrderHash;
}

export function formatPositionHistoryEvent(event: ParamPositionHistoryEvent) {
    const text = Object.values(ParamPositionHistoryEvent)[event].toString();
    return text.replace('_', ' ');
}

export function isTraderLiquidated(trader: string, trade: Jsonify<FillsResponseItem>) {
    if (trade.reason === ParamFillReason.Liquidation) {
        return `0x${trade.liquidatedTrader?.substring(4)}`;
    }
    return null;
}

export function getOrderSideFromFill(
    trader: string,
    strategyIdHash: string,
    trade: Jsonify<FillsResponseItem>,
    orders: Jsonify<OrderIntentResponseItem>[],
) {
    if (trade.reason === ParamFillReason.Liquidation) {
        const orderHash = trade.makerOrderHash;
        const order = orders.find((it) => it.orderHash === orderHash);
        if (order !== undefined) {
            return invertOrderSide(order.side);
        }
    }
    const takerOrderTraderWithoutDescriminant = `0x${trade.takerOrderTrader?.substring(4)}`;
    if (
        trader.toLowerCase() === takerOrderTraderWithoutDescriminant.toLowerCase() &&
        strategyIdHash === trade.takerOrderStrategyIdHash
    ) {
        const orderHash = trade.takerOrderHash;
        const order = orders.find((it) => it.orderHash === orderHash);
        if (order !== undefined) {
            return order.side;
        }
    } else {
        const orderHash = trade.makerOrderHash;
        const order = orders.find((it) => it.orderHash === orderHash);
        if (order !== undefined) {
            return order.side;
        }
    }
    return ParamOrderSide.Bid;
}
