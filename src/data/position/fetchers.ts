import { statsApiListFetcher } from '@utils';
import BigNumber from 'bignumber.js';

import { AdlResponseItem, ParamPositionSide, PositionResponseItem } from '@/types/api';

import { FillsResponseItem, ParamFillReason } from '../fill/types';
import { OrderIntentResponseItem, ParamOrderSide } from '../order/types';
import { ParamPositionHistoryEvent, PositionHistoryItem } from './types';
import {
    getOrderHashFromFill,
    getOrderSideFromFill,
    invertPositionSide,
    isFillAmountBiggerPositionBalance,
    isPositionAndOrderSameSide,
    isTraderLiquidated,
} from './utils';

export async function statsApiPositionHistoryFetcher(
    trader: string,
    strategyId: string,
    symbol: string,
    limit: number,
    host: string,
    lookbackBalance?: string,
    pagination?: { epoch: string; txOrdinal: string; ordinal: string },
): Promise<PositionHistoryItem[] | null> {
    const paginationUrl = pagination
        ? `&epoch=${pagination.epoch}&txOrdinal=${pagination.txOrdinal}&ordinal=${pagination.ordinal}`
        : '';
    const [positionResponse, fillsResponse, adlResponse] = await Promise.all([
        statsApiListFetcher<PositionResponseItem>(
            `/account/${trader}/strategy/${strategyId}/positions?symbol=${symbol}${paginationUrl}`,
            host,
        ),
        statsApiListFetcher<FillsResponseItem>(
            `/account/${trader}/strategy/${strategyId}/order_updates?order=desc&limit=${limit}&symbol=${symbol}&fillReason=0&fillReason=1${paginationUrl}`,
            host,
        ),
        statsApiListFetcher<AdlResponseItem>(
            `/account/${trader}/strategy/${strategyId}/adl?order=desc&limit=${limit}&symbol=${symbol}${paginationUrl}`,
            host,
        ),
    ]);

    if (!positionResponse.success || !fillsResponse.success || !adlResponse.success) {
        return null;
    }
    let [position] = positionResponse.value;

    const orderHashes = fillsResponse.value.map((it) => getOrderHashFromFill(trader, position.strategyIdHash, it));
    const orderIntentsUrl = `/order_intents?${orderHashes.map((o) => `orderHash=${o}`).join('&')}`;
    const ordersResponse = await statsApiListFetcher<OrderIntentResponseItem>(orderIntentsUrl, host);
    if (!ordersResponse.success) {
        return null;
    }

    const list: PositionHistoryItem[] = [];

    if (position === undefined) {
        position = {
            side: ParamPositionSide.None,
            balance: '0',
            trader,
            symbol,
            strategyIdHash: '',
            avgEntryPrice: '0',
            lastModifiedInEpoch: 0,
        };
    } else {
        position.balance = position.balance;
    }

    if (lookbackBalance) {
        position.balance = lookbackBalance;
    }

    const previousContext: { side: ParamPositionSide; balance: BigNumber; epochId: string; txOrdinal: string } = {
        side: position.side,
        balance: new BigNumber(position.balance),
        epochId: '',
        txOrdinal: '',
    };

    let adlIndex = 0;

    for (let i = 0; i < fillsResponse.value.length; i++) {
        const fillItem = fillsResponse.value[i];
        // Get corresponding order intent side that this trader used to get fills and position update
        const orderSide = getOrderSideFromFill(trader, position.strategyIdHash, fillItem, ordersResponse.value);

        const resultingBalance = previousContext.balance;
        const positionSide = previousContext.side;
        let fillAmount = new BigNumber(fillItem.amount);

        // Logic for Adl, assuming there is only one adl between fills
        // Maybe that needs to be rethink
        const adlEntry = adlResponse.value.length > adlIndex ? adlResponse.value[adlIndex] : null;
        if (adlEntry) {
            let entryToAdd = false;
            if (+adlEntry.epochId > +fillItem.epochId) {
                adlIndex++;
                entryToAdd = true;
            } else if (adlEntry.epochId === fillItem.epochId) {
                if (+adlEntry.txOrdinal > +fillItem.txOrdinal) {
                    adlIndex++;
                    entryToAdd = true;
                }
            }
            if (entryToAdd) {
                const item = {
                    epochId: adlEntry.epochId,
                    txOrdinal: adlEntry.txOrdinal,
                    ordinal: adlEntry.ordinal,
                    event: ParamPositionHistoryEvent.Size_Decrease,
                    side: adlEntry.side,
                    amount: new BigNumber(adlEntry.amount),
                    balance: resultingBalance.absoluteValue(),
                    createdAt: new Date(adlEntry.createdAt),
                };
                list.push(item);
                previousContext.balance = resultingBalance.plus(adlEntry.amount).abs();
            }
        }

        // Continue with Fill Logic
        let event = ParamPositionHistoryEvent.Close;
        if (fillItem.reason === ParamFillReason.Liquidation && isTraderLiquidated(trader, fillItem)) {
            event = ParamPositionHistoryEvent.Close;
            previousContext.balance = resultingBalance.plus(fillAmount);
            previousContext.side = orderSide === ParamOrderSide.Bid ? ParamPositionSide.Long : ParamPositionSide.Short;
            fillAmount = fillAmount.multipliedBy(-1);
        } else {
            if (isFillAmountBiggerPositionBalance(resultingBalance, fillAmount)) {
                if (resultingBalance.isZero()) {
                    event = ParamPositionHistoryEvent.Close;
                    previousContext.balance = resultingBalance.minus(fillAmount).abs();
                    previousContext.side =
                        orderSide === ParamOrderSide.Bid ? ParamPositionSide.Short : ParamPositionSide.Long;
                    fillAmount = fillAmount.multipliedBy(-1);
                } else {
                    if (isPositionAndOrderSameSide(positionSide, orderSide)) {
                        event = ParamPositionHistoryEvent.Crossover;
                        previousContext.balance = resultingBalance.minus(fillAmount).abs();
                        previousContext.side = invertPositionSide(positionSide);
                    } else {
                        event = ParamPositionHistoryEvent.Size_Decrease;
                        previousContext.balance = resultingBalance.plus(fillAmount).abs();
                        fillAmount = fillAmount.multipliedBy(-1);
                    }
                }
            } else if (isPositionAndOrderSameSide(positionSide, orderSide)) {
                previousContext.balance = resultingBalance.minus(fillAmount);
                previousContext.side = positionSide;
                if (previousContext.balance.isZero()) {
                    event = ParamPositionHistoryEvent.Open;
                } else {
                    event = ParamPositionHistoryEvent.Size_Increase;
                }
            } else {
                event = ParamPositionHistoryEvent.Size_Decrease;
                previousContext.balance = resultingBalance.plus(fillAmount);
                previousContext.side = positionSide;
                fillAmount = fillAmount.multipliedBy(-1);
            }
        }

        const item = {
            epochId: fillItem.epochId,
            txOrdinal: fillItem.txOrdinal,
            ordinal: fillItem.ordinal,
            event: event,
            side: event === ParamPositionHistoryEvent.Close ? ParamPositionSide.None : positionSide,
            amount: fillAmount,
            balance: resultingBalance.absoluteValue(),
            createdAt: new Date(fillItem.createdAt),
        };
        list.push(item);

        previousContext.epochId = item.epochId;
        previousContext.txOrdinal = item.txOrdinal;
    }

    const response = list.slice(0, limit);

    return response;
}
