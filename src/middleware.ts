import { NextRequest, NextResponse } from 'next/server';

// source: https://github.com/vercel/next.js/issues/43704#issuecomment-1411186664
export function middleware(request: NextRequest) {
    // Store current request url in a custom header
    const requestHeaders = new Headers(request.headers);
    requestHeaders.set('url', request.url);
    requestHeaders.set('host', request.nextUrl.host);
    return NextResponse.next({
        request: {
            // Apply new request headers
            headers: requestHeaders,
        },
    });
}
