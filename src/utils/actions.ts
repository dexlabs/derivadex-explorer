'use server';

import { Jsonify, StatsAPIListResponse } from 'src/data/types';
import { getDeploymentEnvironmentDomain, getStatsApiBaseUrl, statsApiFetcherBase } from 'src/data/utils';

export async function getStatsApiURLWithToken(path: string, host: string): Promise<string> {
    let url = `${getStatsApiBaseUrl(host)}${path}`;

    if (process.env.HMAC_SECRET) {
        const urlBuilder = new URL(url);
        const message = urlBuilder.pathname + urlBuilder.search;
        const unixTimestamp = Math.floor(Date.now() / 1000);

        const encoder = new TextEncoder();
        const key = await crypto.subtle.importKey(
            'raw',
            encoder.encode(process.env.HMAC_SECRET),
            { name: 'HMAC', hash: 'SHA-256' },
            false,
            ['sign', 'verify'],
        );
        const signature = await crypto.subtle.sign('HMAC', key, encoder.encode(message + unixTimestamp));
        const digest = Buffer.from(String.fromCharCode(...new Uint8Array(signature)), 'binary').toString('base64');

        urlBuilder.searchParams.append('verify', `${unixTimestamp}-${digest}`);
        url = urlBuilder.href;
    }
    return url;
}

export async function statsApiSpecialFetcher<T>(host: string, path: string): Promise<StatsAPIListResponse<Jsonify<T>>> {
    return (await statsApiFetcherBase<T>(path, host)) as StatsAPIListResponse<Jsonify<T>>;
}

export async function getRealtimeApiURLWithToken(host: string): Promise<string> {
    let url = `wss://${getDeploymentEnvironmentDomain(host)}/realtime-api`;

    if (process.env.HMAC_SECRET) {
        const urlBuilder = new URL(url);
        const message = urlBuilder.pathname + urlBuilder.search;
        const unixTimestamp = Math.floor(Date.now() / 1000);

        const encoder = new TextEncoder();
        const key = await crypto.subtle.importKey(
            'raw',
            encoder.encode(process.env.HMAC_SECRET),
            { name: 'HMAC', hash: 'SHA-256' },
            false,
            ['sign', 'verify'],
        );
        const signature = await crypto.subtle.sign('HMAC', key, encoder.encode(message + unixTimestamp));
        const digest = Buffer.from(String.fromCharCode(...new Uint8Array(signature)), 'binary').toString('base64');

        urlBuilder.searchParams.append('verify', `${unixTimestamp}-${digest}`);
        url = urlBuilder.href;
    }
    return url;
}
