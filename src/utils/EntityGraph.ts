export class EntityGraph {
    private nodes: Map<string, Set<string>>;
    private incomingEdges: Map<string, Set<string>>;
    private params: Map<string, string>;

    constructor() {
        this.nodes = new Map();
        this.incomingEdges = new Map();
        this.params = new Map();
        this.populateGraph();
    }

    addNode(nodeName: string, paramName): void {
        if (!this.nodes.has(nodeName)) {
            this.nodes.set(nodeName, new Set());
            this.incomingEdges.set(nodeName, new Set());
            this.params.set(nodeName, paramName);
        }
    }

    addEdge(fromNode: string, toNode: string): void {
        if (!this.nodes.has(fromNode) || !this.nodes.has(toNode)) {
            throw new Error('One or both of the nodes do not exist in the graph.');
        }

        const outgoingEdges = this.nodes.get(fromNode);
        const incomingEdges = this.incomingEdges.get(toNode);

        outgoingEdges!.add(toNode);
        incomingEdges!.add(fromNode);
    }

    getAdjacentNodes(nodeName: string): Set<string> | undefined {
        return this.nodes.get(nodeName);
    }

    getAncestors(nodeName: string): string[] | undefined {
        const visited: Set<string> = new Set();
        let longestPath: string[] = [];

        const dfs = (currentNode: string, path: string[]) => {
            visited.add(currentNode);

            const incomingEdges = this.incomingEdges.get(currentNode);
            if (incomingEdges) {
                for (const ancestor of incomingEdges) {
                    if (!visited.has(ancestor)) {
                        dfs(ancestor, [ancestor, ...path]);
                    }
                }
                if (incomingEdges.size === 0 && path.length > longestPath.length) {
                    // we can modify this later to include the shorter paths for the top level nav in the return
                    longestPath = path;
                }
            }
        };

        dfs(nodeName, [nodeName]);

        return longestPath;
    }

    getParam(nodeName: string): string | undefined {
        return this.params.get(nodeName);
    }

    populateGraph() {
        this.addNode('account', 'trader');
        this.addNode('strategy', 'strategyId');
        this.addNode('position', 'symbol');
        this.addNode('market', 'symbol');
        this.addNode('order', 'orderHash');
        this.addEdge('account', 'strategy');
        this.addEdge('strategy', 'position');
        this.addEdge('strategy', 'order');
        this.addEdge('market', 'position');
    }
}
