import BigNumber from 'bignumber.js';
import { Breadcrumb } from 'src/components/breadcrumbs';
import { FillsResponseItem } from 'src/data/fill/types';
import { Jsonify, StatsAPIListResponse, StatsAPISingleResponse, StatsAPITimeseriesResponse } from 'src/data/types';
import {
    apiFetcherBase,
    getDeploymentEnvironmentDomain,
    localApiFetcherBase,
    statsApiFetcherBase,
} from 'src/data/utils';

import { EntityGraph } from './EntityGraph';

export interface FetcherOptions {
    revalidate?: number;
}

export async function statsApiSingleFetcher<T>(
    path: string,
    host: string,
    options?: FetcherOptions,
): Promise<StatsAPISingleResponse<Jsonify<T>>> {
    return (await statsApiFetcherBase<T>(path, host, options)) as StatsAPISingleResponse<Jsonify<T>>;
}

export async function statsApiListFetcher<T>(
    path: string,
    host: string,
    options?: FetcherOptions,
): Promise<StatsAPIListResponse<Jsonify<T>>> {
    return (await statsApiFetcherBase<T>(path, host, options)) as StatsAPIListResponse<Jsonify<T>>;
}

export async function timeseriesStatsAPIHandler<T>(
    path: string,
    host: string,
    options?: FetcherOptions,
): Promise<StatsAPITimeseriesResponse<Jsonify<T>>> {
    const response = (await statsApiFetcherBase<T>(path, host, options)) as StatsAPITimeseriesResponse<Jsonify<T>>;
    return response;
}

export async function timeseriesLocalAPIHandler<T>(
    path: string,
    host: string,
    options?: FetcherOptions,
): Promise<StatsAPITimeseriesResponse<Jsonify<T>>> {
    const response = (await localApiFetcherBase<T>(path, host, options)) as StatsAPITimeseriesResponse<Jsonify<T>>;
    return response;
}

export function getCurrentDateAndTime(dateToFormat?: Date): string {
    const date = dateToFormat ?? new Date();
    const dateOptions: Intl.DateTimeFormatOptions = {
        timeZone: 'UTC',
        year: 'numeric',
        month: 'long',
        day: 'numeric',
    };
    const timeOptions: Intl.DateTimeFormatOptions = {
        timeZone: 'UTC',
        hour: '2-digit',
        minute: '2-digit',
        hour12: false,
    };
    const dateString = date.toLocaleDateString('en-US', dateOptions);
    const timeString = date.toLocaleTimeString('en-US', timeOptions);
    return `${dateString} ${timeString} UTC`;
}

export function getRealtimeApiURL(host: string): string {
    return `wss://${getDeploymentEnvironmentDomain(host)}/realtime-api`;
}

export function extractUrlSearchParam(url: string, param: string): string | null {
    const regex = new RegExp(`${param}\/([^/?]+|$)`);
    const match = url.match(regex);
    return match?.[1] || null;
}

export function getBreadcrumbsForEntity(entity: string, params: { [key: string]: string }): Breadcrumb[] {
    const graph = new EntityGraph();
    const connectedEntities = graph.getAncestors(entity);
    let path = '';
    const crumbs: Breadcrumb[] = [];
    const capitalizeEntityName = (entity: string) => {
        return entity.charAt(0).toUpperCase() + entity.slice(1);
    };
    connectedEntities?.forEach((entity: string) => {
        const paramValue = params[graph.getParam(entity) ?? ''];
        path = path + `/${entity}/${paramValue}`;
        const truncatedAddressOrHash = entity === 'account' || entity === 'order' ? shortenHex(paramValue) : paramValue;
        crumbs.push({ breadcrumb: `${capitalizeEntityName(entity)} ${truncatedAddressOrHash}`, href: path });
    });
    return crumbs;
}

export function sideEnumToString(side: number) {
    if (side === 1) {
        return 'Long';
    } else if (side === 2) {
        return 'Short';
    }
    return 'None';
}

export async function statsApiClientsideFetcher<T>(url: string): Promise<StatsAPIListResponse<Jsonify<T>>> {
    'use client';
    return (await apiFetcherBase<T>(url)) as StatsAPIListResponse<Jsonify<T>>;
}

export function getDateAndTime(dateToFormat?: Date): string {
    const date = dateToFormat ?? new Date();
    const dateOptions: Intl.DateTimeFormatOptions = {
        timeZone: 'UTC',
        year: 'numeric',
        month: 'long',
        day: 'numeric',
    };
    const timeOptions: Intl.DateTimeFormatOptions = {
        timeZone: 'UTC',
        hour: '2-digit',
        minute: '2-digit',
        hour12: false,
    };
    const dateString = date.toLocaleDateString('en-US', dateOptions);
    const timeString = date.toLocaleTimeString('en-US', timeOptions);
    return `${dateString} ${timeString} UTC`;
}

export function formatAge(time: number): string {
    const now = new Date().getTime();
    const diff = now - time;
    const inSeconds = +(diff / 1000).toFixed(0);
    const inMinutes = +(inSeconds / 60).toFixed(0);
    const inHours = +(inMinutes / 60).toFixed(0);
    const inDays = +(inHours / 24).toFixed(0);
    if (inDays > 1) {
        return `${inDays} days ago`;
    } else if (inHours > 1) {
        return `${inHours.toFixed(0)} hrs ago`;
    } else if (inMinutes > 0) {
        return `${inMinutes.toFixed(0)} mins ago`;
    } else {
        return '1 min ago';
    }
}

export function getPriceFormatter() {
    return Intl.NumberFormat('en', { minimumFractionDigits: 2, maximumFractionDigits: 2 });
}

export function getFeeFormatter() {
    return Intl.NumberFormat('en', { minimumFractionDigits: 4, maximumFractionDigits: 4 });
}

export function shortenHex(address: string, length = 4): string {
    return `${address.slice(0, length + 2)}…${address.slice(address.length - length)}`;
}

export function getTraderFeeAmountFromFill(trader: string, strategyIdHash: string, fill: FillsResponseItem): BigNumber {
    const isTaker =
        trader === fill.takerOrderTrader?.replace('0x00', '0x') && strategyIdHash === fill.takerOrderStrategyIdHash;
    if (isTaker) {
        const takerFeeUSDC = fill.takerFeeUSDC;
        return takerFeeUSDC.isZero() ? fill.takerFeeDDX : takerFeeUSDC;
    } else {
        return fill.makerFeeUSDC.isZero() ? fill.makerFeeDDX : fill.makerFeeUSDC;
    }
}

export function debounce(func, timeout = 300) {
    let timer;
    return (...args) => {
        clearTimeout(timer);
        timer = setTimeout(() => {
            func.apply(this, args);
        }, timeout);
    };
}
