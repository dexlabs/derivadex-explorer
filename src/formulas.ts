import BigNumber from 'bignumber.js';
import { MarketResponseItem, PositionResponseItem, StrategyResponseItem } from 'src/types/api';

import { Jsonify } from './data/types';

export function calculateLeverage(
    positions: Jsonify<PositionResponseItem>[],
    markets: Jsonify<MarketResponseItem>[],
    strategy: Jsonify<StrategyResponseItem>,
): BigNumber {
    const notionalBalance = calculateNotionalBalance(positions, markets);
    const strategyValue = new BigNumber(strategy.availCollateral).plus(new BigNumber(strategy.lockedCollateral));
    if (strategyValue.isZero() || notionalBalance.isZero()) {
        return new BigNumber(0);
    }
    return new BigNumber(notionalBalance).dividedBy(strategyValue);
}

export function calculateNotionalBalance(
    positions: Jsonify<PositionResponseItem>[],
    markets: Jsonify<MarketResponseItem>[],
): BigNumber {
    let notionalBalance = new BigNumber(0);
    positions.forEach((position) => {
        const market = markets.find((m) => m.market === position.symbol);
        if (market) {
            notionalBalance = notionalBalance.plus(
                new BigNumber(position.balance).multipliedBy(new BigNumber(market.price)),
            );
        }
    });
    return notionalBalance;
}

export function computePercentageGainOrLossToString(avgEntryPrice: BigNumber, markPrice: BigNumber): string {
    if (avgEntryPrice.isZero() || markPrice.isZero()) {
        return '+0%';
    }
    const difference = markPrice.minus(avgEntryPrice);
    const percentage = difference.dividedBy(avgEntryPrice).multipliedBy(100);
    if (percentage.toNumber() < 0) {
        return '-' + percentage.abs().dp(2) + '%';
    } else {
        return '+' + percentage.dp(2) + '%';
    }
}

export function computeStrategyCostBasis(positions: Jsonify<PositionResponseItem>[]): BigNumber {
    let totalCost = new BigNumber(0);
    positions.forEach((position) => {
        totalCost = totalCost.plus(new BigNumber(position.avgEntryPrice).multipliedBy(position.balance));
    });

    return totalCost;
}

export function computeStrategyNotionalValue(
    positions: Jsonify<PositionResponseItem>[],
    markets: Jsonify<MarketResponseItem>[],
): BigNumber {
    let totalValue = new BigNumber(0);
    positions.forEach((position) => {
        const markPrice = markets.find((it) => it.market === position.symbol)?.price;
        if (markPrice) {
            totalValue = totalValue.plus(new BigNumber(position.balance).multipliedBy(new BigNumber(markPrice)));
        }
    });

    return totalValue;
}
