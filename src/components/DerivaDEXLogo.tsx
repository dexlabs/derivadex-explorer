'use client';

import styled from '@emotion/styled';
import { motion } from 'framer-motion';

const pathVariants1 = {
    hidden: {
        opacity: 0,
        pathLength: 0,
    },
    visible: {
        opacity: 1,
        pathLength: 1,
        transition: {
            duration: 0.5,
            ease: 'easeInOut',
        },
    },
};

const pathVariants2 = {
    hidden: {
        opacity: 0,
        pathLength: 0,
    },
    visible: {
        opacity: 1,
        pathLength: 1,
        transition: {
            duration: 2.5,
            ease: 'easeInOut',
        },
    },
};

const StyledSVGDesktop = styled(motion.svg)`
    display: none;

    @media (min-width: 768px) {
        display: block;
        overflow: visible;
        color: #ffffff;
    }
`;

const StyledSVGMobile = styled(motion.svg)`
    color: #ffffff;
    overflow: visible;
    width: 3.5rem;

    @media (min-width: 768px) {
        display: none;
    }
    /* &:hover {
        cursor: pointer;
    } */
`;

export default function DerivaDEXLogo({ colorMode }) {
    const fillColor = colorMode === 'light' ? '#2c125c' : 'white';
    return (
        <div>
            <StyledSVGDesktop initial="hidden" viewBox="0 0 1372 137" animate="visible" width="100%">
                <motion.path
                    d="M0 1.89502H56.8508C90.0138 1.89502 116.86 28.7412 116.86 61.9042H0V1.89502Z"
                    fill={fillColor}
                    variants={pathVariants1}
                />
                <motion.path
                    d="M129.494 1.89502H186.344C219.507 1.89502 246.354 28.7412 246.354 61.9042H129.494V1.89502Z"
                    fill={fillColor}
                    variants={pathVariants1}
                />
                <motion.path
                    d="M0 134.547H56.8508C90.0138 134.547 116.86 107.701 116.86 74.5376H0V134.547Z"
                    variants={pathVariants1}
                    fill={fillColor}
                />
                <motion.path
                    d="M129.494 134.547H186.344C219.507 134.547 246.354 107.701 246.354 74.5376H129.494V134.547Z"
                    variants={pathVariants1}
                    fill={fillColor}
                />
                <motion.path
                    d="M241.695 1.89502H283.702L376.558 134.547H334.63L241.695 1.89502Z"
                    fill={fillColor}
                    variants={pathVariants1}
                />
                <motion.path
                    d="M299.256 1.89502L337.946 57.0877L376.558 1.89502H299.256Z"
                    fill="#EF88AA"
                    variants={pathVariants2}
                />
                <motion.path
                    d="M319.154 134.547L280.464 79.2754L241.853 134.547H319.154Z"
                    fill="#EF88AA"
                    variants={pathVariants2}
                />
            </StyledSVGDesktop>
            <StyledSVGMobile
                xmlns="http://www.w3.org/2000/svg"
                viewBox="0 -10 473 168"
                initial="hidden"
                animate="visible"
            >
                <motion.path
                    d="M0 1.89502H56.8508C90.0138 1.89502 116.86 28.7412 116.86 61.9042H0V1.89502Z"
                    fill={fillColor}
                    variants={pathVariants1}
                />
                <motion.path
                    d="M129.494 1.89502H186.344C219.507 1.89502 246.354 28.7412 246.354 61.9042H129.494V1.89502Z"
                    fill={fillColor}
                    variants={pathVariants1}
                />
                <motion.path
                    d="M0 134.547H56.8508C90.0138 134.547 116.86 107.701 116.86 74.5376H0V134.547Z"
                    variants={pathVariants1}
                    fill={fillColor}
                />
                <motion.path
                    d="M129.494 134.547H186.344C219.507 134.547 246.354 107.701 246.354 74.5376H129.494V134.547Z"
                    variants={pathVariants1}
                    fill={fillColor}
                />
                <motion.path
                    d="M241.695 1.89502H283.702L376.558 134.547H334.63L241.695 1.89502Z"
                    fill={fillColor}
                    variants={pathVariants1}
                />
                <motion.path
                    d="M299.256 1.89502L337.946 57.0877L376.558 1.89502H299.256Z"
                    fill="#EF88AA"
                    variants={pathVariants2}
                />
                <motion.path
                    d="M319.154 134.547L280.464 79.2754L241.853 134.547H319.154Z"
                    fill="#EF88AA"
                    variants={pathVariants2}
                />
            </StyledSVGMobile>
        </div>
    );
}
