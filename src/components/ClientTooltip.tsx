'use client';

import { Tooltip } from '@chakra-ui/react';

export default function ClientTooltip({ label, text }: { label: string; text: string }) {
    return (
        <Tooltip label={label} hasArrow={true}>
            {text}
        </Tooltip>
    );
}
