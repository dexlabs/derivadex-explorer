import { shortenHex } from '@utils';
import Link from 'next/link';

import CopyButton from './CopyButton';

export default function OrderLink({
    trader,
    strategyId,
    orderHash,
}: {
    trader: string;
    strategyId: string;
    orderHash: string;
}) {
    const traderAddress = `0x${trader?.substring(4)}`;
    return (
        <span style={{ whiteSpace: 'nowrap' }}>
            <Link href={`/account/${traderAddress}/strategy/${strategyId}/order/${orderHash}`}>
                {shortenHex(orderHash)}
            </Link>
            <CopyButton content={orderHash} description="Copy order hash" />
        </span>
    );
}
