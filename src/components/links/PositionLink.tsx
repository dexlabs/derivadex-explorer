import Link from 'next/link';

export default function PositionLink({
    trader,
    strategyId,
    symbol,
    text,
}: {
    trader: string;
    strategyId: string;
    symbol: string;
    text: string;
}) {
    const traderAddress = `0x${trader?.substring(4)}`;
    return <Link href={`/account/${traderAddress}/strategy/${strategyId}/position/${symbol}`}>{text}</Link>;
}
