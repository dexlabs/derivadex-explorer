import Link from 'next/link';

export default function MarketLink({ symbol }: { symbol: string }) {
    return <Link href={`/market/${symbol}`}>{symbol}</Link>;
}
