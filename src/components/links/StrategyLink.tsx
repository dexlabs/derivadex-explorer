import Link from 'next/link';

export default function StrategyLink({ trader, strategyId }: { trader: string; strategyId: string }) {
    const traderAddress = `0x${trader?.substring(4)}`;
    return <Link href={`/account/${traderAddress}/strategy/${strategyId}`}>{strategyId}</Link>;
}
