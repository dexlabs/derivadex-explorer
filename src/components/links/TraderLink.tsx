import { shortenHex } from '@utils';
import Link from 'next/link';

import CopyButton from './CopyButton';

export default function TraderLink({ trader }: { trader: string }) {
    const traderAddress = trader.length === 44 ? `0x${trader?.substring(4)}` : trader;
    if (trader !== undefined) {
        return (
            <span style={{ whiteSpace: 'nowrap' }}>
                <Link href={`/account/${traderAddress}`}>{shortenHex(traderAddress)}</Link>
                <CopyButton content={traderAddress} description="Copy trader address" />
            </span>
        );
    } else {
        return <span>---</span>;
    }
}
