'use client';

import { CheckIcon, CopyIcon } from '@chakra-ui/icons';
import { IconButton, Tooltip, useClipboard } from '@chakra-ui/react';

export default function CopyButton({ description, content }: { description: string; content: string }) {
    const { onCopy, hasCopied } = useClipboard(content, 2000);

    return (
        <Tooltip label={hasCopied ? 'Copied!' : description}>
            <IconButton
                size="xs"
                ml="0.3rem"
                variant="link"
                aria-label={description}
                icon={
                    hasCopied ? (
                        <CheckIcon boxSize="4" color="gray.500" />
                    ) : (
                        <CopyIcon className="copy-button" boxSize={4} />
                    )
                }
                onClick={() => {
                    onCopy();
                }}
            />
        </Tooltip>
    );
}
