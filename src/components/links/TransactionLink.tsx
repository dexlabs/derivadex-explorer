import Link from 'next/link';

export default function TransactionLink({ epochId, txOrdinal }: { epochId: string; txOrdinal: string }) {
    return <Link href={`/epoch/${epochId}/txOrdinal/${txOrdinal}`}>{`Epoch ${epochId}, TxOrdinal ${txOrdinal}`}</Link>;
}
