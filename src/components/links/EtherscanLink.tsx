import { shortenHex } from '@utils';
import Link from 'next/link';

import CopyButton from './CopyButton';

export default function EtherscanLink({ txHash }: { txHash: string }) {
    return (
        <span style={{ whiteSpace: 'nowrap' }}>
            <Link href={`https://sepolia.etherscan.io/tx/${txHash}`}>{shortenHex(txHash)}</Link>
            <CopyButton content={txHash} description="Copy Ethereum transaction hash" />
        </span>
    );
}
