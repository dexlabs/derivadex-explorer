'use client';

import {
    Box,
    Button,
    Card,
    Flex,
    Highlight,
    Input,
    InputGroup,
    InputRightElement,
    Spinner,
    StackDivider,
    VStack,
} from '@chakra-ui/react';
import { debounce } from '@utils';
import Link from 'next/link';
import { useRouter } from 'next/navigation';
import { useCallback, useEffect, useState } from 'react';
import { internalFetcher } from 'src/data/utils';
import { useNavigationEvent } from 'src/hooks/useNavigationEvent';
import useSWRImmutable from 'swr/immutable';

import { ParamEventTypes } from '@/types/api';
import { SearchResponse } from '@/types/explorer';

const MAX_SEARCH_RESULTS_NUMBER = 6;

export default function SearchBox() {
    const [query, setQuery] = useState('');
    const [remoteQuery, setRemoteQuery] = useState('');
    const handleQueryChange = (event) => setQuery(event.target.value);
    const handleRemoteQueryChange = useCallback(
        debounce((val) => setRemoteQuery(val), 300),
        [],
    );
    useNavigationEvent(() => setQuery(''));

    const router = useRouter();
    const { data, isLoading } = useSWRImmutable(
        () => (remoteQuery && /^[1-9]/i.test(remoteQuery) ? `/api/search?query=${remoteQuery}` : '/api/search'),
        internalFetcher<SearchResponse>,
    );
    useEffect(() => {
        handleRemoteQueryChange(query);
    }, [query]);

    let searchResults: React.ReactNode[] | null = null;
    if (data) {
        if (query.startsWith('0')) {
            searchResults = data.traders
                .filter((t) => t.toLowerCase().startsWith(query.toLowerCase()))
                .slice(0, MAX_SEARCH_RESULTS_NUMBER)
                .map((t) => (
                    <Link href={`/account/${t}`} key={t}>
                        <Box p="0.5rem">
                            Account:{' '}
                            <Highlight styles={{ bg: 'orange.100' }} query={query}>
                                {t}
                            </Highlight>
                        </Box>
                    </Link>
                ));
        } else if (/^[a-z]/i.test(query)) {
            searchResults = data.markets
                .filter((m) => m.toLowerCase().includes(query.toLowerCase()))
                .slice(0, MAX_SEARCH_RESULTS_NUMBER)
                .map((m) => (
                    <Link href={`/market/${m}`} key={m}>
                        <Box p="0.5rem">
                            Market:{' '}
                            <Highlight styles={{ bg: 'orange.100' }} query={query}>
                                {m}
                            </Highlight>
                        </Box>
                    </Link>
                ));
        } else {
            searchResults = data.epochs
                .map((e) => (
                    <Box p="0.5rem" key={e}>
                        Epoch: {e}
                    </Box>
                ))
                .concat(
                    data.transactions
                        .map((t) => (
                            <Link href={`/epoch/${t.epoch}/txOrdinal/${t.txOrdinal}`} key={`${t.epoch}_${t.txOrdinal}`}>
                                <Box p="0.5rem">
                                    Epoch: {t.epoch}, TxOrdinal: {t.txOrdinal}, Type:{' '}
                                    {Object.keys(ParamEventTypes)[
                                        Object.values(ParamEventTypes).indexOf(t.eventKind)
                                    ].replace('_', ' ')}
                                </Box>
                            </Link>
                        ))
                        .slice(0, MAX_SEARCH_RESULTS_NUMBER),
                );
        }
    }

    return (
        <Box borderRadius="5px" mt="1rem" px={{ base: '5vw', sm: '5vw', md: '8vw', lg: '10vw', '2xl': '12vw' }}>
            <div style={{ position: 'relative' }}>
                <InputGroup size="md" width="100%">
                    <Input
                        pr="4.5rem"
                        type="text"
                        placeholder="Type an Account Address (0x123...), Market (ETHP), or Epoch/Transaction ordinals (45,23)"
                        value={query}
                        onChange={handleQueryChange}
                        onKeyPress={(e) => {
                            if (e.key === 'Enter' && searchResults && searchResults.length === 1) {
                                if (query.startsWith('0')) {
                                    router.push(`/account/${data!.traders[0]}`);
                                } else if (/^[a-z]/i.test(query)) {
                                    router.push(`/market/${data!.markets[0]}`);
                                }
                            }
                        }}
                    />
                    <InputRightElement mr="0.5rem">
                        <Flex alignItems="center">
                            <Spinner size="xs" visibility={isLoading ? 'inherit' : 'hidden'} />
                            <Button
                                isDisabled={!(searchResults && searchResults.length === 1)}
                                size="sm"
                                variant="ghost"
                                colorScheme="blue"
                                onClick={() => {
                                    if (query.startsWith('0')) {
                                        router.push(`/account/${data!.traders[0]}`);
                                    } else if (/^[a-z]/i.test(query)) {
                                        router.push(`/market/${data!.markets[0]}`);
                                    }
                                }}
                            >
                                Go
                            </Button>
                        </Flex>
                    </InputRightElement>
                </InputGroup>
                {query.length ? (
                    <VStack
                        divider={<StackDivider borderColor="gray.200" />}
                        spacing={0}
                        align="stretch"
                        position="absolute"
                        marginTop="0.5rem"
                        width={{ base: '100%', lg: '50%' }}
                        zIndex="1"
                        border="1px"
                        borderRadius="5px"
                        className="search-results"
                        boxShadow="lg"
                    >
                        {searchResults ? (
                            searchResults.length ? (
                                searchResults
                            ) : (
                                <Box p="0.5rem">No results found</Box>
                            )
                        ) : (
                            <Box p="0.5rem">Loading...</Box>
                        )}
                    </VStack>
                ) : null}
            </div>
        </Box>
    );
}
