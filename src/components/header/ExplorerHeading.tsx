'use client';

import { Box, Divider, Flex, Heading, Link, Text, useColorMode } from '@chakra-ui/react';
import { useEffect, useState } from 'react';

import { DarkModeSwitch } from '../DarkModeSwitch';
import DerivaDEXLogo from '../DerivaDEXLogo';
import { ExchangeButton } from '../ExchangeButton';
import SearchBox from './SearchBox';

export default function ExplorerHeading() {
    const { colorMode } = useColorMode();
    const [windowWidth, setWindowWidth] = useState(0);
    useEffect(() => {
        setWindowWidth(window.innerWidth);
    }, []);

    const isDesktop = windowWidth >= 768;
    return (
        <Box pt={{ base: '5vw', sm: '5vw', md: '5vw', lg: '2vw', '2xl': '2vw' }}>
            <Flex
                justifyContent="space-between"
                alignItems="center"
                px={{ base: '5vw', sm: '5vw', md: '8vw', lg: '10vw', '2xl': '12vw' }}
                mb="1.5rem"
            >
                <Heading>
                    <Link href="/" textDecoration={'none'} _hover={{ textDecoration: 'none' }}>
                        <Flex alignItems="center">
                            <DerivaDEXLogo colorMode={colorMode} />
                            {isDesktop && (
                                <Text className="ddx-logo" marginLeft={'-12.7rem'} marginTop="0.25rem">
                                    DerivaDEX Explorer
                                </Text>
                            )}
                        </Flex>
                    </Link>
                </Heading>
                <Flex>
                    <Flex marginRight={'1rem'}>
                        <DarkModeSwitch />
                    </Flex>
                    <ExchangeButton />
                </Flex>
            </Flex>
            <SearchBox />
            <Divider my="2rem" />
        </Box>
    );
}
