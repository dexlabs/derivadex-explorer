import { MoonIcon, SunIcon } from '@chakra-ui/icons';
import { IconButton, useColorMode } from '@chakra-ui/react';

export const DarkModeSwitch = () => {
    const { colorMode, toggleColorMode } = useColorMode();
    const isDark = colorMode === 'dark';
    return (
        <IconButton
            icon={isDark ? <SunIcon /> : <MoonIcon />}
            aria-label="Toggle Dark Mode"
            colorScheme="gray"
            onClick={toggleColorMode}
        />
    );
};
