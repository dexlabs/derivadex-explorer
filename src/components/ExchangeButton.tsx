'use client';

import { Button } from '@chakra-ui/react';
import { useEffect, useState } from 'react';
import { getDeploymentEnvironmentDomain } from 'src/data/utils';

export const ExchangeButton = () => {
    const [host, setHost] = useState('');
    useEffect(() => {
        setHost(window.location.host);
    }, []);

    return (
        <Button as="a" href={`https://${getDeploymentEnvironmentDomain(host)}`} target="_blank">
            Go to Exchange
        </Button>
    );
};
