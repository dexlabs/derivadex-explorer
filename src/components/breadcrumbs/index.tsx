'use client';

import { getBreadcrumbsForEntity } from '@utils';
import Link from 'next/link';
import React from 'react';

export interface Breadcrumb {
    breadcrumb: string;
    href: string;
}

export interface BreadcrumbsProps {
    entity: string;
    params: { [key: string]: string };
}

const Breadcrumbs = ({ entity, params }: BreadcrumbsProps) => {
    const breadcrumbs = getBreadcrumbsForEntity(entity, params);
    return (
        <div className="breadcrumbs" style={{ display: 'flex', flexDirection: 'row' }}>
            {breadcrumbs.length > 0 &&
                breadcrumbs.map((breadcrumb, i) => {
                    return (
                        <div key={breadcrumb.href} style={{ paddingRight: '1rem' }}>
                            <Link href={breadcrumb.href} style={{ marginRight: '1rem' }}>
                                {breadcrumb.breadcrumb}
                            </Link>
                            {i !== breadcrumbs.length - 1 ? '>' : ''}
                        </div>
                    );
                })}
        </div>
    );
};

export default Breadcrumbs;
