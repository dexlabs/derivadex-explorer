import { getPriceFormatter, sideEnumToString, statsApiListFetcher, statsApiSingleFetcher } from '@utils';
import BigNumber from 'bignumber.js';
import { headers } from 'next/headers';
import MarketLink from 'src/components/links/MarketLink';
import StrategyLink from 'src/components/links/StrategyLink';
import TraderLink from 'src/components/links/TraderLink';
import ErrorWidget from 'src/components/widgets/ErrorWidget';
import { calculateLeverage, computePercentageGainOrLossToString } from 'src/formulas';

import { MarketResponseItem, PositionResponseItem, StrategyResponseItem } from '@/types/api';

import ListWidget from '../../widgets/ListWidget';

export default async function PositionWidget({
    trader,
    strategyId,
    symbol,
}: {
    trader: string;
    strategyId: string;
    symbol: string;
}) {
    const headersList = headers();
    const host = headersList.get('host') || '';
    const [positionResponse, strategyResponse, marketsResponse] = await Promise.all([
        statsApiListFetcher<PositionResponseItem>(`/account/${trader}/strategy/${strategyId}/positions`, host),
        statsApiSingleFetcher<StrategyResponseItem>(`/account/${trader}/strategy/${strategyId}`, host),
        statsApiListFetcher<MarketResponseItem>('/markets', host),
    ]);
    if (!positionResponse.success || !strategyResponse.success || !marketsResponse.success) {
        return <ErrorWidget title="Position Details" />;
    }
    const positionValue = positionResponse.value.find((it) => it.symbol === symbol);
    const markPriceValue = marketsResponse.value.find((it) => it.market === symbol)?.price;
    const markPrice = markPriceValue !== undefined ? new BigNumber(markPriceValue) : undefined;
    if (positionValue === undefined || markPrice === undefined) {
        return <ErrorWidget title="Position Details" />;
    }
    const leverage = calculateLeverage(positionResponse.value, marketsResponse.value, strategyResponse.value);
    const priceFormatter = getPriceFormatter();
    const avgEntryPrice = new BigNumber(positionValue.avgEntryPrice);
    const notionalValueString = `$${priceFormatter.format(
        new BigNumber(positionValue.balance).multipliedBy(markPrice).toNumber(),
    )}`;
    const percentPnlString = computePercentageGainOrLossToString(avgEntryPrice, markPrice);

    return (
        <>
            <ListWidget
                title={'Overview'}
                lastUpdatedTimestamp={positionResponse.timestamp}
                listItems={[
                    {
                        label: 'Account',
                        value: <TraderLink trader={trader} />,
                    },
                    {
                        label: 'Strategy',
                        value: <StrategyLink trader={positionValue.trader} strategyId={strategyId} />,
                    },
                    {
                        label: 'Symbol',
                        value: <MarketLink symbol={symbol} />,
                    },
                    {
                        label: 'Side',
                        value: sideEnumToString(positionValue.side),
                    },
                    {
                        label: 'Balance',
                        value: positionValue.balance,
                    },
                ]}
            />
            <ListWidget
                title={'More Info'}
                lastUpdatedTimestamp={positionResponse.timestamp}
                listItems={[
                    {
                        label: 'Avg Entry Price',
                        value: `$${priceFormatter.format(avgEntryPrice.toNumber())}`,
                    },
                    {
                        label: 'Mark Price',
                        value: `$${priceFormatter.format(markPrice.toNumber())}`,
                    },
                    {
                        label: 'Percent PnL',
                        value: percentPnlString,
                    },
                    {
                        label: 'Leverage',
                        value: `${leverage.toNumber() < 0.01 ? '<0.01' : leverage.toFixed(2)}x`,
                    },
                    {
                        label: 'Notional Value',
                        value: notionalValueString,
                    },
                ]}
            />
        </>
    );
}
