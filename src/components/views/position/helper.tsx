import { formatAge } from '@utils';
import BigNumber from 'bignumber.js';
import TransactionLink from 'src/components/links/TransactionLink';
import { PositionHistoryItem } from 'src/data/position/types';
import { formatPositionHistoryEvent } from 'src/data/position/utils';
import { Jsonify } from 'src/data/types';

import { ParamPositionSide } from '@/types/api';

export const parseTableData = (response, extraData?: { [key: string]: string }) => {
    return response.value.map((it: Jsonify<PositionHistoryItem>) => ({
        age: <div>{formatAge(new Date(it.createdAt).getTime())}</div>,
        event: (
            <>
                <div>
                    <div>{formatPositionHistoryEvent(it.event)}</div>
                </div>
                <TransactionLink epochId={it.epochId} txOrdinal={it.txOrdinal} />
            </>
        ),
        side: <div>{Object.values(ParamPositionSide)[it.side]}</div>,
        amount: <div>{new BigNumber(it.amount).isNegative() ? `${it.amount}` : `+${it.amount}`}</div>,
        balance: <div>{it.balance}</div>,
    }));
};
