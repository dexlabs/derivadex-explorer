'use client';

import { timeseriesLocalAPIHandler } from '@utils';
import BigNumber from 'bignumber.js';
import { Column } from 'react-table';
import { PositionHistoryItem } from 'src/data/position/types';
import { Jsonify, StatsAPIListResponse, StatsAPISuccessTimeseriesResponse } from 'src/data/types';

import TablePaginationWidget, { DefaultPagination } from '../../widgets/TablePaginationWidget';
import { parseTableData } from './helper';

type DataTableProps = {
    lastUpdatedTimestamp: number;
    initialData: StatsAPIListResponse<Jsonify<PositionHistoryItem>>;
    columns: Column<any>[];
    pagination: DefaultPagination;
};

export default function TableTimeseriesPaginationWidget({
    lastUpdatedTimestamp,
    columns,
    initialData,
    pagination,
}: DataTableProps) {
    const getNextPageQueryParams = (
        previousRequest: string,
        previousResultSet: StatsAPIListResponse<Jsonify<PositionHistoryItem>>,
    ): { [key: string]: string } => {
        const timeSeriesResponse = previousResultSet as StatsAPISuccessTimeseriesResponse<Jsonify<PositionHistoryItem>>;
        const lastEntry = timeSeriesResponse.value[timeSeriesResponse.value.length - 1];
        return {
            epoch: timeSeriesResponse.nextEpoch?.toString() || '',
            txOrdinal: timeSeriesResponse.nextTxOrdinal?.toString() || '',
            ordinal: timeSeriesResponse.nextOrdinal?.toString() || '',
            lookbackValue: new BigNumber(lastEntry.balance).minus(lastEntry.amount).toString(),
        };
    };
    return (
        <TablePaginationWidget
            lastUpdatedTimestamp={lastUpdatedTimestamp}
            columns={columns}
            initialData={initialData}
            pagination={pagination}
            dataParser={parseTableData}
            getNextPageQueryParams={getNextPageQueryParams}
            fetcher={timeseriesLocalAPIHandler}
        />
    );
}
