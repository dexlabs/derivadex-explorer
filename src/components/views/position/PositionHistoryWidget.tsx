import { timeseriesLocalAPIHandler } from '@utils';
import { headers } from 'next/headers';
import ErrorWidget from 'src/components/widgets/ErrorWidget';
import { PositionHistoryItem } from 'src/data/position/types';
import { buildUrl } from 'src/data/utils';

import PositionHistoryTablePaginationWidget from './PositionHistoryTablePaginationWidget';

export default async function PositionHistoryWidget({
    trader,
    strategyId,
    symbol,
}: {
    trader: string;
    strategyId: string;
    symbol: string;
}) {
    const headersList = headers();
    const host = headersList.get('host') || '';
    const url = '/position_history';
    const params = { trader, strategyId, symbol, limit: 5 };
    const response = await timeseriesLocalAPIHandler<PositionHistoryItem>(buildUrl(url, params), host);

    if (!response.success) {
        return <ErrorWidget title="Position History" />;
    }

    const columns = [
        {
            Header: 'Age',
            accessor: 'age',
            collapse: false,
        },
        {
            Header: 'Event',
            accessor: 'event',
            collapse: false,
        },
        {
            Header: 'Side',
            accessor: 'side',
            collapse: false,
        },
        {
            Header: 'Change Amount',
            accessor: 'amount',
            collapse: false,
            isNumeric: true,
        },
        {
            Header: 'Resulting Balance',
            accessor: 'balance',
            collapse: false,
            isNumeric: true,
        },
    ];

    return (
        <PositionHistoryTablePaginationWidget
            lastUpdatedTimestamp={response.timestamp}
            columns={columns}
            initialData={response}
            pagination={{ url, host, params }}
        />
    );
}
