import { getPriceFormatter, statsApiListFetcher } from '@utils';
import BigNumber from 'bignumber.js';
import { headers } from 'next/headers';
import StrategyLink from 'src/components/links/StrategyLink';
import TraderLink from 'src/components/links/TraderLink';
import ErrorWidget from 'src/components/widgets/ErrorWidget';
import TableWidget from 'src/components/widgets/TableWidget';
import { LiquidationsResponseItem, ParamEventTypes, TraderUpdateResponseItem } from 'src/types/api';

export default async function TxLiquidatedStrategiesTableWidget({
    epochId,
    txOrdinal,
    event,
}: {
    epochId: string;
    txOrdinal: string;
    event: ParamEventTypes;
}) {
    const headersList = headers();
    const host = headersList.get('host') || '';
    const response = await statsApiListFetcher<LiquidationsResponseItem>(
        `/liquidations?order=desc&epoch=${epochId}&txOrdinal=${+txOrdinal + 1}`,
        host,
    );
    if (!response.success || response.value.length === 0) {
        return <ErrorWidget title="Liquidated Strategies" />;
    }
    // response will contain all transactions previous to epoch, txOrdinal passed in params
    const filtered = response.value.filter((it) => it.epochId === epochId && it.txOrdinal === txOrdinal);

    if (filtered.length === 0) {
        return <ErrorWidget title="Liquidated Strategies" />;
    }

    const columns = [
        {
            Header: 'Trader',
            accessor: 'trader',
            collapse: false,
        },
        {
            Header: 'Strategy',
            accessor: 'strategy',
            collapse: false,
        },
    ];

    const tableData = filtered.map((it) => ({
        trader: (
            <div>
                <TraderLink trader={it.trader} />
            </div>
        ),
        strategy: (
            <div>
                <StrategyLink trader={it.trader} strategyId="main" />
            </div>
        ),
    }));

    return <TableWidget lastUpdatedTimestamp={response.timestamp} columns={columns} data={tableData} />;
}
