'use client';

import { Column } from 'react-table';
import TableTimeseriesPaginationWidget from 'src/components/widgets/TableTimeseriesPaginationWidget';
import { Jsonify, StatsAPIListResponse } from 'src/data/types';

import { StrategyUpdateResponseItem } from '@/types/api';

import { DefaultPagination } from '../../widgets/TablePaginationWidget';
import { parseTableData } from './strategies_helper';

type DataTableProps = {
    lastUpdatedTimestamp: number;
    initialData: StatsAPIListResponse<Jsonify<StrategyUpdateResponseItem>>;
    columns: Column<any>[];
    pagination: DefaultPagination;
};

export default function TxStrategiesTablePaginationWidget({
    lastUpdatedTimestamp,
    columns,
    initialData,
    pagination,
}: DataTableProps) {
    return (
        <TableTimeseriesPaginationWidget
            lastUpdatedTimestamp={lastUpdatedTimestamp}
            columns={columns}
            initialData={initialData}
            pagination={pagination}
            dataParser={parseTableData}
        />
    );
}
