import { getPriceFormatter } from '@utils';
import { headers } from 'next/headers';
import Link from 'next/link';
import MarketLink from 'src/components/links/MarketLink';
import OrderLink from 'src/components/links/OrderLink';
import PositionLink from 'src/components/links/PositionLink';
import ErrorWidget from 'src/components/widgets/ErrorWidget';
import { statsApiTxFillTakerFetcher } from 'src/data/fill/fetchers';
import { ParamOrderSide, ParamOrderType } from 'src/data/order/types';
import { ParamEventTypes, ParamPositionSide } from 'src/types/api';

import ListWidget from '../../widgets/ListWidget';

export default async function TxFillDetailsWidget({
    epochId,
    txOrdinal,
    event,
}: {
    epochId: string;
    txOrdinal: string;
    event: ParamEventTypes;
}) {
    const headersList = headers();
    const host = headersList.get('host') || '';
    const response = await statsApiTxFillTakerFetcher(epochId, txOrdinal, host);
    const priceFormatter = getPriceFormatter();

    if (!response) {
        return <ErrorWidget title="Taker Details" />;
    }

    return (
        <ListWidget
            title={'Taker Details'}
            lastUpdatedTimestamp={response.timestamp}
            listItems={[
                {
                    label: 'Symbol',
                    value: <MarketLink symbol={response.value.takerOrder.symbol} />,
                },
                {
                    label: 'Filled Amount',
                    value: (
                        <span>
                            {response.value.takerOrder.amount.toString()}{' '}
                            <PositionLink
                                trader={response.value.takerOrder.traderAddress}
                                strategyId={response.value.takerOrder.strategyId}
                                symbol={response.value.takerOrder.symbol}
                                text={'View Position'}
                            />
                        </span>
                    ),
                },
                {
                    label: 'Remaining Amount',
                    value: response.value.takerOrder.remainingAmount.toString(),
                },
                {
                    label: 'Order',
                    value: (
                        <span>
                            {Object.values(ParamOrderSide)[response.value.takerOrder.side].toString()}
                            {' | '}
                            {Object.values(ParamOrderType)[response.value.takerOrder.orderType].toString()}
                            {' | '}
                            <OrderLink
                                trader={response.value.takerOrder.traderAddress}
                                strategyId={response.value.takerOrder.strategyId}
                                orderHash={response.value.takerOrder.orderHash}
                            />
                        </span>
                    ),
                },
                {
                    label: 'Average Price',
                    value: `$${priceFormatter.format(response.value.takerAvgPrice.toNumber())}`,
                },
                {
                    label: 'Fees Paid',
                    value: (
                        <span>
                            <Link href={'/insurance_fund'}>{`${priceFormatter.format(response.value.fees.toNumber())} ${
                                response.value.feesCurrency
                            }`}</Link>
                        </span>
                    ),
                },
                {
                    label: 'Notional Value',
                    value: `$${priceFormatter.format(
                        response.value.takerAvgPrice.multipliedBy(response.value.takerOrder.amount).toNumber(),
                    )}`,
                },
            ]}
        ></ListWidget>
    );
}
