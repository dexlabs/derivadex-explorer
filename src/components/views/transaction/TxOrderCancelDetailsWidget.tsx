import { headers } from 'next/headers';
import MarketLink from 'src/components/links/MarketLink';
import StrategyLink from 'src/components/links/StrategyLink';
import TraderLink from 'src/components/links/TraderLink';
import ErrorWidget from 'src/components/widgets/ErrorWidget';
import { statsApiTxCancelDetailsFetcher } from 'src/data/order/fetchers';
import { ParamEventTypes } from 'src/types/api';

import ListWidget from '../../widgets/ListWidget';

export default async function TxOrderCancelDetailsWidget({
    epochId,
    txOrdinal,
    event,
}: {
    epochId: string;
    txOrdinal: string;
    event: ParamEventTypes;
}) {
    const headersList = headers();
    const host = headersList.get('host') || '';
    const response = await statsApiTxCancelDetailsFetcher(epochId, txOrdinal, host);

    if (!response) {
        return <ErrorWidget title="Transaction Details" />;
    }

    return (
        <ListWidget
            title={'Transaction Details'}
            lastUpdatedTimestamp={response.timestamp}
            listItems={[
                {
                    label: 'Symbol',
                    value: <MarketLink symbol={response.value.symbol} />,
                },
                {
                    label: 'Trader',
                    value: <TraderLink trader={response.value.traderAddress} />,
                },
                {
                    label: 'Strategy',
                    value: (
                        <StrategyLink trader={response.value.traderAddress} strategyId={response.value.strategyId} />
                    ),
                },
            ]}
        ></ListWidget>
    );
}
