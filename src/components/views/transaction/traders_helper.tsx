import { getPriceFormatter } from '@utils';
import BigNumber from 'bignumber.js';
import TraderLink from 'src/components/links/TraderLink';

export const parseTableData = (response, extraData: { [key: string]: string }) => {
    const priceFormatter = getPriceFormatter();
    const filtered = response.value.filter(
        (it) => it.epochId === extraData.epochId && it.txOrdinal === extraData.txOrdinal,
    );
    return filtered.map((it) => ({
        trader: (
            <div>
                <TraderLink trader={it.trader} />
            </div>
        ),
        amount: (
            <div>
                <span>{priceFormatter.format(new BigNumber(it.amount)?.toNumber())} DDX</span>
            </div>
        ),
        balance: (
            <div>
                <span>{priceFormatter.format(new BigNumber(it.newDdxBalance)?.toNumber())} DDX</span>
            </div>
        ),
    }));
};
