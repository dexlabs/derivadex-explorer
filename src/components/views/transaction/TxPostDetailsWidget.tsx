import { getPriceFormatter, statsApiListFetcher } from '@utils';
import { headers } from 'next/headers';
import Link from 'next/link';
import MarketLink from 'src/components/links/MarketLink';
import OrderLink from 'src/components/links/OrderLink';
import StrategyLink from 'src/components/links/StrategyLink';
import TraderLink from 'src/components/links/TraderLink';
import ErrorWidget from 'src/components/widgets/ErrorWidget';
import ListWidget from 'src/components/widgets/ListWidget';
import { OrderIntentResponseItem, ParamOrderSide, ParamOrderType } from 'src/data/order/types';
import { ParamEventTypes } from 'src/types/api';

export default async function TxPostDetailsWidget({
    epochId,
    txOrdinal,
    event,
}: {
    epochId: string;
    txOrdinal: string;
    event: ParamEventTypes;
}) {
    const headersList = headers();
    const host = headersList.get('host') || '';
    const response = await statsApiListFetcher<OrderIntentResponseItem>(
        `/order_intents?order=desc&limit=1&epoch=${epochId}&txOrdinal=${+txOrdinal + 1}`,
        host,
    );

    if (!response.success || response.value.length === 0) {
        return <ErrorWidget title="Transaction Details" />;
    }

    const [post] = response.value;

    const priceFormatter = getPriceFormatter();

    return (
        <ListWidget
            title={'Transaction Details'}
            lastUpdatedTimestamp={response.timestamp}
            listItems={[
                {
                    label: 'Symbol',
                    value: <MarketLink symbol={post.symbol} />,
                },
                {
                    label: 'Account',
                    value: <TraderLink trader={post.traderAddress} />,
                },
                {
                    label: 'Strategy',
                    value: <StrategyLink trader={post.traderAddress} strategyId={post.strategyId} />,
                },
                {
                    label: 'Order',
                    value: (
                        <span>
                            {Object.values(ParamOrderSide)[post.side]}
                            {' | '}
                            {Object.values(ParamOrderType)[post.orderType].toString()}
                            {' | '}
                            <OrderLink
                                trader={post.traderAddress}
                                strategyId={post.strategyId}
                                orderHash={post.orderHash}
                            />
                        </span>
                    ),
                },
                {
                    label: 'Price',
                    value: `$${priceFormatter.format(+post.price)}`,
                },
                {
                    label: 'Amount',
                    value: post.amount.toString(),
                },
            ]}
        ></ListWidget>
    );
}
