import { getPriceFormatter, statsApiListFetcher } from '@utils';
import BigNumber from 'bignumber.js';
import { headers } from 'next/headers';
import EtherscanLink from 'src/components/links/EtherscanLink';
import TraderLink from 'src/components/links/TraderLink';
import ErrorWidget from 'src/components/widgets/ErrorWidget';
import ListWidget from 'src/components/widgets/ListWidget';
import { ParamEventTypes, ParamTraderUpdateKind, TraderUpdateResponseItem } from 'src/types/api';

export default async function TxTraderUpdateDetailsWidget({
    epochId,
    txOrdinal,
    event,
}: {
    epochId: string;
    txOrdinal: string;
    event: ParamEventTypes;
}) {
    const headersList = headers();
    const host = headersList.get('host') || '';
    const response = await statsApiListFetcher<TraderUpdateResponseItem>(
        `/trader_updates?order=desc&limit=1&epoch=${epochId}&txOrdinal=${+txOrdinal + 1}`,
        host,
    );
    if (!response.success || response.value.length === 0) {
        return <ErrorWidget title="Traders" />;
    }

    const [tu] = response.value;
    const priceFormatter = getPriceFormatter();

    const kind = Object.values(ParamTraderUpdateKind)[tu.kind] || '';

    const listItems = [
        {
            label: 'Event',
            value: kind,
        },
        {
            label: 'Account',
            value: <TraderLink trader={tu.trader} />,
        },
        {
            label: 'Amount',
            value: <span>{priceFormatter.format(new BigNumber(tu.amount).toNumber())} DDX</span>,
        },
        {
            label: 'DDX Balance',
            value: <span>{priceFormatter.format(new BigNumber(tu.newDdxBalance).toNumber())} DDX</span>,
        },
    ];

    if (tu.txHash) {
        listItems.push({
            label: 'Ethereum Tx Hash',
            value: <EtherscanLink txHash={tu.txHash} />,
        });
    }

    return (
        <ListWidget
            title={'Transaction Details'}
            lastUpdatedTimestamp={response.timestamp}
            listItems={listItems}
        ></ListWidget>
    );
}
