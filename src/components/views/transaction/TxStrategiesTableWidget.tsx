import { statsApiListFetcher } from '@utils';
import { headers } from 'next/headers';
import ErrorWidget from 'src/components/widgets/ErrorWidget';
import { buildUrl } from 'src/data/utils';
import { ParamEventTypes, StrategyUpdateResponseItem } from 'src/types/api';

import TxStrategiesTablePaginationWidget from './TxStrategiesTablePaginationWidget';

export default async function TxStrategiesTableWidget({
    epochId,
    txOrdinal,
    event,
}: {
    epochId: string;
    txOrdinal: string;
    event: ParamEventTypes;
}) {
    const headersList = headers();
    const host = headersList.get('host') || '';
    const params = { order: 'desc', limit: 5, epoch: epochId, txOrdinal: +txOrdinal + 1 };
    const url = '/strategy_updates';
    const response = await statsApiListFetcher<StrategyUpdateResponseItem>(buildUrl(url, params), host);
    if (!response.success || response.value.length === 0) {
        return <ErrorWidget title="Strategies" />;
    }

    const columns = [
        {
            Header: 'Trader',
            accessor: 'trader',
            collapse: false,
        },
        {
            Header: 'Strategy',
            accessor: 'strategy',
            collapse: false,
        },
        {
            Header: 'Distribution',
            accessor: 'amount',
            collapse: false,
            isNumeric: true,
        },
        {
            Header: 'Strategy Collateral',
            accessor: 'newAvailCollateral',
            collapse: false,
            isNumeric: true,
        },
    ];

    return (
        <TxStrategiesTablePaginationWidget
            lastUpdatedTimestamp={response.timestamp}
            columns={columns}
            initialData={response}
            pagination={{ url, host, params, extraData: { epochId, txOrdinal } }}
        />
    );
}
