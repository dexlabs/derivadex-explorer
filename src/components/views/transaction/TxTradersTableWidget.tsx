import { statsApiListFetcher } from '@utils';
import { headers } from 'next/headers';
import ErrorWidget from 'src/components/widgets/ErrorWidget';
import { buildUrl } from 'src/data/utils';
import { ParamEventTypes, TraderUpdateResponseItem } from 'src/types/api';

import TxTradersTablePaginationWidget from './TxTradersTablePaginationWidget';

export default async function TxTradersTableWidget({
    epochId,
    txOrdinal,
    event,
}: {
    epochId: string;
    txOrdinal: string;
    event: ParamEventTypes;
}) {
    const headersList = headers();
    const host = headersList.get('host') || '';
    const url = '/trader_updates';
    const params = { order: 'desc', limit: 5, epoch: epochId, txOrdinal: +txOrdinal + 1 };
    const response = await statsApiListFetcher<TraderUpdateResponseItem>(buildUrl(url, params), host);
    if (!response.success || response.value.length === 0) {
        return <ErrorWidget title="Traders" />;
    }

    const columns = [
        {
            Header: 'Trader',
            accessor: 'trader',
            collapse: false,
        },
        {
            Header: 'Rewards Distributed',
            accessor: 'amount',
            collapse: false,
            isNumeric: true,
        },
        {
            Header: 'Trader Balance',
            accessor: 'balance',
            collapse: false,
            isNumeric: true,
        },
    ];

    return (
        <TxTradersTablePaginationWidget
            lastUpdatedTimestamp={response.timestamp}
            columns={columns}
            initialData={response}
            pagination={{ url, host, params, extraData: { epochId, txOrdinal } }}
        />
    );
}
