import { getPriceFormatter, statsApiListFetcher } from '@utils';
import BigNumber from 'bignumber.js';
import { headers } from 'next/headers';
import EtherscanLink from 'src/components/links/EtherscanLink';
import StrategyLink from 'src/components/links/StrategyLink';
import TraderLink from 'src/components/links/TraderLink';
import ErrorWidget from 'src/components/widgets/ErrorWidget';
import ListWidget from 'src/components/widgets/ListWidget';
import { ParamEventTypes, ParamStrategyUpdateKind, StrategyUpdateResponseItem } from 'src/types/api';

export default async function TxStrategyUpdateDetailsWidget({
    epochId,
    txOrdinal,
    event,
}: {
    epochId: string;
    txOrdinal: string;
    event: ParamEventTypes;
}) {
    const headersList = headers();
    const host = headersList.get('host') || '';
    const response = await statsApiListFetcher<StrategyUpdateResponseItem>(
        `/strategy_updates?order=desc&limit=1&epoch=${epochId}&txOrdinal=${+txOrdinal + 1}`,
        host,
    );
    if (!response.success || response.value.length === 0) {
        return <ErrorWidget title="Transaction Details" />;
    }

    const [su] = response.value;
    const priceFormatter = getPriceFormatter();

    const kind = Object.values(ParamStrategyUpdateKind)[su.kind] || '';

    const listItems = [
        {
            label: 'Event',
            value: kind,
        },
        {
            label: 'Account',
            value: <TraderLink trader={su.trader} />,
        },
        {
            label: 'Strategy',
            value: su.strategyId ? (
                <StrategyLink trader={su.trader} strategyId={su.strategyId} />
            ) : (
                <span>{su.strategyIdHash}</span>
            ),
        },
        {
            label: 'Amount',
            value: <span>{priceFormatter.format(new BigNumber(su.amount)?.toNumber())} USDC</span>,
        },
    ];

    if (su.txHash) {
        listItems.push({
            label: 'Ethereum Tx Hash',
            value: <EtherscanLink txHash={su.txHash} />,
        });
    }

    return (
        <ListWidget
            title={'Transaction Details'}
            lastUpdatedTimestamp={response.timestamp}
            listItems={listItems}
        ></ListWidget>
    );
}
