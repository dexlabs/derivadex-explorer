import { getPriceFormatter } from '@utils';
import { headers } from 'next/headers';
import OrderLink from 'src/components/links/OrderLink';
import ErrorWidget from 'src/components/widgets/ErrorWidget';
import TableWidget from 'src/components/widgets/TableWidget';
import { statsApiTxOrderCancelFetcher } from 'src/data/order/fetchers';
import { ParamOrderSide } from 'src/data/order/types';
import { ParamEventTypes } from 'src/types/api';

export default async function TxOrderCancelWidget({
    epochId,
    txOrdinal,
    event,
}: {
    epochId: string;
    txOrdinal: string;
    event: ParamEventTypes;
}) {
    const headersList = headers();
    const host = headersList.get('host') || '';
    const response = await statsApiTxOrderCancelFetcher(epochId, txOrdinal, host);

    if (!response) {
        return <ErrorWidget title="Cancelled Orders" />;
    }

    const priceFormatter = getPriceFormatter();

    const columns = [
        {
            Header: 'Order',
            accessor: 'order',
            collapse: false,
        },
        {
            Header: 'Price',
            accessor: 'price',
            collapse: false,
            isNumeric: true,
        },
        {
            Header: 'Remaining Amount',
            accessor: 'remainingAmount',
            collapse: false,
            isNumeric: true,
        },
    ];

    const tableData = response.value.map((it) => ({
        order: (
            <div>
                <span>{` ${Object.values(ParamOrderSide)[it.side]} | `}</span>
                <OrderLink trader={it.traderAddress} strategyId={it.strategyId} orderHash={it.orderHash} />
            </div>
        ),
        price: <div>${priceFormatter.format(it.price.toNumber())}</div>,
        remainingAmount: <div>{`${it.remainingAmount.toString()} of ${it.amount.toString()}`}</div>,
    }));

    return <TableWidget lastUpdatedTimestamp={response.timestamp} columns={columns} data={tableData} />;
}
