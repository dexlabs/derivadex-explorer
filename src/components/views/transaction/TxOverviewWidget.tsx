'use client';

import { formatAge, getCurrentDateAndTime } from '@utils';
import Link from 'next/link';

import { ParamEventTypes, TxLogResponseItem } from '../../../types/api';
import ListWidget from '../../widgets/ListWidget';

export default function TxOverviewWidget({
    transaction,
    lastUpdatedTimestamp,
}: {
    transaction: TxLogResponseItem;
    lastUpdatedTimestamp: number;
}) {
    const indexOf = Object.values(ParamEventTypes).indexOf(transaction.eventKind as unknown as ParamEventTypes);
    const key = Object.keys(ParamEventTypes)[indexOf];
    const eventType = key.toString().replace('_', ' ');

    return (
        <ListWidget
            title={'Transaction Overview'}
            lastUpdatedTimestamp={lastUpdatedTimestamp}
            listItems={[
                {
                    label: 'Epoch',
                    value: <Link href={`/epoch/${transaction.epochId}`}>{transaction.epochId}</Link>,
                },
                {
                    label: 'TxOrdinal',
                    value: transaction.txOrdinal,
                },
                {
                    label: 'Event',
                    value: eventType,
                },
                {
                    label: 'Age',
                    value: `${formatAge(+transaction.timeStamp)}`,
                },
                {
                    label: 'Timestamp',
                    value: getCurrentDateAndTime(new Date(+transaction.timeStamp)),
                },
            ]}
        ></ListWidget>
    );
}
