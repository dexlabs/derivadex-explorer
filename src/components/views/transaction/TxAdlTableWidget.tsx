import { getPriceFormatter, statsApiListFetcher } from '@utils';
import { headers } from 'next/headers';
import PositionLink from 'src/components/links/PositionLink';
import StrategyLink from 'src/components/links/StrategyLink';
import TraderLink from 'src/components/links/TraderLink';
import ErrorWidget from 'src/components/widgets/ErrorWidget';
import TableWidget from 'src/components/widgets/TableWidget';
import { AdlResponseItem, ParamEventTypes } from 'src/types/api';

export default async function TxAdlTableWidget({
    epochId,
    txOrdinal,
    event,
}: {
    epochId: string;
    txOrdinal: string;
    event: ParamEventTypes;
}) {
    const headersList = headers();
    const host = headersList.get('host') || '';
    const response = await statsApiListFetcher<AdlResponseItem>(
        `/adl?order=desc&epoch=${epochId}&txOrdinal=${+txOrdinal + 1}`,
        host,
    );
    if (!response.success) {
        return <ErrorWidget title="Adl" />;
    }
    // response will contain all transactions previous to epoch, txOrdinal passed in params
    const filtered = response.value.filter((it) => it.epochId === epochId && it.txOrdinal === txOrdinal);

    const columns = [
        {
            Header: 'Trader',
            accessor: 'trader',
            collapse: false,
        },
        {
            Header: 'Strategy',
            accessor: 'strategy',
            collapse: false,
        },
        {
            Header: 'Adl Amount',
            accessor: 'amount',
            collapse: false,
            isNumeric: true,
        },
    ];

    const tableData = filtered.map((it) => ({
        trader: (
            <div>
                <TraderLink trader={it.adlTrader} />
            </div>
        ),
        strategy: (
            <div>
                <StrategyLink trader={it.adlTrader} strategyId="main" />
            </div>
        ),
        amount: (
            <div>
                <span>{`${it.amount.toString()} `}</span>
                <PositionLink trader={it.adlTrader} strategyId="main" symbol={it.symbol} text={'View Position'} />
            </div>
        ),
    }));

    return <TableWidget lastUpdatedTimestamp={response.timestamp} columns={columns} data={tableData} />;
}
