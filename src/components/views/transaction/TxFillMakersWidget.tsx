import { getPriceFormatter } from '@utils';
import { headers } from 'next/headers';
import OrderLink from 'src/components/links/OrderLink';
import PositionLink from 'src/components/links/PositionLink';
import StrategyLink from 'src/components/links/StrategyLink';
import TraderLink from 'src/components/links/TraderLink';
import ErrorWidget from 'src/components/widgets/ErrorWidget';
import TableWidget from 'src/components/widgets/TableWidget';
import { statsApiTxFillMakersFetcher } from 'src/data/fill/fetchers';
import { ParamOrderSide } from 'src/data/order/types';
import { ParamEventTypes } from 'src/types/api';

export default async function TxFillAffectedWidget({
    epochId,
    txOrdinal,
    event,
}: {
    epochId: string;
    txOrdinal: string;
    event: ParamEventTypes;
}) {
    const headersList = headers();
    const host = headersList.get('host') || '';
    const response = await statsApiTxFillMakersFetcher(epochId, txOrdinal, host);

    if (!response) {
        return <ErrorWidget title="Makers" />;
    }
    const priceFormatter = getPriceFormatter();

    const columns = [
        {
            Header: 'Trader',
            accessor: 'trader',
            collapse: false,
        },
        {
            Header: 'Strategy',
            accessor: 'strategy',
            collapse: false,
        },
        {
            Header: 'Order Hash',
            accessor: 'orderHash',
            collapse: false,
        },
        {
            Header: 'Price',
            accessor: 'price',
            collapse: false,
        },
        {
            Header: 'Amount',
            accessor: 'amount',
            collapse: false,
        },
        {
            Header: 'Notional Value',
            accessor: 'notional',
            collapse: false,
        },
    ];

    const tableData = response.value.affected.map((it) => ({
        trader: (
            <div>
                <TraderLink trader={it.traderAddress} />
            </div>
        ),
        strategy: (
            <div>
                <StrategyLink trader={it.traderAddress} strategyId={it.strategyId} />
            </div>
        ),
        orderHash: (
            <div>
                <span>{` ${Object.values(ParamOrderSide)[it.side]} | `}</span>
                <OrderLink trader={it.traderAddress} strategyId={it.strategyId} orderHash={it.orderHash} />
            </div>
        ),
        price: <div>${priceFormatter.format(it.price.toNumber())}</div>,
        amount: (
            <div>
                <span>{` ${it.amount.toString()} `}</span>
                <PositionLink
                    trader={it.traderAddress}
                    strategyId={it.strategyId}
                    symbol={it.symbol}
                    text={'View Position'}
                />
            </div>
        ),
        notional: <div>${priceFormatter.format(it.price.multipliedBy(it.amount).toNumber())}</div>,
    }));

    return <TableWidget lastUpdatedTimestamp={response.timestamp} columns={columns} data={tableData} />;
}
