import { formatAge, getPriceFormatter, statsApiListFetcher } from '@utils';
import BigNumber from 'bignumber.js';
import { headers } from 'next/headers';
import ErrorWidget from 'src/components/widgets/ErrorWidget';
import TableWidget from 'src/components/widgets/TableWidget';

import { MarkPricesResponseItem } from '@/types/api';

export default async function TxPricesTableWidget({ epochId, txOrdinal }: { epochId: string; txOrdinal: string }) {
    const headersList = headers();
    const host = headersList.get('host') || '';
    const response = await statsApiListFetcher<MarkPricesResponseItem>(
        `/price_checkpoints?order=desc&epoch=${epochId}&txOrdinal=${+txOrdinal + 1}`,
        host,
    );
    if (!response.success || response.value.length === 0) {
        return <ErrorWidget title="Prices" />;
    }

    // response will contain all transactions previous to epoch, txOrdinal passed in params
    const filtered = response.value.filter((it) => it.epochId === epochId && it.txOrdinal === txOrdinal);

    const priceFormatter = getPriceFormatter();

    const columns = [
        {
            Header: 'Age',
            accessor: 'age',
            collapse: false,
        },
        {
            Header: 'Symbol',
            accessor: 'symbol',
            collapse: false,
        },
        {
            Header: 'Index Price',
            accessor: 'index',
            collapse: false,
            isNumeric: true,
        },
        {
            Header: 'EMA',
            accessor: 'ema',
            collapse: false,
            isNumeric: true,
        },
        {
            Header: 'Mark Price',
            accessor: 'mark',
            collapse: false,
            isNumeric: true,
        },
    ];

    const tableData = filtered.map((it, index) => {
        return {
            age: formatAge(new Date(it.createdAt).getTime()),
            index: `$${priceFormatter.format(new BigNumber(it.indexPrice).toNumber())}`,
            symbol: `${it.symbol}`,
            ema: `${new BigNumber(it.ema).toString()}`,
            mark: `$${priceFormatter.format(new BigNumber(it.markPrice).toNumber())}`,
        };
    });

    return <TableWidget lastUpdatedTimestamp={response.timestamp} columns={columns} data={tableData} />;
}
