import { getPriceFormatter, statsApiListFetcher } from '@utils';
import BigNumber from 'bignumber.js';
import { headers } from 'next/headers';
import MarketLink from 'src/components/links/MarketLink';
import ErrorWidget from 'src/components/widgets/ErrorWidget';
import { InsuranceFundResponseItem, LiquidationsResponseItem, ParamEventTypes } from 'src/types/api';

import ListWidget from '../../widgets/ListWidget';

export default async function TxLiquidationDetailsWidget({
    epochId,
    txOrdinal,
    event,
}: {
    epochId: string;
    txOrdinal: string;
    event: ParamEventTypes;
}) {
    const headersList = headers();
    const host = headersList.get('host') || '';
    const [liquidationResponse, insuranceFundResponse] = await Promise.all([
        statsApiListFetcher<LiquidationsResponseItem>(
            `/liquidations?order=desc&epoch=${epochId}&txOrdinal=${+txOrdinal + 1}`,
            host,
        ),
        statsApiListFetcher<InsuranceFundResponseItem>(
            `/insurance_fund?order=desc&limit=2&epoch=${epochId}&txOrdinal=${+txOrdinal + 1}`,
            host,
        ),
    ]);
    if (
        !liquidationResponse.success ||
        liquidationResponse.value.length === 0 ||
        !insuranceFundResponse.success ||
        insuranceFundResponse.value.length !== 2
    ) {
        return <ErrorWidget title="Details" />;
    }
    // response will contain all transactions previous to epoch, txOrdinal passed in params
    const filtered = liquidationResponse.value.filter((it) => it.epochId === epochId && it.txOrdinal === txOrdinal);

    if (filtered.length === 0) {
        return <ErrorWidget title="Details" />;
    }

    // Just need one, the trigger and insurance should be the same
    // The ordinal is to refer to different traders / strategies / markets
    const [liquidation] = filtered;

    const insuranceFundDiff = new BigNumber(insuranceFundResponse.value[0].totalCapitalization).minus(
        insuranceFundResponse.value[1].totalCapitalization,
    );

    const priceFormatter = getPriceFormatter();

    /**
     * TODO: Should be able to fetch the mark price by hash (currently stats api does not allow that)
     * For now assuming the market symbol & price from the respective row data, but that can be from a different
     * price hash, for example when multiple markets of same strategy being liquidated, but only mark price from specific market triggered that
     */
    const triggeredSymbol = liquidation.symbol;
    const triggeredPrice = liquidation.markPrice;

    return (
        <ListWidget
            title={'Details'}
            lastUpdatedTimestamp={liquidationResponse.timestamp}
            listItems={[
                {
                    label: 'Triggered By Market',
                    value: <MarketLink symbol={triggeredSymbol} />,
                },
                {
                    label: 'Triggered by Price',
                    value: `$${priceFormatter.format(+triggeredPrice)}`,
                },
                {
                    label: 'Insurance Fund Capitalization',
                    value: `${insuranceFundDiff.isPositive() ? '+' : '-'}$${priceFormatter.format(
                        +insuranceFundDiff.abs(),
                    )} ($${priceFormatter.format(+liquidation.insuranceFundCapitalization)} total)`,
                },
            ]}
        ></ListWidget>
    );
}
