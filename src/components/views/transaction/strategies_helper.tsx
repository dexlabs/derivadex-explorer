import { getPriceFormatter } from '@utils';
import BigNumber from 'bignumber.js';
import StrategyLink from 'src/components/links/StrategyLink';
import TraderLink from 'src/components/links/TraderLink';

export const parseTableData = (response, extraData: { [key: string]: string }) => {
    const priceFormatter = getPriceFormatter();
    // response will contain all transactions previous to epoch, txOrdinal passed in params
    const filtered = response.value.filter(
        (it) => it.epochId === extraData.epochId && it.txOrdinal === extraData.txOrdinal,
    );
    return filtered.map((it) => ({
        trader: (
            <div>
                <TraderLink trader={it.trader} />
            </div>
        ),
        strategy: (
            <div>
                <StrategyLink trader={it.trader} strategyId={'main'} />
            </div>
        ),
        amount: (
            <div>
                <span>{priceFormatter.format(new BigNumber(it.amount)?.toNumber())} USDC</span>
            </div>
        ),
        collateral: (
            <div>
                <span>{priceFormatter.format(new BigNumber(it.newAvailCollateral)?.toNumber())} USDC</span>
            </div>
        ),
    }));
};
