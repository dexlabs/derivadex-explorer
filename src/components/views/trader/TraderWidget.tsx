import { getPriceFormatter, statsApiSingleFetcher } from '@utils';
import BigNumber from 'bignumber.js';
import { headers } from 'next/headers';
import TraderLink from 'src/components/links/TraderLink';
import ErrorWidget from 'src/components/widgets/ErrorWidget';

import { TradersResponseItem } from '@/types/api';

import ListWidget from '../../widgets/ListWidget';

export default async function TraderWidget({ trader }: { trader: string }) {
    const headersList = headers();
    const host = headersList.get('host') || '';
    const response = await statsApiSingleFetcher<TradersResponseItem>(`/account/${trader}`, host);
    if (!response.success) {
        return <ErrorWidget title="Account Details" />;
    }
    const priceFormatter = getPriceFormatter();
    const traderResponseValue = response.value;
    return (
        <ListWidget
            title={'Account Details'}
            lastUpdatedTimestamp={response.timestamp}
            listItems={[
                {
                    label: 'Account',
                    value: <TraderLink trader={trader} />,
                },
                {
                    label: 'Free DDX',
                    value: priceFormatter.format(new BigNumber(traderResponseValue.availDdx).toNumber()),
                },
                {
                    label: 'Frozen DDX',
                    value: priceFormatter.format(new BigNumber(traderResponseValue.lockedDdx).toNumber()),
                },
                {
                    label: 'Pay Fees In DDX',
                    value: traderResponseValue.payFeesInDdx ? 'Enabled' : 'Disabled',
                },
            ]}
        ></ListWidget>
    );
}
