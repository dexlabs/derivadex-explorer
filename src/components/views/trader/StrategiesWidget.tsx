import { statsApiListFetcher } from '@utils';
import { headers } from 'next/headers';
import Link from 'next/link';
import ErrorWidget from 'src/components/widgets/ErrorWidget';

import { StrategyResponseItem } from '@/types/api';

import ListWidget from '../../widgets/ListWidget';

export default async function StrategiesWidget({ trader }: { trader: string }) {
    const headersList = headers();
    const host = headersList.get('host') || '';
    const strategiesResponse = await statsApiListFetcher<StrategyResponseItem>(`/account/${trader}/strategies`, host);
    if (!strategiesResponse.success) {
        return <ErrorWidget title="Owned Strategies" />;
    }
    return (
        <ListWidget
            title={'Owned Strategies'}
            lastUpdatedTimestamp={Date.now()}
            listItems={strategiesResponse.value.map((strategy) => {
                return {
                    value: (
                        <Link href={`/account/${trader}/strategy/${strategy.strategyId}`}>{strategy.strategyId}</Link>
                    ),
                };
            })}
        ></ListWidget>
    );
}
