import { formatAge, getPriceFormatter } from '@utils';
import BigNumber from 'bignumber.js';
import TransactionLink from 'src/components/links/TransactionLink';
import { ParamTraderHistoryEvent } from 'src/data/trader/types';

export const parseTableData = (response, extraData: { [key: string]: string }) => {
    const priceFormatter = getPriceFormatter();
    return response.value.map((it) => {
        const kind = Object.values(ParamTraderHistoryEvent)[it.event] || '';
        return {
            event: (
                <div>
                    <div>{kind.toString().replaceAll('_', ' ')}</div>
                    <TransactionLink epochId={it.epochId.toString()} txOrdinal={it.txOrdinal.toString()} />
                </div>
            ),
            amount: <div>{`${priceFormatter.format(new BigNumber(it.amount).toNumber())} DDX`}</div>,
            age: <div>{formatAge(new Date(it.createdAt).getTime())}</div>,
        };
    });
};
