'use client';

import { timeseriesLocalAPIHandler } from '@utils';
import { Column } from 'react-table';
import { DefaultPagination } from 'src/components/widgets/TablePaginationWidget';
import TableTimeseriesPaginationWidget from 'src/components/widgets/TableTimeseriesPaginationWidget';
import { TraderHistoryEntity } from 'src/data/trader/types';
import { Jsonify, StatsAPITimeseriesResponse } from 'src/data/types';

import { parseTableData as traderParseTableData } from './helper';

type DataTableProps = {
    lastUpdatedTimestamp: number;
    initialData: StatsAPITimeseriesResponse<Jsonify<TraderHistoryEntity>>;
    columns: Column<any>[];
    pagination: DefaultPagination;
};

export default function TraderHistoryTablePaginationWidget({
    lastUpdatedTimestamp,
    columns,
    initialData,
    pagination,
}: DataTableProps) {
    return (
        <TableTimeseriesPaginationWidget
            lastUpdatedTimestamp={lastUpdatedTimestamp}
            columns={columns}
            initialData={initialData}
            pagination={pagination}
            dataParser={traderParseTableData}
            fetcher={timeseriesLocalAPIHandler}
        />
    );
}
