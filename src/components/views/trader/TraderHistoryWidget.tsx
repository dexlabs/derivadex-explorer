import { timeseriesLocalAPIHandler } from '@utils';
import { headers } from 'next/headers';
import ErrorWidget from 'src/components/widgets/ErrorWidget';
import { TraderHistoryEntity } from 'src/data/trader/types';
import { buildUrl } from 'src/data/utils';

import TraderHistoryTablePaginationWidget from './TraderHistoryTablePaginationWidget';

export default async function TraderHistoryWidget({ trader }: { trader: string }) {
    const headersList = headers();
    const host = headersList.get('host') || '';
    const url = '/trader_history';
    const params = { trader, limit: 5 };
    const response = await timeseriesLocalAPIHandler<TraderHistoryEntity>(buildUrl(url, params), host);
    if (!response || !response.success) {
        return <ErrorWidget title="" />;
    }
    const columns = [
        {
            Header: 'Age',
            accessor: 'age',
            collapse: false,
            isNumeric: false,
        },
        {
            Header: 'Event',
            accessor: 'event',
            collapse: false,
        },
        {
            Header: 'Amount',
            accessor: 'amount',
            collapse: false,
            isNumeric: true,
        },
    ];

    return (
        <TraderHistoryTablePaginationWidget
            lastUpdatedTimestamp={response.timestamp}
            columns={columns}
            initialData={response}
            pagination={{
                url,
                host,
                params,
                extraData: { trader },
            }}
        />
    );
}
