import { formatAge } from '@utils';
import TraderLink from 'src/components/links/TraderLink';
import TransactionLink from 'src/components/links/TransactionLink';
import { OrderHistoryItem, ParamOrderHistoryEvent } from 'src/data/order/types';
import { Jsonify } from 'src/data/types';

export const parseTableData = (response, extraData?: { [key: string]: string }) => {
    return response.value.map((it: Jsonify<OrderHistoryItem>) => ({
        event: (
            <>
                <div>{Object.values(ParamOrderHistoryEvent)[it.event]}</div>
                <TransactionLink epochId={it.epochId} txOrdinal={it.txOrdinal} />
            </>
        ),
        trader: it.tradedBy ? <TraderLink trader={it.tradedBy} /> : '--',
        filled: <div>{it.filledAmount !== '0' ? it.filledAmount : '--'}</div>,
        amount: <div>{it.remainingAmount}</div>,
        age: <div>{formatAge(new Date(it.createdAt).getTime())}</div>,
    }));
};
