import { getPriceFormatter, statsApiListFetcher } from '@utils';
import BigNumber from 'bignumber.js';
import { headers } from 'next/headers';
import Link from 'next/link';
import OrderLink from 'src/components/links/OrderLink';
import ErrorWidget from 'src/components/widgets/ErrorWidget';
import TableWidget from 'src/components/widgets/TableWidget';
import { OrderBookResponseItem, ParamOrderSide } from 'src/data/order/types';

export default async function OpenOrdersWidget({ trader, strategyId }: { trader: string; strategyId: string }) {
    const headersList = headers();
    const host = headersList.get('host') || '';
    const response = await statsApiListFetcher<OrderBookResponseItem>(
        `/account/${trader}/strategy/${strategyId}/order_book?order=desc&limit=10`,
        host,
    );
    if (!response.success) {
        return <ErrorWidget title="Open Orders" />;
    }
    const priceFormatter = getPriceFormatter();
    const columns = [
        {
            Header: 'Order',
            accessor: 'orderHash',
            collapse: false,
        },
        {
            Header: 'Symbol',
            accessor: 'symbol',
            collapse: false,
        },
        {
            Header: 'Side',
            accessor: 'side',
            collapse: false,
            isNumeric: true,
        },
        {
            Header: 'Amount',
            accessor: 'amount',
            collapse: false,
            isNumeric: true,
        },
        {
            Header: 'Remaining',
            accessor: 'remaining',
            collapse: false,
            isNumeric: true,
        },
        {
            Header: 'Price',
            accessor: 'price',
            collapse: false,
            isNumeric: true,
        },
    ];

    const tableData = response.value.map((it) => ({
        orderHash: <OrderLink trader={trader} strategyId={'main'} orderHash={it.orderHash} />,
        symbol: (
            <Link href={`/market/${it.symbol}`}>
                <div>{it.symbol}</div>
            </Link>
        ),
        side: <div>{Object.values(ParamOrderSide)[it.side]}</div>,
        amount: <div>{it.originalAmount}</div>,
        remaining: <div>{it.amount}</div>,
        price: <div>${priceFormatter.format(new BigNumber(it.price).toNumber())}</div>,
    }));

    return <TableWidget lastUpdatedTimestamp={response.timestamp} columns={columns} data={tableData} />;
}
