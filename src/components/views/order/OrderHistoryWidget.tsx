import { timeseriesLocalAPIHandler } from '@utils';
import { headers } from 'next/headers';
import ErrorWidget from 'src/components/widgets/ErrorWidget';
import { OrderHistoryItem } from 'src/data/order/types';
import { buildUrl } from 'src/data/utils';

import OrderHistoryTablePaginationWidget from './OrderHistoryTablePaginationWidget';

export default async function OrderHistoryWidget({ orderHash }: { orderHash: string }) {
    const headersList = headers();
    const host = headersList.get('host') || '';
    const url = '/order_history';
    const params = { orderHash, limit: 5 };
    const response = await timeseriesLocalAPIHandler<OrderHistoryItem>(buildUrl(url, params), host);
    if (!response.success) {
        return <ErrorWidget title="" />;
    }

    const columns = [
        {
            Header: 'Age',
            accessor: 'age',
            collapse: false,
        },
        {
            Header: 'Event',
            accessor: 'event',
            collapse: false,
        },
        {
            Header: 'Filled Amount',
            accessor: 'filled',
            collapse: false,
            isNumeric: true,
        },
        {
            Header: 'Remaining Amount',
            accessor: 'amount',
            collapse: false,
            isNumeric: true,
        },
        {
            Header: 'Filled By Trader',
            accessor: 'trader',
            collapse: false,
        },
    ];

    return (
        <OrderHistoryTablePaginationWidget
            lastUpdatedTimestamp={response.timestamp}
            columns={columns}
            initialData={response}
            pagination={{ url, host, params }}
        />
    );
}
