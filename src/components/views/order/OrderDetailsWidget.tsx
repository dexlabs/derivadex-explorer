import { getDateAndTime, getPriceFormatter, shortenHex } from '@utils';
import BigNumber from 'bignumber.js';
import { headers } from 'next/headers';
import Link from 'next/link';
import CopyButton from 'src/components/links/CopyButton';
import MarketLink from 'src/components/links/MarketLink';
import StrategyLink from 'src/components/links/StrategyLink';
import TraderLink from 'src/components/links/TraderLink';
import ErrorWidget from 'src/components/widgets/ErrorWidget';
import ListWidget from 'src/components/widgets/ListWidget';
import { statsApiOrderDetailsFetcher } from 'src/data/order/fetchers';
import { OrderRejectReason, ParamOrderSide } from 'src/data/order/types';
import { orderRejectionReasonEnumToString, orderTypeEnumToString } from 'src/data/order/utils';

export default async function OrderDetailsWidget({
    trader,
    strategyId,
    orderHash,
}: {
    trader: string;
    strategyId: string;
    orderHash: string;
}) {
    const headersList = headers();
    const host = headersList.get('host') || '';
    const response = await statsApiOrderDetailsFetcher(trader, orderHash, host);
    if (!response.success) {
        return <ErrorWidget title="Order Details" />;
    }
    const priceFormatter = getPriceFormatter();
    const orderResponseValue = response.value;
    return (
        <ListWidget
            title={'Order Details'}
            lastUpdatedTimestamp={response.timestamp}
            listItems={[
                {
                    label: 'Order Hash',
                    value: (
                        <span style={{ whiteSpace: 'nowrap' }}>
                            {shortenHex(orderResponseValue.orderHash)}
                            <CopyButton content={orderHash} description="Copy order hash" />
                        </span>
                    ),
                },
                {
                    label: 'Symbol',
                    value: <MarketLink symbol={orderResponseValue.symbol} />,
                },
                {
                    label: 'Side',
                    value: ParamOrderSide[orderResponseValue.side],
                },
                {
                    label: 'Amount',
                    value: priceFormatter.format(orderResponseValue.amount.toNumber()),
                },
                {
                    label: 'Price',
                    value: orderResponseValue.price.gt(0)
                        ? `$${priceFormatter.format(orderResponseValue.price.toNumber())}`
                        : '--',
                },
                {
                    label: 'Account',
                    value: <TraderLink trader={trader} />,
                },
                {
                    label: 'Strategy',
                    value: <StrategyLink trader={trader} strategyId={orderResponseValue.strategyId} />,
                },
                {
                    label: 'Order Type',
                    value: orderTypeEnumToString(orderResponseValue.orderType),
                },
                {
                    label: 'Remaining Amount',
                    value: priceFormatter.format(orderResponseValue.remainingAmount.toNumber()),
                },
                {
                    label: 'Rejected Amount',
                    value: priceFormatter.format(orderResponseValue.rejectedAmount.toNumber()),
                },
                {
                    label: 'Rejected Reason',
                    value: orderRejectionReasonEnumToString(orderResponseValue.rejectedReason),
                },
                {
                    label: 'Created At',
                    value: getDateAndTime(orderResponseValue.createdAt),
                },
            ]}
        ></ListWidget>
    );
}
