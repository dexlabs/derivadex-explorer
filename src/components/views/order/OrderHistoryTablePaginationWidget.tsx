'use client';

import { timeseriesLocalAPIHandler } from '@utils';
import { Column } from 'react-table';
import TableTimeseriesPaginationWidget from 'src/components/widgets/TableTimeseriesPaginationWidget';
import { OrderHistoryItem } from 'src/data/order/types';
import { Jsonify, StatsAPIListResponse, StatsAPISuccessTimeseriesResponse } from 'src/data/types';

import TablePaginationWidget, { DefaultPagination } from '../../widgets/TablePaginationWidget';
import { parseTableData } from './helper';

type DataTableProps = {
    lastUpdatedTimestamp: number;
    initialData: StatsAPIListResponse<Jsonify<OrderHistoryItem>>;
    columns: Column<any>[];
    pagination: DefaultPagination;
};

export default function OrderHistoryTablePaginationWidget({
    lastUpdatedTimestamp,
    columns,
    initialData,
    pagination,
}: DataTableProps) {
    const getNextPageQueryParams = (
        previousRequest: string,
        previousResultSet: StatsAPIListResponse<Jsonify<OrderHistoryItem>>,
    ): { [key: string]: string } => {
        const timeSeriesResponse = previousResultSet as StatsAPISuccessTimeseriesResponse<Jsonify<OrderHistoryItem>>;
        return {
            epoch: timeSeriesResponse.nextEpoch?.toString() || '',
            txOrdinal: timeSeriesResponse.nextTxOrdinal?.toString() || '',
            ordinal: timeSeriesResponse.nextOrdinal?.toString() || '',
            lookbackAmount: timeSeriesResponse.value[timeSeriesResponse.value.length - 1].remainingAmount,
        };
    };
    return (
        <TablePaginationWidget
            lastUpdatedTimestamp={lastUpdatedTimestamp}
            columns={columns}
            initialData={initialData}
            pagination={pagination}
            dataParser={parseTableData}
            getNextPageQueryParams={getNextPageQueryParams}
            fetcher={timeseriesLocalAPIHandler}
        />
    );
}
