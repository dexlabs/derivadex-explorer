import { getPriceFormatter, sideEnumToString } from '@utils';
import BigNumber from 'bignumber.js';
import PositionLink from 'src/components/links/PositionLink';
import StrategyLink from 'src/components/links/StrategyLink';
import TraderLink from 'src/components/links/TraderLink';

export const parseTableData = (response, extraData: { [key: string]: string }) => {
    const priceFormatter = getPriceFormatter();
    const markPrice = new BigNumber(extraData.markPrice);
    return response.value.map((it) => ({
        account: (
            <div>
                <TraderLink trader={it.trader} />
            </div>
        ),
        strategy: (
            <div>
                <StrategyLink trader={it.trader} strategyId={'main'} />
            </div>
        ),
        position: (
            <div>
                <span>
                    {sideEnumToString(it.side)} |{' '}
                    <PositionLink
                        trader={it.trader}
                        strategyId="main"
                        symbol={it.symbol}
                        text={it.balance.toString()}
                    />
                </span>
            </div>
        ),
        notional: <div>${priceFormatter.format(markPrice.multipliedBy(it.balance).toNumber())}</div>,
    }));
};
