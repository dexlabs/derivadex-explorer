'use client';

import { Column } from 'react-table';
import { Jsonify, StatsAPIListResponse } from 'src/data/types';

import { PositionResponseItem } from '@/types/api';

import TableOffsetPaginationWidget from '../../widgets/TableOffsetPaginationWidget';
import { DefaultPagination } from '../../widgets/TablePaginationWidget';
import { parseTableData as positionsParseTableData } from './helper';

type DataTableProps = {
    lastUpdatedTimestamp: number;
    initialData: StatsAPIListResponse<Jsonify<PositionResponseItem>>;
    columns: Column<any>[];
    pagination: DefaultPagination;
};

export default function PositionsTablePaginationWidget({
    lastUpdatedTimestamp,
    columns,
    initialData,
    pagination,
}: DataTableProps) {
    return (
        <TableOffsetPaginationWidget
            lastUpdatedTimestamp={lastUpdatedTimestamp}
            columns={columns}
            initialData={initialData}
            pagination={pagination}
            dataParser={positionsParseTableData}
        />
    );
}
