import { headers } from 'next/headers';
import Link from 'next/link';
import ErrorWidget from 'src/components/widgets/ErrorWidget';
import TableWidget from 'src/components/widgets/TableWidget';

import { PriceFeedSpecsResponseItem } from '../../../types/api';
import { statsApiListFetcher } from '../../../utils';

export default async function MarketPriceFeedsWidget({ symbol }: { symbol: string }) {
    const headersList = headers();
    const host = headersList.get('host') || '';
    const response = await statsApiListFetcher<PriceFeedSpecsResponseItem>('/specs?kind=1', host);
    if (!response.success) {
        return <ErrorWidget title="Market Gateway" />;
    }
    const sourcesForMarketSymbol = response.value
        .filter((it) => it.value.symbols.includes(symbol.slice(0, -1)))
        .map((it) => it.name);

    const columns = [
        {
            Header: 'Source',
            accessor: 'feed',
            collapse: false,
        },
    ];

    const tableData = sourcesForMarketSymbol.map((it) => ({
        feed: (
            <Link href={`/market_gateway/${it.replace(/\./g, '_')}`}>
                <div>{it}</div>
            </Link>
        ),
    }));

    return <TableWidget lastUpdatedTimestamp={response.timestamp} columns={columns} data={tableData} />;
}
