import BigNumber from 'bignumber.js';
import { headers } from 'next/headers';
import PositionsTablePaginationWidget from 'src/components/views/market/PositionsTablePaginationWidget';
import ErrorWidget from 'src/components/widgets/ErrorWidget';
import { buildUrl } from 'src/data/utils';

import { MarketResponseItem, PositionResponseItem } from '../../../types/api';
import { statsApiListFetcher } from '../../../utils';

export default async function PositionsWidget({ symbol }: { symbol: string }) {
    const headersList = headers();
    const host = headersList.get('host') || '';
    // TODO: We should add sort by balance capability to a v2 of the positions endpoint API.
    const url = '/positions';
    const params = { symbol, limit: 5 };
    const [marketsResponse, positionsResponse] = await Promise.all([
        statsApiListFetcher<MarketResponseItem>(`/markets?symbol=${symbol}`, host),
        statsApiListFetcher<PositionResponseItem>(buildUrl(url, params), host),
    ]);
    if (!marketsResponse.success || !positionsResponse.success) {
        return <ErrorWidget title="Positions" />;
    }

    const columns = [
        {
            Header: 'Account',
            accessor: 'account',
            collapse: false,
        },
        {
            Header: 'Strategy',
            accessor: 'strategy',
            collapse: false,
        },
        {
            Header: 'Position',
            accessor: 'position',
            collapse: false,
            isNumeric: true,
        },
        {
            Header: 'Notional Value',
            accessor: 'notional',
            collapse: false,
            isNumeric: true,
        },
    ];

    return (
        <PositionsTablePaginationWidget
            lastUpdatedTimestamp={positionsResponse.timestamp}
            columns={columns}
            initialData={positionsResponse}
            pagination={{ url, host, params, extraData: { markPrice: marketsResponse.value[0].price } }}
        />
    );
}
