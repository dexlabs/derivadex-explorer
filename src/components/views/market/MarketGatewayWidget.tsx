import { statsApiListFetcher } from '@utils';
import { headers } from 'next/headers';
import Link from 'next/link';
import { Suspense } from 'react';
import ErrorWidget from 'src/components/widgets/ErrorWidget';
import TableWidget from 'src/components/widgets/TableWidget';
import WidgetFlexContainer from 'src/components/widgets/WidgetFlexContainer';
import WidgetSkeleton from 'src/components/widgets/WidgetSkeleton';
import WidgetTabbedContainer from 'src/components/widgets/WidgetTabbedContainer';

import { PriceFeedSpecsResponseItem } from '@/types/api';

import ListWidget from '../../widgets/ListWidget';

export default async function MarketGatewayWidget({ name }: { name: string }) {
    const headersList = headers();
    const host = headersList.get('host') || '';
    const response = await statsApiListFetcher<PriceFeedSpecsResponseItem>('/specs?kind=1', host);
    if (!response.success) {
        return <ErrorWidget title="Market Gateway Details" />;
    }
    const responseValue = response.value.find((it) => it.name === name);
    if (responseValue === undefined) {
        return <ErrorWidget title="Market Gateway Details" />;
    }
    const symbols = responseValue.value.symbols.filter((it) => it !== 'DDX');
    const columns = [
        {
            Header: 'Symbol',
            accessor: 'symbol',
            collapse: false,
        },
    ];
    const tableData = symbols.map((symbol) => ({
        symbol: (
            <Link href={`/market/${symbol}P`}>
                <div>{symbol}P</div>
            </Link>
        ),
    }));
    return (
        <>
            <WidgetFlexContainer>
                <Suspense fallback={<WidgetSkeleton title={'Market Gateway Details'} />}>
                    <ListWidget
                        title={'Market Gateway Details'}
                        lastUpdatedTimestamp={response.timestamp}
                        listItems={[
                            {
                                label: 'Hostname',
                                value: `${responseValue.value.hostname}`,
                            },
                            {
                                label: 'Quote',
                                value: `${responseValue.value.quote}`,
                            },
                            {
                                label: 'Spot Price Source',
                                value: `${responseValue.value.spotPriceOp.query.Transform.Format}`,
                            },
                            {
                                label: 'Time Source',
                                value: `${responseValue.value.timeOp.query.Literal}`,
                            },
                        ]}
                    ></ListWidget>
                </Suspense>
            </WidgetFlexContainer>
            <WidgetTabbedContainer
                tabNames={['Markets']}
                tabPanels={[
                    <Suspense fallback={<WidgetSkeleton />}>
                        <TableWidget lastUpdatedTimestamp={response.timestamp} columns={columns} data={tableData} />
                    </Suspense>,
                ]}
            />
        </>
    );
}
