import { getPriceFormatter, statsApiListFetcher } from '@utils';
import BigNumber from 'bignumber.js';
import { headers } from 'next/headers';
import ErrorWidget from 'src/components/widgets/ErrorWidget';
import { Jsonify } from 'src/data/types';

import { MarketResponseItem, MarketSpecsResponseItem, ParamSpecsKind, TickerResponseItem } from '@/types/api';

import ListWidget from '../../widgets/ListWidget';

function SinglePerpetualWidget(
    tickerValue: Jsonify<TickerResponseItem>,
    specsValue: Jsonify<MarketSpecsResponseItem>,
    priceFormatter,
    timestamp,
) {
    const percentChange = new BigNumber(tickerValue ? tickerValue.percentage : 0);
    return (
        <ListWidget
            title={'More Info'}
            lastUpdatedTimestamp={timestamp}
            listItems={[
                {
                    label: 'Open',
                    value: tickerValue ? (
                        `$${priceFormatter.format(new BigNumber(tickerValue.open).toNumber())}`
                    ) : (
                        <span>---</span>
                    ),
                },
                {
                    label: 'High',
                    value: tickerValue ? (
                        `$${priceFormatter.format(new BigNumber(tickerValue.high).toNumber())}`
                    ) : (
                        <span>---</span>
                    ),
                },
                {
                    label: 'Low',
                    value: tickerValue ? (
                        `$${priceFormatter.format(new BigNumber(tickerValue.low).toNumber())}`
                    ) : (
                        <span>---</span>
                    ),
                },
                {
                    label: 'Close',
                    value: tickerValue ? (
                        `$${priceFormatter.format(new BigNumber(tickerValue.close).toNumber())}`
                    ) : (
                        <span>---</span>
                    ),
                },
                {
                    label: 'Change',
                    value: `${percentChange.isNegative() ? '-' : '+'}${priceFormatter.format(
                        percentChange.abs().toNumber(),
                    )}%`,
                },
                {
                    label: 'Volume Weighted Avg Price',
                    value: tickerValue ? (
                        `$${priceFormatter.format(new BigNumber(tickerValue.volumeWeightedAveragePrice).toNumber())}`
                    ) : (
                        <span>---</span>
                    ),
                },
                {
                    label: 'Tick Size',
                    value: `${new BigNumber(specsValue.value.tickSize).toNumber()}`,
                },
                {
                    label: 'Min Order Size',
                    value: `${new BigNumber(specsValue.value.minOrderSize).toNumber()}`,
                },
                {
                    label: 'Max Order Notional',
                    value: `$${priceFormatter.format(new BigNumber(specsValue.value.maxOrderNotional).toNumber())}`,
                },
                {
                    label: 'Max Taker Price Deviation',
                    value: `${specsValue.value.maxTakerPriceDeviation}`,
                },
            ]}
        />
    );
}

function IndexFundPerpetualWidget(
    tickerValue: Jsonify<TickerResponseItem>,
    specsValue: Jsonify<MarketSpecsResponseItem>,
    priceFormatter,
    timestamp,
) {
    const percentChange = new BigNumber(tickerValue ? tickerValue.percentage : 0);
    return (
        <ListWidget
            title={'More Info'}
            lastUpdatedTimestamp={timestamp}
            listItems={[
                {
                    label: 'Open',
                    value: tickerValue ? (
                        `$${priceFormatter.format(new BigNumber(tickerValue.open).toNumber())}`
                    ) : (
                        <span>---</span>
                    ),
                },
                {
                    label: 'High',
                    value: tickerValue ? (
                        `$${priceFormatter.format(new BigNumber(tickerValue.high).toNumber())}`
                    ) : (
                        <span>---</span>
                    ),
                },
                {
                    label: 'Low',
                    value: tickerValue ? (
                        `$${priceFormatter.format(new BigNumber(tickerValue.low).toNumber())}`
                    ) : (
                        <span>---</span>
                    ),
                },
                {
                    label: 'Close',
                    value: tickerValue ? (
                        `$${priceFormatter.format(new BigNumber(tickerValue.close).toNumber())}`
                    ) : (
                        <span>---</span>
                    ),
                },
                {
                    label: 'Change',
                    value: `${percentChange.isNegative() ? '-' : '+'}${priceFormatter.format(
                        percentChange.abs().toNumber(),
                    )}%`,
                },
                {
                    label: 'Volume Weighted Avg Price',
                    value: tickerValue ? (
                        `$${priceFormatter.format(new BigNumber(tickerValue.volumeWeightedAveragePrice).toNumber())}`
                    ) : (
                        <span>---</span>
                    ),
                },
                {
                    label: 'Tick Size',
                    value: `${new BigNumber(specsValue.value.tickSize).toNumber()}`,
                },
                {
                    label: 'Min Order Size',
                    value: `${new BigNumber(specsValue.value.minOrderSize).toNumber()}`,
                },
                {
                    label: 'Max Order Notional',
                    value: `$${priceFormatter.format(new BigNumber(specsValue.value.maxOrderNotional).toNumber())}`,
                },
                {
                    label: 'Max Taker Price Deviation',
                    value: `${specsValue.value.maxTakerPriceDeviation}`,
                },
                {
                    label: 'Rebalance',
                    value: `${specsValue.value.rebalanceInterval}`,
                },
                {
                    label: 'Initial Index Price',
                    value: `${specsValue.value.initialIndexPrice}`,
                },
                {
                    label: 'Allocation',
                    value: `${Object.keys(specsValue.value.allocation)}`,
                },
            ]}
        />
    );
}

export default async function MarketWidget({ symbol }: { symbol: string }) {
    const headersList = headers();
    const host = headersList.get('host') || '';
    const [marketsResponse, tickerResponse, specsResponse] = await Promise.all([
        statsApiListFetcher<MarketResponseItem>(`/markets?symbol=${symbol}`, host),
        statsApiListFetcher<TickerResponseItem>(`/markets/tickers?symbol=${symbol}`, host),
        statsApiListFetcher<MarketSpecsResponseItem>('/specs', host),
    ]);
    if (!marketsResponse.success || !tickerResponse.success || !specsResponse.success) {
        return <ErrorWidget title="Market Details" />;
    }
    const specsResponseValue = specsResponse.value.find((it) => it.name === symbol);
    if (specsResponseValue === undefined) {
        return <ErrorWidget title="Market Details" />;
    }
    const priceFormatter = getPriceFormatter();
    const marketResponseValue = marketsResponse.value[0];
    const tickerResponseValue = tickerResponse.value[0];

    const marketDetailsList = [
        {
            label: 'Market',
            value: marketResponseValue.market,
        },
        {
            label: 'Kind',
            value: +specsResponseValue.kind === 0 ? 'Single Name Perpetual' : 'Index Fund Perpetual',
        },
        {
            label: 'Volume',
            value: `$${priceFormatter.format(new BigNumber(marketResponseValue.volume).toNumber())}`,
        },
        {
            label: 'Price',
            value: `$${priceFormatter.format(new BigNumber(marketResponseValue.price).toNumber())}`,
        },
        {
            label: 'Funding Rate',
            value: new BigNumber(marketResponseValue.fundingRate).decimalPlaces(4).toString(),
        },
        {
            label: 'Open Interest',
            value: `$${priceFormatter.format(
                new BigNumber(marketResponseValue.openInterest)
                    .multipliedBy(new BigNumber(marketResponseValue.price))
                    .toNumber(),
            )}`,
        },
    ];

    return (
        <>
            <ListWidget
                title={'Market Details'}
                lastUpdatedTimestamp={marketsResponse.timestamp}
                listItems={marketDetailsList}
            ></ListWidget>
            {specsResponseValue.value.kind === ParamSpecsKind.SingleNamePerpetual
                ? SinglePerpetualWidget(
                      tickerResponseValue,
                      specsResponseValue,
                      priceFormatter,
                      specsResponse.timestamp,
                  )
                : IndexFundPerpetualWidget(
                      tickerResponseValue,
                      specsResponseValue,
                      priceFormatter,
                      specsResponse.timestamp,
                  )}
        </>
    );
}
