'use client';

import { Column } from 'react-table';
import TableTimeseriesPaginationWidget from 'src/components/widgets/TableTimeseriesPaginationWidget';
import { Jsonify, StatsAPIListResponse } from 'src/data/types';

import { TxLogResponseItem } from '@/types/api';

import { DefaultPagination } from '../../widgets/TablePaginationWidget';
import { parseRecentTransactionsTableData, parseTableData } from './helper';

type DataTableProps<Data> = {
    lastUpdatedTimestamp: number;
    initialData: StatsAPIListResponse<Jsonify<TxLogResponseItem>>;
    columns: Column<Data>[];
    pagination: DefaultPagination;
    dataParser?: (response, extraData?: { [key: string]: string }) => Data[];
};

export default function EpochHistoryTablePaginationWidget<Data>({
    lastUpdatedTimestamp,
    columns,
    initialData,
    pagination,
    dataParser = parseTableData,
}: DataTableProps<Data>) {
    return (
        <TableTimeseriesPaginationWidget
            lastUpdatedTimestamp={lastUpdatedTimestamp}
            columns={columns}
            initialData={initialData}
            pagination={pagination}
            dataParser={dataParser}
        />
    );
}
