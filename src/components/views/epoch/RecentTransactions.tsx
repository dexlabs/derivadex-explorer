'use client';
import { RepeatIcon } from '@chakra-ui/icons';
import { Box, Heading, HStack, IconButton, Link } from '@chakra-ui/react';
import { statsApiListFetcher, statsApiSingleFetcher } from '@utils';
import { useEffect, useState } from 'react';
import BaseWidget from 'src/components/widgets/BaseWidget';
import { StatsAPIListResponse, StatsAPISingleResponse } from 'src/data/types';
import { buildUrl } from 'src/data/utils';

import { TxLogResponseItem } from '@/types/api';

import EpochHistoryTablePaginationWidget from './EpochHistoryTablePaginationWidget';
import { parseRecentTransactionsTableData } from './helper';

export default function RecentTransactionsClientWidget({
    host,
    initialData,
    onChainData,
}: {
    host: string;
    initialData: StatsAPIListResponse<TxLogResponseItem>;
    onChainData: StatsAPISingleResponse<any>;
}) {
    const url = '/tx_logs';
    const params = { order: 'desc', limit: 5 };
    const headerUrl = '/status/exchange';

    const [data, setData] = useState<StatsAPIListResponse<TxLogResponseItem>>(initialData);
    const [statusData, setStatusData] = useState<StatsAPISingleResponse<any>>(onChainData);

    const callList = async () => {
        const response = await statsApiListFetcher<TxLogResponseItem>(buildUrl(url, params), host);
        setData(response);
    };

    const callHeader = async () => {
        const headerResponse = await statsApiSingleFetcher<any>(buildUrl(headerUrl, {}), host);
        console.log(headerResponse);
        setStatusData(headerResponse);
    };

    const columns = [
        {
            Header: 'Age',
            accessor: 'age',
            collapse: false,
        },
        {
            Header: 'Transaction',
            accessor: 'transaction',
            collapse: false,
            isNumeric: false,
        },
        {
            Header: 'Event',
            accessor: 'event',
            collapse: false,
        },
    ];
    return data?.success && data.value.length > 0 ? (
        <BaseWidget>
            <HStack justifyContent="space-between">
                <Heading fontSize="1.3rem">{'Most Recent Transactions'}</Heading>
                <HStack justifyContent="space-between">
                    <Box key={'active'} ml="1.5rem">
                        <b>{'Active Addresses'}</b>
                        {': '}
                        {statusData?.success && statusData.value.activeAddresses}
                    </Box>
                    <Box key={'epoch'} ml="1.5rem">
                        <b>{'Latest Epoch'}</b>
                        {': '}
                        {statusData?.success && data?.success && data.value[0].epochId}
                    </Box>
                    <Box key={'checkpoint'} ml="1.5em">
                        <b>{'On Chain Checkpoint'}</b>
                        {': '}
                        <Link
                            href={
                                statusData?.success &&
                                statusData.value.latestOnChainCheckpoint.latestCheckpointTransactionLink
                            }
                            key={
                                statusData?.success && statusData.value.latestOnChainCheckpoint.latestOnChainCheckpoint
                            }
                            target="_blank"
                        >
                            {statusData?.success && statusData.value.latestOnChainCheckpoint.latestOnChainCheckpoint}
                        </Link>
                    </Box>
                    <IconButton
                        icon={<RepeatIcon />}
                        aria-label="Refresh"
                        colorScheme="gray"
                        size="sm"
                        onClick={() => {
                            callHeader();
                            callList();
                        }}
                    />
                </HStack>
            </HStack>
            <EpochHistoryTablePaginationWidget
                lastUpdatedTimestamp={data?.timestamp}
                columns={columns}
                initialData={data}
                pagination={{ url, host, params, extraData: {} }}
                dataParser={parseRecentTransactionsTableData}
            />
        </BaseWidget>
    ) : (
        <></>
    );
}
