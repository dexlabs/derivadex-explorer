import { formatAge } from '@utils';
import TransactionLink from 'src/components/links/TransactionLink';
import { getTransactionEventType } from 'src/data/transaction/utils';

export const parseTableData = (response, extraData: { [key: string]: string }) => {
    // response will contain all transactions previous to epoch, txOrdinal passed in params
    const filtered = response.value.filter((it) => it.epochId === extraData.epochId);
    return filtered.map((it) => {
        return {
            age: formatAge(new Date(+it.timeStamp).getTime()),
            transaction: <TransactionLink epochId={it.epochId} txOrdinal={it.txOrdinal} />,
            event: getTransactionEventType(it.eventKind),
        };
    });
};

export const parseRecentTransactionsTableData = (response, extraData: { [key: string]: string }) => {
    return response.value.map((it) => {
        return {
            age: formatAge(new Date(+it.timeStamp).getTime()),
            transaction: <TransactionLink epochId={it.epochId} txOrdinal={it.txOrdinal} />,
            event: getTransactionEventType(it.eventKind),
        };
    });
};
