import { statsApiListFetcher } from '@utils';
import { headers } from 'next/headers';
import ErrorWidget from 'src/components/widgets/ErrorWidget';
import { buildUrl } from 'src/data/utils';

import { TxLogResponseItem } from '@/types/api';

import EpochHistoryTablePaginationWidget from './EpochHistoryTablePaginationWidget';

export default async function EpochHistoryWidget({ epochId }: { epochId: string }) {
    const headersList = headers();
    const host = headersList.get('host') || '';
    const url = '/tx_logs';
    const params = { order: 'desc', limit: 5, epoch: +epochId + 1 };
    const response = await statsApiListFetcher<TxLogResponseItem>(buildUrl(url, params), host);
    if (!response.success) {
        return <ErrorWidget title="Epoch History" />;
    }

    const columns = [
        {
            Header: 'Age',
            accessor: 'age',
            collapse: false,
        },
        {
            Header: 'Transaction',
            accessor: 'transaction',
            collapse: false,
            isNumeric: false,
        },
        {
            Header: 'Event',
            accessor: 'event',
            collapse: false,
        },
    ];
    return (
        <EpochHistoryTablePaginationWidget
            lastUpdatedTimestamp={response.timestamp}
            columns={columns}
            initialData={response}
            pagination={{ url, host, params, extraData: { epochId } }}
        />
    );
}
