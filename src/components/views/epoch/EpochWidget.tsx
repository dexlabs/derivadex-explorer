import { getCurrentDateAndTime, statsApiListFetcher } from '@utils';
import { headers } from 'next/headers';
import ClientTooltip from 'src/components/ClientTooltip';
import ErrorWidget from 'src/components/widgets/ErrorWidget';

import { ParamEventTypes, TxLogResponseItem } from '@/types/api';

import ListWidget from '../../widgets/ListWidget';

export default async function EpochWidget({ epochId }: { epochId: string }) {
    const headersList = headers();
    const host = headersList.get('host') || '';

    const [previousEpochResponse, advancedEpochResponse] = await Promise.all([
        statsApiListFetcher<TxLogResponseItem>(`/tx_logs?epoch=${epochId}&order=desc&limit=1`, host),
        statsApiListFetcher<TxLogResponseItem>(`/tx_logs?epoch=${+epochId + 1}&order=desc&limit=4`, host),
    ]);

    if (!advancedEpochResponse.success || !previousEpochResponse.success) {
        return <ErrorWidget title="Epoch Details" />;
    }

    const [advanceEpoch] = advancedEpochResponse.value;
    const [previousEpoch] = previousEpochResponse.value;
    const isSettlementEpoch = advancedEpochResponse.value.find((it) =>
        [ParamEventTypes.PNL_SETTLEMENT, ParamEventTypes.FUNDING, ParamEventTypes.TRADE_MINING].includes(it.eventKind),
    );

    return (
        <>
            <ListWidget
                title={'Epoch Details'}
                lastUpdatedTimestamp={advancedEpochResponse.timestamp}
                listItems={[
                    {
                        label: 'Epoch ID',
                        value: advanceEpoch.epochId,
                    },
                    {
                        label: 'Start Time',
                        value: getCurrentDateAndTime(new Date(+previousEpoch.timeStamp)),
                    },
                    {
                        label: 'End Time',
                        value: getCurrentDateAndTime(new Date(+advanceEpoch.timeStamp)),
                    },
                    {
                        label: 'Type',
                        value:
                            isSettlementEpoch !== undefined ? (
                                <ClientTooltip
                                    label="This epoch contains unrealized PnL settlements, trade mining, funding payments, and regular trading activity."
                                    text="Settlement"
                                />
                            ) : (
                                <ClientTooltip
                                    label="This epoch only contains common trading activity such as post orders, fills, cancels, etc."
                                    text="Regular"
                                />
                            ),
                    },
                ]}
            ></ListWidget>
        </>
    );
}
