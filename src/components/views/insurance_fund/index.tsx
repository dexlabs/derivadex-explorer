import { getCurrentDateAndTime, getPriceFormatter, statsApiListFetcher } from '@utils';
import BigNumber from 'bignumber.js';
import { headers } from 'next/headers';
import TransactionLink from 'src/components/links/TransactionLink';
import ErrorWidget from 'src/components/widgets/ErrorWidget';

import { InsuranceFundResponseItem, ParamInsuranceFundEvent } from '@/types/api';

import ListWidget from '../../widgets/ListWidget';

export default async function InsuranceFundWidget() {
    const headersList = headers();
    const host = headersList.get('host') || '';
    const response = await statsApiListFetcher<InsuranceFundResponseItem>('/insurance_fund?limit=1&order=desc', host);
    if (!response.success) {
        return <ErrorWidget title="Insurance Fund Details" />;
    }
    const priceFormatter = getPriceFormatter();

    return (
        <>
            <ListWidget
                title={'Insurance Fund Details'}
                lastUpdatedTimestamp={response.timestamp}
                listItems={[
                    {
                        label: 'Total Capitalization',
                        value: `$${priceFormatter.format(
                            new BigNumber(response.value[0].totalCapitalization).toNumber(),
                        )}`,
                    },
                    {
                        label: 'Event',
                        value: ParamInsuranceFundEvent[response.value[0].kind],
                    },
                    {
                        label: 'Transaction',
                        value: (
                            <TransactionLink
                                epochId={response.value[0].epochId}
                                txOrdinal={response.value[0].txOrdinal}
                            />
                        ),
                    },
                    {
                        label: 'Timestamp',
                        value: getCurrentDateAndTime(new Date(response.value[0].createdAt)),
                    },
                ]}
            ></ListWidget>
        </>
    );
}
