import { statsApiListFetcher } from '@utils';
import { headers } from 'next/headers';
import ErrorWidget from 'src/components/widgets/ErrorWidget';
import { buildUrl } from 'src/data/utils';

import { InsuranceFundResponseItem } from '@/types/api';

import InsuranceFundTablePaginationWidget from './InsuranceFundTablePaginationWidget';

export default async function InsuranceFundHistoryWidget() {
    const headersList = headers();
    const host = headersList.get('host') || '';
    const url = '/insurance_fund';
    const params = { order: 'desc', limit: 5 };
    const response = await statsApiListFetcher<InsuranceFundResponseItem>(buildUrl(url, params), host);
    if (!response.success) {
        return <ErrorWidget title="Insurance Fund History" />;
    }

    const columns = [
        {
            Header: 'Age',
            accessor: 'age',
            collapse: false,
        },
        {
            Header: 'Total Capitalization',
            accessor: 'capitalization',
            collapse: false,
            isNumeric: true,
        },
        {
            Header: 'Change',
            accessor: 'capitalizationDiff',
            collapse: false,
            isNumeric: true,
        },
        {
            Header: 'Event',
            accessor: 'event',
            collapse: false,
        },
        {
            Header: 'Transaction',
            accessor: 'transaction',
            collapse: false,
            isNumeric: true,
        },
    ];

    return (
        <InsuranceFundTablePaginationWidget
            lastUpdatedTimestamp={response.timestamp}
            columns={columns}
            initialData={response}
            pagination={{ url, host, params }}
        />
    );
}
