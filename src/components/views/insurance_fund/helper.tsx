import { formatAge, getFeeFormatter } from '@utils';
import BigNumber from 'bignumber.js';
import TransactionLink from 'src/components/links/TransactionLink';

import { ParamInsuranceFundEvent } from '@/types/api';

export const parseTableData = (response, extraData: { [key: string]: string }) => {
    const priceFormatter = getFeeFormatter();
    return response.value.map((it, index) => {
        const capitalization = new BigNumber(it.totalCapitalization);
        const previousCapitalization =
            index < response.value.length - 1 ? new BigNumber(response.value[index + 1].totalCapitalization) : 0;
        const capitalizationDiff = capitalization.minus(previousCapitalization);

        return {
            age: formatAge(new Date(it.createdAt).getTime()),
            event: ParamInsuranceFundEvent[it.kind],
            capitalization: `${priceFormatter.format(capitalization.toNumber())} USDC`,
            transaction: <TransactionLink epochId={it.epochId} txOrdinal={it.txOrdinal} />,
            capitalizationDiff:
                index === response.value.length - 1 ? (
                    <span>---</span>
                ) : (
                    `${capitalizationDiff.isNegative() ? '-' : '+'}${priceFormatter.format(
                        capitalizationDiff.abs().toNumber(),
                    )} USDC`
                ),
        };
    });
};
