'use client';

import { Column } from 'react-table';
import TableTimeseriesPaginationWidget from 'src/components/widgets/TableTimeseriesPaginationWidget';
import { Jsonify, StatsAPIListResponse } from 'src/data/types';

import { InsuranceFundResponseItem } from '@/types/api';

import { DefaultPagination } from '../../widgets/TablePaginationWidget';
import { parseTableData } from './helper';

type DataTableProps = {
    lastUpdatedTimestamp: number;
    initialData: StatsAPIListResponse<Jsonify<InsuranceFundResponseItem>>;
    columns: Column<any>[];
    pagination: DefaultPagination;
};

export default function InsuranceFundTablePaginationWidget({
    lastUpdatedTimestamp,
    columns,
    initialData,
    pagination,
}: DataTableProps) {
    return (
        <TableTimeseriesPaginationWidget
            lastUpdatedTimestamp={lastUpdatedTimestamp}
            columns={columns}
            initialData={initialData}
            pagination={pagination}
            dataParser={parseTableData}
        />
    );
}
