import { timeseriesLocalAPIHandler } from '@utils';
import { headers } from 'next/headers';
import ErrorWidget from 'src/components/widgets/ErrorWidget';
import { StrategyHistoryEntity } from 'src/data/strategy/types';
import { buildUrl } from 'src/data/utils';

import StrategyHistoryTablePaginationWidget from './StrategyHistoryTablePaginationWidget';

export default async function StrategyHistoryWidget({ trader, strategyId }: { trader: string; strategyId: string }) {
    const headersList = headers();
    const host = headersList.get('host') || '';
    const url = '/strategy_history';
    const params = { trader, strategyId, limit: 5 };
    const response = await timeseriesLocalAPIHandler<StrategyHistoryEntity>(buildUrl(url, params), host);
    if (!response || !response.success) {
        return <ErrorWidget title="Strategy Updates" />;
    }

    const columns = [
        {
            Header: 'Age',
            accessor: 'age',
            collapse: false,
        },
        {
            Header: 'Event',
            accessor: 'event',
            collapse: false,
        },
        {
            Header: 'Amount',
            accessor: 'amount',
            collapse: false,
            isNumeric: true,
        },
    ];

    return (
        <StrategyHistoryTablePaginationWidget
            lastUpdatedTimestamp={response.timestamp}
            columns={columns}
            initialData={response}
            pagination={{
                url,
                host,
                params,
                extraData: { trader, strategyId },
            }}
        />
    );
}
