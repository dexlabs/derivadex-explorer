import { statsApiListFetcher } from '@utils';
import { headers } from 'next/headers';
import Link from 'next/link';
import ErrorWidget from 'src/components/widgets/ErrorWidget';

import { PositionResponseItem } from '@/types/api';

import ListWidget from '../../widgets/ListWidget';

export default async function PositionsWidget({ trader, strategyId }: { trader: string; strategyId: string }) {
    const headersList = headers();
    const host = headersList.get('host') || '';
    const response = await statsApiListFetcher<PositionResponseItem>(
        `/account/${trader}/strategy/${strategyId}/positions`,
        host,
    );
    if (!response.success) {
        return <ErrorWidget title="Owned Positions" />;
    }

    return (
        <ListWidget
            title="Owned Positions"
            lastUpdatedTimestamp={response.timestamp}
            listItems={response.value.map((row) => ({
                value: (
                    <Link href={`/account/${trader}/strategy/${strategyId}/position/${row.symbol}`}>{row.symbol}</Link>
                ),
            }))}
        ></ListWidget>
    );
}
