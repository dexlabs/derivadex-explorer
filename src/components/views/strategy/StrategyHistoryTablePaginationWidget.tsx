'use client';

import { timeseriesLocalAPIHandler } from '@utils';
import { Column } from 'react-table';
import { DefaultPagination } from 'src/components/widgets/TablePaginationWidget';
import TableTimeseriesPaginationWidget from 'src/components/widgets/TableTimeseriesPaginationWidget';
import { StrategyHistoryEntity } from 'src/data/strategy/types';
import { Jsonify, StatsAPITimeseriesResponse } from 'src/data/types';

import { parseTableData as strategyParseTableData } from './helper';

type DataTableProps = {
    lastUpdatedTimestamp: number;
    initialData: StatsAPITimeseriesResponse<Jsonify<StrategyHistoryEntity>>;
    columns: Column<any>[];
    pagination: DefaultPagination;
};

export default function StrategyHistoryTablePaginationWidget({
    lastUpdatedTimestamp,
    columns,
    initialData,
    pagination,
}: DataTableProps) {
    return (
        <TableTimeseriesPaginationWidget
            lastUpdatedTimestamp={lastUpdatedTimestamp}
            columns={columns}
            initialData={initialData}
            pagination={pagination}
            dataParser={strategyParseTableData}
            fetcher={timeseriesLocalAPIHandler}
        />
    );
}
