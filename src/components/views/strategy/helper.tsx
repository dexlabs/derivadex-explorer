import { formatAge, getPriceFormatter } from '@utils';
import BigNumber from 'bignumber.js';
import TransactionLink from 'src/components/links/TransactionLink';
import { ParamStrategyHistoryEvent } from 'src/data/strategy/types';

export const parseTableData = (response, extraData: { [key: string]: string }) => {
    const priceFormatter = getPriceFormatter();
    return response.value.map((it) => {
        const kind = Object.values(ParamStrategyHistoryEvent)[it.event] || '';
        const amount = new BigNumber(it.amount);
        return {
            age: <div>{formatAge(new Date(it.createdAt).getTime())}</div>,
            event: (
                <div>
                    <div>{kind.toString().replaceAll('_', ' ')}</div>
                    <TransactionLink epochId={it.epochId} txOrdinal={it.txOrdinal} />
                </div>
            ),
            amount: (
                <div>{`${
                    [7, 8].includes(it.event)
                        ? 'TBD'
                        : (amount.isNegative() ? '-' : '+') + priceFormatter.format(amount.abs().toNumber()) + ' USDC'
                }`}</div>
            ),
        };
    });
};
