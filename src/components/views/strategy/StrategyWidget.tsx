import { getPriceFormatter, statsApiListFetcher, statsApiSingleFetcher } from '@utils';
import BigNumber from 'bignumber.js';
import { headers } from 'next/headers';
import TraderLink from 'src/components/links/TraderLink';
import ErrorWidget from 'src/components/widgets/ErrorWidget';
import { calculateLeverage } from 'src/formulas';

import { MarketResponseItem, PositionResponseItem, StrategyResponseItem } from '@/types/api';

import ListWidget from '../../widgets/ListWidget';

export default async function StrategyWidget({ trader, strategyId }: { trader: string; strategyId: string }) {
    const headersList = headers();
    const host = headersList.get('host') || '';
    const [positionResponse, strategyResponse, marketsResponse] = await Promise.all([
        statsApiListFetcher<PositionResponseItem>(`/account/${trader}/strategy/${strategyId}/positions`, host),
        statsApiSingleFetcher<StrategyResponseItem>(`/account/${trader}/strategy/${strategyId}`, host),
        statsApiListFetcher<MarketResponseItem>('/markets', host),
    ]);
    if (!positionResponse.success || !strategyResponse.success || !marketsResponse.success) {
        return <ErrorWidget title="Position Details" />;
    }
    const leverage = calculateLeverage(positionResponse.value, marketsResponse.value, strategyResponse.value);
    const priceFormatter = getPriceFormatter();
    const strategyResponseValue = strategyResponse.value;

    return (
        <ListWidget
            title={'Strategy Details'}
            lastUpdatedTimestamp={strategyResponse.timestamp}
            listItems={[
                {
                    label: 'Account',
                    value: <TraderLink trader={trader} />,
                },
                {
                    label: 'Strategy',
                    value: strategyResponseValue.strategyId,
                },
                {
                    label: 'Leverage',
                    value: `${leverage.toNumber() < 0.01 ? '<0.01' : leverage.toFixed(2)}x`,
                },
                {
                    label: 'Free Collateral',
                    value: priceFormatter.format(new BigNumber(strategyResponseValue.availCollateral).toNumber()),
                },
                {
                    label: 'Frozen Collateral',
                    value: priceFormatter.format(new BigNumber(strategyResponseValue.lockedCollateral).toNumber()),
                },
            ]}
        ></ListWidget>
    );
}
