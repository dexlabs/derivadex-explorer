'use client';

import { timeseriesLocalAPIHandler } from '@utils';
import { Column } from 'react-table';
import TableTimeseriesPaginationWidget from 'src/components/widgets/TableTimeseriesPaginationWidget';
import { Jsonify, StatsAPIListResponse } from 'src/data/types';

import { DdxFeePoolResponseItem } from '@/types/api';

import { DefaultPagination } from '../../widgets/TablePaginationWidget';
import { parseTableData } from './helper';

type DataTableProps = {
    lastUpdatedTimestamp: number;
    initialData: StatsAPIListResponse<Jsonify<DdxFeePoolResponseItem>>;
    columns: Column<any>[];
    pagination: DefaultPagination;
};

export default function DDXFeePoolTablePaginationWidget({
    lastUpdatedTimestamp,
    columns,
    initialData,
    pagination,
}: DataTableProps) {
    return (
        <TableTimeseriesPaginationWidget
            lastUpdatedTimestamp={lastUpdatedTimestamp}
            columns={columns}
            initialData={initialData}
            pagination={pagination}
            dataParser={parseTableData}
            fetcher={timeseriesLocalAPIHandler}
        />
    );
}
