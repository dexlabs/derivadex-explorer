import { getCurrentDateAndTime, getPriceFormatter, statsApiListFetcher } from '@utils';
import BigNumber from 'bignumber.js';
import { headers } from 'next/headers';
import TransactionLink from 'src/components/links/TransactionLink';
import ErrorWidget from 'src/components/widgets/ErrorWidget';

import { DdxFeePoolResponseItem } from '@/types/api';

import ListWidget from '../../widgets/ListWidget';

export default async function DDXFeePoolWidget() {
    const headersList = headers();
    const host = headersList.get('host') || '';
    const response = await statsApiListFetcher<DdxFeePoolResponseItem>('/ddx_fee_pool?limit=1&order=desc', host);
    if (!response.success) {
        return <ErrorWidget title="DDX Fee Pool Details" />;
    }
    const [value] = response.value;
    const priceFormatter = getPriceFormatter();

    return (
        <>
            <ListWidget
                title={'DDX Fee Pool Details'}
                lastUpdatedTimestamp={response.timestamp}
                listItems={[
                    {
                        label: 'Total Capitalization',
                        value: `$${priceFormatter.format(new BigNumber(value.totalCapitalization).toNumber())}`,
                    },
                    {
                        label: 'Transaction',
                        value: (
                            <TransactionLink
                                epochId={value.epochId.toString()}
                                txOrdinal={value.txOrdinal.toString()}
                            />
                        ),
                    },
                    {
                        label: 'Timestamp',
                        value: getCurrentDateAndTime(new Date(value.createdAt)),
                    },
                ]}
            ></ListWidget>
        </>
    );
}
