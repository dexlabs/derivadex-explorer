import { timeseriesLocalAPIHandler } from '@utils';
import { headers } from 'next/headers';
import ErrorWidget from 'src/components/widgets/ErrorWidget';
import { buildUrl } from 'src/data/utils';

import { DdxFeePoolResponseItem } from '@/types/api';

import DDXFeePoolTablePaginationWidget from './DDXFeePoolTablePaginationWidget';

export default async function DDXFeePoolHistoryWidget() {
    const headersList = headers();
    const host = headersList.get('host') || '';
    const url = '/ddx_fee_pool';
    const params = { limit: 5 };
    const response = await timeseriesLocalAPIHandler<DdxFeePoolResponseItem>(buildUrl(url, params), host);
    if (!response || !response.success) {
        return <ErrorWidget title="DDX Fee Pool History" />;
    }

    const columns = [
        {
            Header: 'Age',
            accessor: 'age',
            collapse: false,
        },
        {
            Header: 'Total Capitalization',
            accessor: 'capitalization',
            collapse: false,
            isNumeric: true,
        },
        {
            Header: 'Change',
            accessor: 'capitalizationDiff',
            collapse: false,
            isNumeric: true,
        },
        {
            Header: 'Transaction',
            accessor: 'transaction',
            collapse: false,
            isNumeric: true,
        },
    ];

    return (
        <DDXFeePoolTablePaginationWidget
            lastUpdatedTimestamp={response.timestamp}
            columns={columns}
            initialData={response}
            pagination={{
                url,
                host,
                params,
            }}
        />
    );
}
