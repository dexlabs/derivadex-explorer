import { formatAge, getPriceFormatter } from '@utils';
import BigNumber from 'bignumber.js';
import TransactionLink from 'src/components/links/TransactionLink';

export const parseTableData = (response, extraData: { [key: string]: string }) => {
    const priceFormatter = getPriceFormatter();
    return response.value.map((it, index) => {
        const capitalization = new BigNumber(it.totalCapitalization);
        const previousCapitalization =
            index < response.value.length - 1
                ? response.value[index + 1].totalCapitalization
                : response.nextCapitalizationValue !== '-1'
                ? response.nextCapitalizationValue
                : 0;
        const capitalizationDiff = capitalization.minus(previousCapitalization);

        return {
            age: formatAge(new Date(it.createdAt).getTime()),
            capitalization: `$${priceFormatter.format(capitalization.toNumber())}`,
            transaction: <TransactionLink epochId={it.epochId.toString()} txOrdinal={it.txOrdinal.toString()} />,
            capitalizationDiff: `${capitalizationDiff.isNegative() ? '-' : '+'}$${priceFormatter.format(
                capitalizationDiff.abs().toNumber(),
            )}`,
        };
    });
};
