'use client';

import { Column } from 'react-table';
import { FillsResponseItem } from 'src/data/fill/types';
import { Jsonify, StatsAPIListResponse } from 'src/data/types';

import { DefaultPagination } from '../../widgets/TablePaginationWidget';
import TableTimeseriesPaginationWidget from '../../widgets/TableTimeseriesPaginationWidget';
import { parseTableData as tradesParseTableData } from './helper';

type DataTableProps = {
    lastUpdatedTimestamp: number;
    initialData: StatsAPIListResponse<Jsonify<FillsResponseItem>>;
    columns: Column<any>[];
    pagination: DefaultPagination;
};

export default function TradesTablePaginationWidget({
    lastUpdatedTimestamp,
    columns,
    initialData,
    pagination,
}: DataTableProps) {
    return (
        <TableTimeseriesPaginationWidget
            lastUpdatedTimestamp={lastUpdatedTimestamp}
            columns={columns}
            initialData={initialData}
            pagination={pagination}
            dataParser={tradesParseTableData}
        />
    );
}
