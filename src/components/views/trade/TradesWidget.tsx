import { statsApiSingleFetcher, timeseriesStatsAPIHandler } from '@utils';
import { headers } from 'next/headers';
import ErrorWidget from 'src/components/widgets/ErrorWidget';
import { FillsResponseItem } from 'src/data/fill/types';
import { buildUrl } from 'src/data/utils';

import { StrategyResponseItem } from '@/types/api';

import TradesTablePaginationWidget from './TradesTablePaginationWidget';

export default async function TradesWidget({ trader, strategyId }: { trader: string; strategyId: string }) {
    const headersList = headers();
    const host = headersList.get('host') || '';
    const url = `/account/${trader}/strategy/${strategyId}/order_updates`;
    // Exclude Cancels from this trades view
    const params = { order: 'desc', limit: '5', fillReason: ['0', '1'] };

    const [fillsResponse, strategyResponse] = await Promise.all([
        timeseriesStatsAPIHandler<FillsResponseItem>(buildUrl(url, params), host),
        statsApiSingleFetcher<StrategyResponseItem>(`/account/${trader}/strategy/${strategyId}`, host),
    ]);

    if (!fillsResponse.success || !strategyResponse.success) {
        return <ErrorWidget title="Trades (10 most recent)" />;
    }

    const strategyIdHash = strategyResponse.value.strategyIdHash;

    const columns = [
        {
            Header: 'Age',
            accessor: 'age',
            collapse: false,
            isNumeric: false,
        },
        {
            Header: 'Symbol',
            accessor: 'symbol',
            collapse: false,
        },
        {
            Header: 'Amount',
            accessor: 'amount',
            collapse: false,
            isNumeric: true,
        },
        {
            Header: 'Price',
            accessor: 'price',
            collapse: false,
            isNumeric: true,
        },
        {
            Header: 'Transaction',
            accessor: 'transaction',
            collapse: false,
            isNumeric: true,
        },
        {
            Header: 'Fee',
            accessor: 'fee',
            collapse: false,
            isNumeric: true,
        },
    ];

    return (
        <TradesTablePaginationWidget
            lastUpdatedTimestamp={fillsResponse.timestamp}
            columns={columns}
            initialData={fillsResponse}
            pagination={{
                url,
                host,
                params,
                extraData: { trader, strategyIdHash },
            }}
        />
    );
}
