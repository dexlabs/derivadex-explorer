import { formatAge, getFeeFormatter, getTraderFeeAmountFromFill } from '@utils';
import BigNumber from 'bignumber.js';
import Link from 'next/link';
import TransactionLink from 'src/components/links/TransactionLink';
import { convertItemToFillResponse } from 'src/data/fill/utils';

const getFeeComponent = (it, trader, strategyIdHash, priceFormatter) => {
    const feeAmount = getTraderFeeAmountFromFill(trader, strategyIdHash, convertItemToFillResponse(it));
    if (feeAmount.isZero()) {
        return <span>---</span>;
    } else {
        return `${priceFormatter.format(feeAmount.toNumber())} ${
            new BigNumber(it.takerFeeUSDC).isZero() ? 'DDX' : 'USDC'
        }`;
    }
};

export const parseTableData = (response, extraData: { [key: string]: string }) => {
    const priceFormatter = getFeeFormatter();
    return response.value.map((it) => ({
        symbol: (
            <Link href={`/market/${it.symbol}`}>
                <div>{it.symbol}</div>
            </Link>
        ),
        amount: <div>{it.amount}</div>,
        price: <div>${priceFormatter.format(new BigNumber(it.price).toNumber())}</div>,
        transaction: <TransactionLink epochId={it.epochId} txOrdinal={it.txOrdinal} />,
        fee: (
            <Link href={'/insurance_fund'}>
                <div>{getFeeComponent(it, extraData.trader, extraData.strategyIdHash, priceFormatter)}</div>
            </Link>
        ),
        age: <div>{formatAge(new Date(it.createdAt).getTime())}</div>,
    }));
};
