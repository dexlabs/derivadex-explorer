import { formatAge, getPriceFormatter, statsApiListFetcher } from '@utils';
import BigNumber from 'bignumber.js';
import { headers } from 'next/headers';
import TransactionLink from 'src/components/links/TransactionLink';
import ErrorWidget from 'src/components/widgets/ErrorWidget';
import TableWidget from 'src/components/widgets/TableWidget';

import { MarkPricesResponseItem } from '@/types/api';

export default async function PriceHistoryWidget({ symbol }: { symbol: string }) {
    const headersList = headers();
    const host = headersList.get('host') || '';
    const response = await statsApiListFetcher<MarkPricesResponseItem>(
        `/mark_prices?symbol=${symbol}&order=desc&limit=20`,
        host,
    );
    if (!response.success) {
        return <ErrorWidget title="Price History" />;
    }
    const priceFormatter = getPriceFormatter();

    const columns = [
        {
            Header: 'Age',
            accessor: 'age',
            collapse: false,
        },
        {
            Header: 'Index Price',
            accessor: 'index',
            collapse: false,
            isNumeric: true,
        },
        {
            Header: 'EMA',
            accessor: 'ema',
            collapse: false,
            isNumeric: true,
        },
        {
            Header: 'Mark Price',
            accessor: 'mark',
            collapse: false,
            isNumeric: true,
        },
        {
            Header: 'Transaction',
            accessor: 'transaction',
            collapse: false,
        },
    ];

    const tableData = response.value.map((it, index) => {
        return {
            age: formatAge(new Date(it.createdAt).getTime()),
            index: `$${priceFormatter.format(new BigNumber(it.indexPrice).toNumber())}`,
            ema: `${new BigNumber(it.ema).toString()}`,
            mark: `$${priceFormatter.format(new BigNumber(it.markPrice).toNumber())}`,
            transaction: <TransactionLink epochId={it.epochId} txOrdinal={it.txOrdinal} />,
        };
    });

    return <TableWidget lastUpdatedTimestamp={response.timestamp} columns={columns} data={tableData} />;
}
