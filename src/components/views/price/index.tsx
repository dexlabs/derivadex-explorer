import { getCurrentDateAndTime, getPriceFormatter, statsApiListFetcher } from '@utils';
import BigNumber from 'bignumber.js';
import { headers } from 'next/headers';
import Link from 'next/link';
import TransactionLink from 'src/components/links/TransactionLink';
import ErrorWidget from 'src/components/widgets/ErrorWidget';

import { MarkPricesResponseItem } from '@/types/api';

import ListWidget from '../../widgets/ListWidget';

export default async function PriceWidget({ symbol }: { symbol: string }) {
    const headersList = headers();
    const host = headersList.get('host') || '';
    const response = await statsApiListFetcher<MarkPricesResponseItem>(
        `/mark_prices?symbol=${symbol}&limit=1&order=desc`,
        host,
    );
    if (!response.success) {
        return <ErrorWidget title="Price Details" />;
    }
    const [value] = response.value;
    const priceFormatter = getPriceFormatter();

    return (
        <>
            <ListWidget
                title={'Price Details'}
                lastUpdatedTimestamp={response.timestamp}
                listItems={[
                    {
                        label: 'Index Price',
                        value: `$${priceFormatter.format(new BigNumber(value.indexPrice).toNumber())}`,
                    },
                    {
                        label: 'EMA',
                        value: `${new BigNumber(value.ema).toString()}`,
                    },
                    {
                        label: 'Mark Price',
                        value: `$${priceFormatter.format(new BigNumber(value.markPrice).toNumber())}`,
                    },
                    {
                        label: 'Transaction',
                        value: <TransactionLink epochId={value.epochId} txOrdinal={value.txOrdinal} />,
                    },
                    {
                        label: 'Timestamp',
                        value: getCurrentDateAndTime(new Date(value.createdAt)),
                    },
                ]}
            ></ListWidget>
        </>
    );
}
