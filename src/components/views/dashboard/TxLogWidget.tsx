'use client';

import { ArrowForwardIcon } from '@chakra-ui/icons';
import { Box, Flex } from '@chakra-ui/react';
import Link from 'next/link';
import FeedWidget from 'src/components/widgets/FeedWidget';
import { Jsonify } from 'src/data/types';
import { RealtimeFeed } from 'src/types/realtime_api';

import { ParamEventTypes, TxLogResponseItem } from '../../../types/api';

function renderTransactions(transactions: Jsonify<TxLogResponseItem>[]) {
    return transactions.map((tx) => {
        const indexOf = Object.values(ParamEventTypes).indexOf(tx.eventKind as unknown as ParamEventTypes);
        const key = Object.keys(ParamEventTypes)[indexOf];
        const eventType = key.toString();
        return (
            <Link
                href={`/epoch/${tx.epochId}/txOrdinal/${tx.txOrdinal}`}
                prefetch={false}
                key={`${tx.epochId}_${tx.txOrdinal}`}
            >
                <Flex
                    my="1rem"
                    className="feed-widget-item"
                    py="0.5rem"
                    px="1rem"
                    rounded="md"
                    alignItems="center"
                    justifyContent="space-between"
                >
                    <div style={{ display: 'inline-block', marginRight: '0.5rem' }}>{`Epoch ${tx.epochId}, TxOrdinal ${
                        tx.txOrdinal
                    }: ${eventType.toString().replace('_', ' ')}`}</div>
                    <ArrowForwardIcon />
                </Flex>
            </Link>
        );
    });
}

export default function TxLogWidget({ txLogUrl, websocketUrl }: { txLogUrl: string; websocketUrl: string }) {
    return (
        <FeedWidget
            title={'Most Recent Transactions'}
            initialDataRestEndpointUrl={txLogUrl}
            websocketParams={{ url: websocketUrl, subscriptions: [{ feed: RealtimeFeed.TX_LOG, params: {} }] }}
            dataProcessor={renderTransactions}
        />
    );
}
