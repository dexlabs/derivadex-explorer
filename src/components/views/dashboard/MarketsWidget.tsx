import { getPriceFormatter, statsApiListFetcher } from '@utils';
import BigNumber from 'bignumber.js';
import { headers } from 'next/headers';
import Link from 'next/link';
import ErrorWidget from 'src/components/widgets/ErrorWidget';
import TableWidget from 'src/components/widgets/TableWidget';

import { MarketsAggregationResponseItem } from '@/types/api';

export default async function MarketsWidget() {
    const headersList = headers();
    const host = headersList.get('host') || '';
    const response = await statsApiListFetcher<MarketsAggregationResponseItem>('/markets', host);
    if (!response.success) {
        return <ErrorWidget title="Markets" />;
    }
    const priceFormatter = getPriceFormatter();

    const columns = [
        {
            Header: 'Market',
            accessor: 'market',
            collapse: false,
        },
        {
            Header: 'Mark Price',
            accessor: 'price',
            collapse: false,
            isNumeric: true,
        },
        {
            Header: 'Volume (24h)',
            accessor: 'volume',
            collapse: false,
            isNumeric: true,
        },
        {
            Header: 'Open Interest',
            accessor: 'openInterest',
            collapse: false,
            isNumeric: true,
        },
        {
            Header: 'Funding Rate',
            accessor: 'fundingRate',
            collapse: false,
            isNumeric: true,
        },
    ];

    const tableData = response.value.map((it) => ({
        market: (
            <div key={it.market}>
                <Link href={`/market/${it.market}`}>{it.market}</Link>
            </div>
        ),
        price: <div>${priceFormatter.format(new BigNumber(it.price).toNumber())}</div>,
        volume: <div>${priceFormatter.format(new BigNumber(it.volume).toNumber())}</div>,
        openInterest: (
            <div>
                $
                {priceFormatter.format(new BigNumber(it.openInterest).multipliedBy(new BigNumber(it.price)).toNumber())}
            </div>
        ),
        fundingRate: <div>{new BigNumber(it.fundingRate).decimalPlaces(4).toString()}</div>,
    }));

    return <TableWidget lastUpdatedTimestamp={response.timestamp} columns={columns} data={tableData} />;
}
