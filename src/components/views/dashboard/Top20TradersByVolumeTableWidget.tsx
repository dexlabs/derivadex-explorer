import { getPriceFormatter, statsApiListFetcher } from '@utils';
import BigNumber from 'bignumber.js';
import { headers } from 'next/headers';
import TraderLink from 'src/components/links/TraderLink';
import ErrorWidget from 'src/components/widgets/ErrorWidget';
import TableWidget from 'src/components/widgets/TableWidget';

import { TopTradersResponseItem } from '@/types/api';

export default async function Top20TradersByVolumeTableWidget() {
    const headersList = headers();
    const host = headersList.get('host') || '';
    const response = await statsApiListFetcher<TopTradersResponseItem>('/aggregations/traders?limit=20', host);
    if (!response.success) {
        return <ErrorWidget title="Top 20 Traders (by Volume)" />;
    }
    const priceFormatter = getPriceFormatter();

    const columns = [
        {
            Header: 'Address',
            accessor: 'address',
            collapse: false,
        },
        {
            Header: 'Volume (30d)',
            accessor: 'volume',
            collapse: false,
            isNumeric: true,
        },
        {
            Header: 'Realized Pnl (30d)',
            accessor: 'pnl',
            collapse: false,
            isNumeric: true,
        },
    ];

    const tableData = response.value.map((it) => ({
        address: <TraderLink trader={'0x' + it.trader} />,
        volume: <div>${priceFormatter.format(new BigNumber(it.volume).toNumber())}</div>,
        pnl: <div>${priceFormatter.format(new BigNumber(it.realizedPnl).toNumber())}</div>,
    }));

    return <TableWidget lastUpdatedTimestamp={response.timestamp} columns={columns} data={tableData} />;
}
