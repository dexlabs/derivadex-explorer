import { statsApiListFetcher, statsApiSingleFetcher } from '@utils';
import { headers } from 'next/headers';
import ErrorWidget from 'src/components/widgets/ErrorWidget';
import { buildUrl } from 'src/data/utils';

import { TxLogResponseItem } from '@/types/api';

import RecentTransactionsClientWidget from '../epoch/RecentTransactions';

export default async function RecentTransactionsWidget() {
    const headersList = headers();
    const host = headersList.get('host') || '';
    const url = '/tx_logs';
    const params = { order: 'desc', limit: 5 };
    const headerUrl = '/status/exchange';

    const [transactionsResponse, onChainResponse] = await Promise.all([
        statsApiListFetcher<TxLogResponseItem>(buildUrl(url, params), host),
        statsApiSingleFetcher<any>(buildUrl(headerUrl, {}), host),
    ]);

    if (!transactionsResponse.success || !onChainResponse.success) {
        return <ErrorWidget title="Most Recent Transactions" />;
    }

    return (
        <RecentTransactionsClientWidget host={host} initialData={transactionsResponse} onChainData={onChainResponse} />
    );
}
