'use client';

import { FetcherOptions } from '@utils';
import { Column } from 'react-table';
import { Jsonify, StatsAPIListResponse } from 'src/data/types';

import TablePaginationWidget, { DefaultPagination } from './TablePaginationWidget';

type DataTableProps<Data, T> = {
    lastUpdatedTimestamp: number;
    initialData: StatsAPIListResponse<Jsonify<T>>;
    columns: Column<Data>[];
    pagination: DefaultPagination;
    dataParser: (response, extraData?: { [key: string]: string }) => Data[];
    fetcher?: (path: string, host: string, options?: FetcherOptions) => Promise<StatsAPIListResponse<Jsonify<T>>>;
};

export default function TableOffsetPaginationWidget<Data, T>({
    lastUpdatedTimestamp,
    columns,
    initialData,
    pagination,
    dataParser,
    fetcher,
}: DataTableProps<Data, T>) {
    const getNextPageQueryParams = (
        previousRequest: string,
        previousResultSet: StatsAPIListResponse<Jsonify<T>>,
    ): { [key: string]: string } => {
        const urlSearchParams = new URLSearchParams(previousRequest);
        const offset = urlSearchParams.get('offset') || '0';
        return { offset: (+offset + (pagination.params.limit as number)).toString() };
    };
    return (
        <TablePaginationWidget
            lastUpdatedTimestamp={lastUpdatedTimestamp}
            columns={columns}
            initialData={initialData}
            pagination={pagination}
            dataParser={dataParser}
            getNextPageQueryParams={getNextPageQueryParams}
            fetcher={fetcher}
        />
    );
}
