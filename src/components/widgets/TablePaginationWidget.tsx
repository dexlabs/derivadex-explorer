'use client';

import { Button, Flex, Table, TableContainer, Tbody, Td, Text, Th, Thead, Tr } from '@chakra-ui/react';
import { FetcherOptions, statsApiListFetcher } from '@utils';
import { useEffect, useState } from 'react';
import { Column, useSortBy, useTable } from 'react-table';
import { Jsonify, StatsAPIListResponse } from 'src/data/types';
import { buildUrl } from 'src/data/utils';

import BaseWidget from './BaseWidget';

export type DataTableProps<Data, T> = {
    lastUpdatedTimestamp: number;
    initialData: StatsAPIListResponse<Jsonify<T>>;
    columns: Column<Data>[];
    pagination: DefaultPagination;
    dataParser: (response, extraData?: { [key: string]: string }) => Data[];
    getNextPageQueryParams: (
        previousRequest: string,
        previousResultSet: StatsAPIListResponse<Jsonify<T>>,
    ) => { [key: string]: string };
    fetcher?: (path: string, host: string, options?: FetcherOptions) => Promise<StatsAPIListResponse<Jsonify<T>>>;
};

export type DefaultPagination = {
    url: string;
    host: string;
    params: { [key: string]: string | number | string[] };
    extraData?: { [key: string]: string };
};

export default function TablePaginationWidget<Data, T>({
    lastUpdatedTimestamp,
    columns,
    initialData,
    pagination,
    dataParser,
    getNextPageQueryParams,
    fetcher = statsApiListFetcher,
}: DataTableProps<Data, T>) {
    const [tableData, setTableData] = useState<Data[]>(dataParser(initialData, pagination.extraData));
    const [currentPage, setCurrentPage] = useState<number>(0);
    const [resultsCache, setResultsCache] = useState<StatsAPIListResponse<Jsonify<T>>[]>([initialData]);
    const [previousRequestUrlCache, setPreviousRequestUrlCache] = useState<string[]>([pagination.url]);
    const [isEndPage, setIsEndPage] = useState<boolean>(
        tableData.length < (pagination.params.limit as number) ? true : false,
    );
    const { headerGroups, rows, prepareRow } = useTable(
        {
            columns,
            data: tableData,
        },
        useSortBy,
    );

    useEffect(() => {
        setTableData(dataParser(initialData, pagination.extraData));
        setCurrentPage(0);
        setResultsCache([initialData]);
        setPreviousRequestUrlCache([pagination.url]);
        setIsEndPage(tableData.length < (pagination.params.limit as number) ? true : false);
    }, [initialData]);

    const nextPage = async () => {
        if (resultsCache.length > currentPage + 1) {
            const response = resultsCache[currentPage + 1];
            const rsp = dataParser(response, pagination.extraData);
            setTableData(rsp);
            setCurrentPage(currentPage + 1);
            if (rsp.length < (pagination.params.limit as number)) {
                setIsEndPage(true);
            }
        } else {
            const paginationParams = getNextPageQueryParams(
                previousRequestUrlCache[previousRequestUrlCache.length - 1],
                resultsCache[resultsCache.length - 1],
            );
            const url = buildUrl(pagination.url, pagination.params, paginationParams);
            const response = await fetcher(url, pagination.host);
            if (!response.success) {
                return;
            }
            const rsp = dataParser(response, pagination.extraData);
            setTableData(rsp);
            if (rsp.length < (pagination.params.limit as number)) {
                setIsEndPage(true);
            }
            setResultsCache(resultsCache.concat(response));
            setCurrentPage(currentPage + 1);
            setPreviousRequestUrlCache(previousRequestUrlCache.concat(url));
        }
    };

    const prevPage = async () => {
        const previousResponse = resultsCache[currentPage - 1];
        const rsp = dataParser(previousResponse, pagination.extraData);
        setTableData(rsp);
        setCurrentPage(currentPage - 1);
        setIsEndPage(false);
    };

    return (
        <BaseWidget lastUpdatedTimestamp={lastUpdatedTimestamp}>
            <TableContainer>
                <Table
                    border-collapse="separate"
                    border-spacing={0}
                    whiteSpace="nowrap"
                    colorScheme="table"
                    bgColor="background.200"
                    mb="1rem"
                >
                    <Thead>
                        {headerGroups.map((headerGroup, i) => (
                            <Tr key={i} whiteSpace="nowrap">
                                {headerGroup.headers.map((column, i) => (
                                    <Th
                                        key={i}
                                        isNumeric={column.isNumeric}
                                        color="text.default"
                                        userSelect="none"
                                        p="1rem"
                                        fontSize="1rem"
                                        position="sticky"
                                        borderBottomColor="white"
                                    >
                                        <div>{column.render('Header')}</div>
                                    </Th>
                                ))}
                            </Tr>
                        ))}
                    </Thead>

                    <Tbody>
                        {!rows.length ? (
                            <Tr>
                                <Td p="0.8rem" color="text.default" borderBottom="hidden">
                                    No results to display
                                </Td>
                            </Tr>
                        ) : (
                            <>
                                {rows.map((row: any, index: number) => {
                                    prepareRow(row);
                                    return (
                                        <Tr key={index}>
                                            {row.cells.map((cell: any, index: number) => {
                                                return (
                                                    <Td
                                                        key={index}
                                                        isNumeric={cell.column.isNumeric}
                                                        p="0.8rem"
                                                        borderBottomColor="grey"
                                                    >
                                                        {cell.render('Cell')}
                                                    </Td>
                                                );
                                            })}
                                        </Tr>
                                    );
                                })}
                            </>
                        )}
                    </Tbody>
                </Table>
            </TableContainer>
            <Flex justifyContent="space-between" mb="0.5rem">
                <Button onClick={prevPage} isDisabled={currentPage === 0} size="sm">
                    Previous Page
                </Button>
                <Text mt="0.5rem">{currentPage + 1}</Text>
                <Button onClick={nextPage} isDisabled={isEndPage} size="sm">
                    Next Page
                </Button>
            </Flex>
        </BaseWidget>
    );
}
