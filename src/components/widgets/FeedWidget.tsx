'use client';

import { Box, Flex, Heading, Spinner } from '@chakra-ui/react';
import { statsApiClientsideFetcher } from '@utils';
import { ReactNode, useEffect, useRef, useState } from 'react';
import { Jsonify } from 'src/data/types';
import { RealtimeFeedOutgoingMessageContents, RealtimeFeedSubscription } from 'src/types/realtime_api';
import useSWR from 'swr';

import BaseWidget from './BaseWidget';

export default function FeedWidget<ResponseType>({
    title,
    initialDataRestEndpointUrl,
    websocketParams,
    dataProcessor,
}: {
    title: string;
    initialDataRestEndpointUrl: string;
    websocketParams: { url: string; subscriptions: RealtimeFeedSubscription[] };
    dataProcessor: (data: Jsonify<ResponseType>[]) => ReactNode[];
}) {
    const [items, setItems] = useState<ReactNode[]>([]);
    const { data: initialData, isLoading } = useSWR(
        initialDataRestEndpointUrl,
        statsApiClientsideFetcher<ResponseType>,
    );
    const ws = useRef<null | WebSocket>(null);
    useEffect(() => {
        if (!initialData || !initialData.success) {
            return;
        }
        setItems(dataProcessor(initialData.value));
    }, [initialData]);

    useEffect(() => {
        if (isLoading) {
            return;
        }
        ws.current = new WebSocket(websocketParams.url);
        ws.current.onopen = () => {
            if (!ws.current) {
                return;
            }
            ws.current.send(JSON.stringify({ action: 'subscribe', feeds: websocketParams.subscriptions }));
        };
        ws.current.onclose = () => console.log('ws closed');
        ws.current.onmessage = async (e) => {
            const contents = JSON.parse(e.data).contents as RealtimeFeedOutgoingMessageContents<ResponseType[]>;
            const data = contents.data;
            if (data) {
                setItems((prevItems) => {
                    return [...dataProcessor(data), ...prevItems].slice(0, 20);
                });
            }
        };

        const wsCurrent = ws.current;

        return () => {
            wsCurrent.close();
        };
    }, [isLoading]);

    return (
        <BaseWidget
            title={title}
            lastUpdatedTimestamp={initialData && initialData.success ? initialData.timestamp : null}
        >
            <Box
                mb="0.5rem"
                height="20rem"
                overflow="scroll"
                style={{ WebkitMask: 'linear-gradient(to bottom, black 50%, transparent 100%)' }}
            >
                {isLoading ? (
                    <Flex direction="column" alignItems="center" height="100%" justifyContent="center">
                        <Spinner />
                        <Heading fontSize="0.9rem" mt="0.5rem">
                            Loading feed...
                        </Heading>
                    </Flex>
                ) : (
                    <>{...items}</>
                )}
            </Box>
        </BaseWidget>
    );
}
