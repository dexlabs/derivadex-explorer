'use client';

import { Box, Card, Flex, Heading, Text } from '@chakra-ui/react';
import { ErrorBoundary } from 'react-error-boundary';

import ErrorWidget from './ErrorWidget';

export default function BaseWidget({
    title,
    lastUpdatedTimestamp,
    children,
}: {
    title?: string;
    lastUpdatedTimestamp?: number | null;
    children: React.ReactNode;
}) {
    return (
        <ErrorBoundary fallback={<ErrorWidget title={title} />}>
            <Card p="1rem" flex="1" minWidth="22rem">
                <Flex direction="column" justifyContent="space-between" height="100%">
                    <Box>
                        {title !== undefined ? <Heading fontSize="1.3rem">{title}</Heading> : null}

                        <Box>{children}</Box>
                    </Box>
                    {lastUpdatedTimestamp ? (
                        <Text fontSize="0.8rem">
                            Last Updated:{' '}
                            {lastUpdatedTimestamp === null
                                ? 'Loading ...'
                                : new Date(lastUpdatedTimestamp).toLocaleTimeString()}
                        </Text>
                    ) : (
                        <></>
                    )}
                </Flex>
            </Card>
        </ErrorBoundary>
    );
}
