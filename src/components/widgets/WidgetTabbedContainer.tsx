'use client';

import { Tab, TabList, TabPanel, TabPanels, Tabs } from '@chakra-ui/react';
import { ReactNode } from 'react';

export default function WidgetTabbedContainer({ tabNames, tabPanels }: { tabNames: string[]; tabPanels: ReactNode[] }) {
    return (
        <Tabs variant="soft-rounded">
            <TabList>
                {tabNames.map((tabName) => (
                    <Tab key={tabName}>{tabName}</Tab>
                ))}
            </TabList>
            <TabPanels>
                {tabPanels.map((tabPanel, i) => (
                    <TabPanel key={i} px="0">
                        {tabPanel}
                    </TabPanel>
                ))}
            </TabPanels>
        </Tabs>
    );
}
