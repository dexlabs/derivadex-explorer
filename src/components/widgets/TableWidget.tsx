'use client';

import { Flex, Table, TableContainer, Tbody, Td, Th, Thead, Tr } from '@chakra-ui/react';
import { Column, useSortBy, useTable } from 'react-table';

import BaseWidget from './BaseWidget';

type DataTableProps<Data extends object> = {
    lastUpdatedTimestamp: number;
    data: Data[];
    columns: Column<Data>[];
};

export default function TableWidget<Data extends object>({
    lastUpdatedTimestamp,
    columns,
    data,
}: DataTableProps<Data>) {
    const { headerGroups, rows, prepareRow } = useTable(
        {
            columns,
            data,
        },
        useSortBy,
    );

    return (
        <BaseWidget lastUpdatedTimestamp={lastUpdatedTimestamp}>
            <TableContainer>
                <Table
                    border-collapse="separate"
                    border-spacing={0}
                    whiteSpace="nowrap"
                    colorScheme="table"
                    bgColor="background.200"
                    mb="1rem"
                >
                    <Thead>
                        {headerGroups.map((headerGroup, i) => (
                            <Tr key={i} whiteSpace="nowrap">
                                {headerGroup.headers.map((column, i) => (
                                    <Th
                                        key={i}
                                        isNumeric={column.isNumeric}
                                        color="text.default"
                                        userSelect="none"
                                        p="1rem"
                                        fontSize="1rem"
                                        position="sticky"
                                        borderBottomColor="white"
                                    >
                                        <div>{column.render('Header')}</div>
                                    </Th>
                                ))}
                            </Tr>
                        ))}
                    </Thead>

                    <Tbody>
                        {!rows.length ? (
                            <Tr>
                                <Td p="0.8rem" color="text.default" borderBottom="hidden">
                                    No results to display
                                </Td>
                            </Tr>
                        ) : (
                            <>
                                {rows.map((row: any, index: number) => {
                                    prepareRow(row);
                                    return (
                                        <Tr key={index}>
                                            {row.cells.map((cell: any, index: number) => {
                                                return (
                                                    <Td
                                                        key={index}
                                                        isNumeric={cell.column.isNumeric}
                                                        p="0.8rem"
                                                        borderBottomColor="grey"
                                                    >
                                                        {cell.render('Cell')}
                                                    </Td>
                                                );
                                            })}
                                        </Tr>
                                    );
                                })}
                            </>
                        )}
                    </Tbody>
                </Table>
            </TableContainer>
        </BaseWidget>
    );
}
