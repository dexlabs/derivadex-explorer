'use client';

import { Box, Flex, Heading, Spinner } from '@chakra-ui/react';

import BaseWidget from './BaseWidget';

export default function WidgetSkeleton({ title }: { title?: string }) {
    return (
        <BaseWidget title={title} lastUpdatedTimestamp={null}>
            <Box mb="0.5rem" height="20rem">
                <Flex direction="column" alignItems="center" height="100%" justifyContent="center">
                    <Spinner />
                    <Heading fontSize="0.9rem" mt="0.5rem">
                        Loading data...
                    </Heading>
                </Flex>
            </Box>
        </BaseWidget>
    );
}
