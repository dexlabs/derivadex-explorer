'use client';

import { WarningIcon } from '@chakra-ui/icons';
import { Card, Flex, Heading } from '@chakra-ui/react';

export default function ErrorWidget({ title }: { title?: string }) {
    return (
        <Card p="1rem" flex="1">
            {title !== undefined ? <Heading fontSize="1.3rem">{title}</Heading> : null}
            <Flex m="1rem" direction="column" alignItems="center" height="100%" justifyContent="center">
                <WarningIcon w={8} h={8} color="red.500" />
                <Heading fontSize="0.9rem" mt="0.5rem">
                    An unexpected error occured.
                </Heading>
            </Flex>
        </Card>
    );
}
