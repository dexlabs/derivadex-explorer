'use client';

import { Flex } from '@chakra-ui/react';

export default function WidgetFlexContainer({ children }: { children?: React.ReactNode }) {
    return (
        <Flex mt="2rem" mb="3rem" display="flex" direction="row" gap="1rem" flexWrap="wrap">
            {children}
        </Flex>
    );
}
