'use client';

import { FetcherOptions } from '@utils';
import { Column } from 'react-table';
import { Jsonify, StatsAPIListResponse, StatsAPISuccessTimeseriesResponse } from 'src/data/types';

import TablePaginationWidget, { DefaultPagination } from './TablePaginationWidget';

type DataTableProps<Data, T> = {
    lastUpdatedTimestamp: number;
    initialData: StatsAPIListResponse<Jsonify<T>>;
    columns: Column<Data>[];
    pagination: DefaultPagination;
    dataParser: (response, extraData?: { [key: string]: string }) => Data[];
    fetcher?: (path: string, host: string, options?: FetcherOptions) => Promise<StatsAPIListResponse<Jsonify<T>>>;
};

export default function TableTimeseriesPaginationWidget<Data, T>({
    lastUpdatedTimestamp,
    columns,
    initialData,
    pagination,
    dataParser,
    fetcher,
}: DataTableProps<Data, T>) {
    const getNextPageQueryParams = (
        previousRequest: string,
        previousResultSet: StatsAPIListResponse<Jsonify<T>>,
    ): { [key: string]: string } => {
        const timeSeriesResponse = previousResultSet as StatsAPISuccessTimeseriesResponse<Jsonify<T>>;
        return {
            epoch: timeSeriesResponse.nextEpoch?.toString() || '',
            txOrdinal: timeSeriesResponse.nextTxOrdinal?.toString() || '',
            ordinal: timeSeriesResponse.nextOrdinal?.toString() || '',
        };
    };
    return (
        <TablePaginationWidget
            lastUpdatedTimestamp={lastUpdatedTimestamp}
            columns={columns}
            initialData={initialData}
            pagination={pagination}
            dataParser={dataParser}
            getNextPageQueryParams={getNextPageQueryParams}
            fetcher={fetcher}
        />
    );
}
