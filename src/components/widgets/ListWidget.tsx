'use client';

import '../../app/styles.css';

import { Box, Flex, SimpleGrid } from '@chakra-ui/react';

import BaseWidget from './BaseWidget';

export default function ListWidget({
    title,
    lastUpdatedTimestamp,
    listItems,
}: {
    title: string;
    lastUpdatedTimestamp: number;
    listItems: Array<{ label?: string; value: React.ReactNode }>;
}) {
    return (
        <BaseWidget title={title} lastUpdatedTimestamp={lastUpdatedTimestamp}>
            <Box
                overflowX="scroll"
                sx={{
                    '::-webkit-scrollbar': {
                        display: 'none',
                    },
                    scrollbarWidth: 'none',
                }}
                height="100%"
                my="1.5rem"
                px="1.5rem"
            >
                <Box fontSize="1.1rem" className="widget">
                    <SimpleGrid columns={2} spacing="2rem">
                        {listItems.map((item, index) => (
                            <Box key={index}>
                                {item.label ? (
                                    <>
                                        <b>{item.label}</b>
                                        {': '}
                                    </>
                                ) : null}
                                {item.value}
                            </Box>
                        ))}
                    </SimpleGrid>
                </Box>
            </Box>
        </BaseWidget>
    );
}
