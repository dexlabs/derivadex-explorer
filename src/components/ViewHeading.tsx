'use client';

import { CheckIcon, LinkIcon } from '@chakra-ui/icons';
import { Heading, IconButton, Tooltip, useClipboard } from '@chakra-ui/react';
import { useEffect } from 'react';

export default function ViewHeading({ title }: { title: string }) {
    const { onCopy, hasCopied, setValue, value } = useClipboard('', 2000);
    useEffect(() => {
        if (value) {
            onCopy();
        }
    }, [value]);

    return (
        <Heading mb="1rem" size="lg">
            {title}
            <Tooltip label={hasCopied ? 'Copied!' : 'Copy page URL'} placement="right">
                <IconButton
                    onClick={() => {
                        setValue(location.href);
                    }}
                    variant="link"
                    aria-label="Copy page URL"
                    icon={
                        hasCopied ? (
                            <CheckIcon boxSize="4" color="gray.500" />
                        ) : (
                            <LinkIcon boxSize="4" color="gray.500" />
                        )
                    }
                    ml="0.5rem"
                />
            </Tooltip>
        </Heading>
    );
}
