import { statsApiListFetcher } from '@utils';
import { NextRequest, NextResponse } from 'next/server';
import { Jsonify } from 'src/data/types';

import { EpochResponseItem, MarketsAggregationResponseItem, TradersResponseItem, TxLogResponseItem } from '@/types/api';

export const config = {
    runtime: 'edge',
};

async function fetchTraders(host: string) {
    const traders: Jsonify<TradersResponseItem>[] = [];
    let offset: undefined | number;

    while (true) {
        const tradersResponse = await statsApiListFetcher<TradersResponseItem>(
            `/traders?limit=1000${offset ? `&offset=${offset}` : ''}`,
            host,
        );
        if (!tradersResponse.success) {
            return null;
        }
        traders.push(...tradersResponse.value);
        if (tradersResponse.value.length !== 1000) {
            break;
        }
        offset = (offset ?? 0) + 1000;
    }
    return traders;
}

export default async function handler(request: NextRequest) {
    const nextUrl = request.nextUrl;

    const { searchParams } = new URL(nextUrl);
    const query = searchParams.get('query');

    if (!query || !query.length) {
        // Missing query param return all the traders and markets
        const [traders, markets] = await Promise.all([
            fetchTraders(nextUrl.host),
            statsApiListFetcher<MarketsAggregationResponseItem>('/markets', nextUrl.host),
        ]);

        if (!traders || !markets.success) {
            return new NextResponse(null, { status: 500 });
        }

        return new Response(
            JSON.stringify({
                traders: traders.map((t) => t.trader.replace('0x00', '0x')),
                markets: markets.value.map((m) => m.market),
                epochs: [],
                transactions: [],
            }),
            {
                status: 200,
                headers: {
                    'content-type': 'application/json',
                    'cache-control': 'public, s-maxage=1200, stale-while-revalidate=600',
                },
            },
        );
    } else if (/^[0-9]*$/i.test(query)) {
        // Fetch the epoch and transactions if query is only numeric
        const [epochs, transactions] = await Promise.all([
            statsApiListFetcher<EpochResponseItem>(`/epochs?offset=${query}&limit=1`, nextUrl.host),
            statsApiListFetcher<TxLogResponseItem>(`/tx_logs?epoch=${+query - 1}&limit=10`, nextUrl.host),
        ]);

        if (!epochs.success || !transactions.success) {
            return new NextResponse(null, { status: 500 });
        }

        return new Response(
            JSON.stringify({
                traders: [],
                markets: [],
                epochs: epochs.value.map((e) => e.epochId),
                transactions: transactions.value.map((t) => ({
                    epoch: t.epochId,
                    txOrdinal: t.txOrdinal,
                    eventKind: t.eventKind,
                })),
            }),
            {
                status: 200,
                headers: {
                    'content-type': 'application/json',
                    'cache-control': 'public, s-maxage=1200, stale-while-revalidate=600',
                },
            },
        );
    } else if (/^[0-9]*,([0-9]*)$/.test(query)) {
        const [epoch, txOrdinal] = query.split(',');
        const transactions = await statsApiListFetcher<TxLogResponseItem>(
            txOrdinal === ''
                ? `/tx_logs?epoch=${+epoch - 1}&limit=10`
                : +txOrdinal === 0
                ? `/tx_logs?epoch=${epoch}&txOrdinal=${+txOrdinal + 1}&order=desc&limit=1`
                : `/tx_logs?epoch=${epoch}&txOrdinal=${+txOrdinal - 1}&limit=10`,
            nextUrl.host,
        );

        if (!transactions.success) {
            return new NextResponse(null, { status: 500 });
        }

        return new Response(
            JSON.stringify({
                traders: [],
                markets: [],
                epochs: [],
                transactions: transactions.value.map((t) => ({
                    epoch: t.epochId,
                    txOrdinal: t.txOrdinal,
                    eventKind: t.eventKind,
                })),
            }),
            {
                status: 200,
                headers: {
                    'content-type': 'application/json',
                    'cache-control': 'public, s-maxage=1200, stale-while-revalidate=600',
                },
            },
        );
    } else {
        return new NextResponse(null, { status: 400 });
    }
}
