import { NextRequest } from 'next/server';
import { statsApiDdxPoolHistoryFetcher } from 'src/data/ddx/fetchers';

export const config = {
    runtime: 'edge',
};

export default async function handler(request: NextRequest) {
    const nextUrl = request.nextUrl;

    const { searchParams } = new URL(nextUrl);
    const limit = searchParams.get('limit');
    const epoch = searchParams.get('epoch');
    const txOrdinal = searchParams.get('txOrdinal');

    // Defaults limit to 50
    const response =
        epoch && txOrdinal
            ? await statsApiDdxPoolHistoryFetcher(limit ? +limit : 50, nextUrl.host, { epoch, txOrdinal })
            : await statsApiDdxPoolHistoryFetcher(limit ? +limit : 50, nextUrl.host);
    if (!response) {
        return new Response(
            JSON.stringify({
                success: false,
                errorMsg: 'Failure',
            }),
            {
                status: 400,
                headers: {
                    'content-type': 'application/json',
                    'cache-control': 'public, s-maxage=1200, stale-while-revalidate=600',
                },
            },
        );
    }
    return new Response(
        JSON.stringify({
            timestamp: Math.floor(Date.now() / 1000),
            success: true,
            nextEpoch: response.value.length > 0 ? response.value[response.value.length - 1].epochId : 0,
            nextTxOrdinal: response.value.length > 0 ? response.value[response.value.length - 1].txOrdinal : 0,
            previousCapitalizationValue: response.previousCapitalizationValue,
            value: response.value,
        }),
        {
            status: 200,
            headers: {
                'content-type': 'application/json',
                'cache-control': 'public, s-maxage=1200, stale-while-revalidate=600',
            },
        },
    );
}
