import { extractUrlSearchParam, getDateAndTime, statsApiListFetcher } from '@utils';
import { ImageResponse } from '@vercel/og';
import { NextRequest } from 'next/server';
import { getTransactionEventType } from 'src/data/transaction/utils';

import { TxLogResponseItem } from '@/types/api';

export const config = {
    runtime: 'edge',
};

const font = fetch(new URL('../../../../../../../../public/assets/TT_Firs_Neue_Regular.ttf', import.meta.url)).then(
    (res) => res.arrayBuffer(),
);
export default async function handler(request: NextRequest) {
    try {
        const fontData = await font;
        const nextUrl = request.nextUrl;
        const epochId = extractUrlSearchParam(nextUrl.href, 'epoch');
        const txOrdinal = extractUrlSearchParam(nextUrl.href, 'txOrdinal');
        if (epochId === null || txOrdinal === null) {
            throw new Error('Transaction fetch did not succeeed');
        }
        const response = await statsApiListFetcher<TxLogResponseItem>(
            `/tx_logs?epoch=${epochId}&txOrdinal=${+txOrdinal + 1}&limit=1&order=desc`,
            nextUrl.host,
        );
        if (!response.success) {
            throw new Error('Transaction fetch did not succeeed');
        }
        const transaction = response.value[0];
        const eventKindString = getTransactionEventType(transaction.eventKind);
        return new ImageResponse(
            (
                <div
                    style={{
                        backgroundImage: `url(${nextUrl.origin}/assets/social_card_default_bg.png)`,
                        height: '100%',
                        width: '100%',
                        display: 'flex',
                        textAlign: 'center',
                        alignItems: 'flex-start',
                        justifyContent: 'center',
                        flexDirection: 'column',
                        flexWrap: 'nowrap',
                        fontSize: 40,
                        fontStyle: 'normal',
                        fontFamily: 'SF Pro Display',
                        color: '#EBEBED',
                        letterSpacing: '-0.025em',
                        lineHeight: 2,
                        whiteSpace: 'pre-wrap',
                    }}
                >
                    <div style={{ display: 'flex', flexDirection: 'row', alignItems: 'center', gap: '-80px' }}>
                        <div
                            style={{
                                display: 'flex',
                                flexDirection: 'column',
                                marginTop: 0,
                                padding: '0 80px',
                            }}
                        >
                            <div style={{ display: 'flex', flexDirection: 'row' }}>
                                <img
                                    height="33.82px"
                                    src={`${nextUrl.origin}/icons/card_logo.png`}
                                    style={{ margin: '40px 5px' }}
                                    width="96px"
                                />
                                <div
                                    style={{
                                        display: 'flex',
                                        fontSize: '48px',
                                        fontFamily: 'TT Firs Neue',
                                        top: '4px',
                                    }}
                                >
                                    {'DerivaDEX Explorer'}
                                </div>
                            </div>
                            <div style={{ display: 'flex' }}>EPOCH: {epochId}</div>
                            <div style={{ display: 'flex' }}>TX ORDINAL: {txOrdinal}</div>
                            <div style={{ display: 'flex' }}>EVENT: {eventKindString.replace(/_/g, ' ')}</div>
                            <div style={{ display: 'flex', fontSize: 20 }}>
                                {getDateAndTime(new Date(+transaction.timeStamp))}
                            </div>
                        </div>
                    </div>
                </div>
            ),
            {
                width: 1200,
                height: 630,
                fonts: [
                    {
                        name: 'TT_Firs_Neue_Regular',
                        data: fontData,
                        style: 'normal',
                    },
                ],
                headers: { 'cache-control': 'public, max-age=60, s-maxage=60' },
            },
        );
    } catch (e: any) {
        return new Response('Failed to generate the image', {
            status: 500,
        });
    }
}
