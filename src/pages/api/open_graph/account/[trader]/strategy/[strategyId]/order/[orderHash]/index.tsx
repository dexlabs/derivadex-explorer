import {
    extractUrlSearchParam,
    getDateAndTime,
    getPriceFormatter,
    sideEnumToString,
    statsApiListFetcher,
} from '@utils';
import { ImageResponse } from '@vercel/og';
import BigNumber from 'bignumber.js';
import { NextRequest } from 'next/server';
import { OrderIntentResponseItem } from 'src/data/order/types';
import { orderTypeEnumToString } from 'src/data/order/utils';

export const config = {
    runtime: 'edge',
};

const font = fetch(
    new URL('../../../../../../../../../../public/assets/TT_Firs_Neue_Regular.ttf', import.meta.url),
).then((res) => res.arrayBuffer());

export default async function handler(request: NextRequest) {
    try {
        const fontData = await font;
        const nextUrl = request.nextUrl;
        const orderHash = extractUrlSearchParam(nextUrl.href, 'order') ?? '';
        const orderResponse = await statsApiListFetcher<OrderIntentResponseItem>(
            `/order_intents?orderHash=${orderHash}`,
            nextUrl.host,
        );
        if (!orderResponse.success) {
            throw new Error('Order Intent fetch did not succeeed');
        }
        const symbol = orderResponse.value[0].symbol;
        const side = orderResponse.value[0].side;
        const priceFormatter = getPriceFormatter();
        const price = new BigNumber(orderResponse.value[0].price);
        const amount = orderResponse.value[0].amount;
        const sideString = sideEnumToString(side);
        const orderTypeString = orderTypeEnumToString(orderResponse.value[0].orderType);
        const priceString = `$${priceFormatter.format(price.toNumber())}`;
        const created_at = orderResponse.value[0].createdAt;
        return new ImageResponse(
            (
                <div
                    style={{
                        backgroundImage: `url(${nextUrl.origin}/assets/social_card_default_bg.png)`,
                        height: '100%',
                        width: '100%',
                        display: 'flex',
                        textAlign: 'center',
                        alignItems: 'flex-start',
                        justifyContent: 'center',
                        flexDirection: 'column',
                        flexWrap: 'nowrap',
                        fontSize: 40,
                        fontStyle: 'normal',
                        fontFamily: 'SF Pro Display',
                        color: '#EBEBED',
                        letterSpacing: '-0.025em',
                        lineHeight: 2,
                        whiteSpace: 'pre-wrap',
                    }}
                >
                    <div style={{ display: 'flex', flexDirection: 'row', alignItems: 'center', gap: '-80px' }}>
                        <div
                            style={{
                                display: 'flex',
                                flexDirection: 'column',
                                marginTop: 0,
                                padding: '0 80px',
                            }}
                        >
                            <div style={{ display: 'flex', flexDirection: 'row' }}>
                                <img
                                    height="33.82px"
                                    src={`${nextUrl.origin}/icons/card_logo.png`}
                                    style={{ margin: '40px 5px' }}
                                    width="96px"
                                />
                                <div
                                    style={{
                                        display: 'flex',
                                        fontSize: '48px',
                                        fontFamily: 'TT Firs Neue',
                                        top: '4px',
                                    }}
                                >
                                    {'DerivaDEX Explorer'}
                                </div>
                            </div>

                            <div style={{ display: 'flex' }}>{symbol}</div>
                            <div
                                style={{ display: 'flex', color: `${sideString === 'Long' ? '#54C438' : '#FF5935'}` }}
                            >{`${orderTypeString.toLocaleUpperCase()} ${sideString === 'Long' ? 'BUY' : 'SELL'}`}</div>
                            <div style={{ display: 'flex' }}>PRICE: {priceString}</div>
                            <div style={{ display: 'flex' }}>AMOUNT: {amount.toString()}</div>
                            <div style={{ display: 'flex', fontSize: 20 }}>
                                CREATED: {getDateAndTime(new Date(created_at))}
                            </div>
                        </div>
                        <div
                            style={{
                                display: 'flex',
                                fontSize: 96,
                                fontWeight: 400,
                                fontFamily: 'SF Pro Rounded',
                                top: '30px',
                            }}
                        >
                            {`${sideString === 'Long' ? 'BUY' : 'SELL'} ORDER`}
                        </div>
                    </div>
                </div>
            ),
            {
                width: 1200,
                height: 630,
                fonts: [
                    {
                        name: 'TT_Firs_Neue_Regular',
                        data: fontData,
                        style: 'normal',
                    },
                ],
                headers: { 'cache-control': 'public, max-age=60, s-maxage=60' },
            },
        );
    } catch (e: any) {
        return new Response('Failed to generate the image', {
            status: 500,
        });
    }
}
