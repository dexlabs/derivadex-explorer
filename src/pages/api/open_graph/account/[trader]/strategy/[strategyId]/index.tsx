import {
    extractUrlSearchParam,
    getDateAndTime,
    getPriceFormatter,
    statsApiListFetcher,
    statsApiSingleFetcher,
} from '@utils';
import { ImageResponse } from '@vercel/og';
import BigNumber from 'bignumber.js';
import { NextRequest } from 'next/server';
import {
    calculateLeverage,
    computePercentageGainOrLossToString,
    computeStrategyCostBasis,
    computeStrategyNotionalValue,
} from 'src/formulas';

import { MarketResponseItem, PositionResponseItem, StrategyResponseItem } from '@/types/api';

export const config = {
    runtime: 'edge',
};

const font = fetch(new URL('../../../../../../../../public/assets/TT_Firs_Neue_Regular.ttf', import.meta.url)).then(
    (res) => res.arrayBuffer(),
);

export default async function handler(request: NextRequest) {
    try {
        const fontData = await font;
        const nextUrl = request.nextUrl;
        const trader = extractUrlSearchParam(nextUrl.href, 'account');
        const strategyId = extractUrlSearchParam(nextUrl.href, 'strategy');
        const [positionsResponse, strategyResponse, marketsResponse] = await Promise.all([
            statsApiListFetcher<PositionResponseItem>(
                `/account/${trader}/strategy/${strategyId}/positions`,
                nextUrl.host,
            ),
            statsApiSingleFetcher<StrategyResponseItem>(`/account/${trader}/strategy/${strategyId}`, nextUrl.host),
            statsApiListFetcher<MarketResponseItem>('/markets', nextUrl.host),
        ]);
        if (!positionsResponse.success || !strategyResponse.success || !marketsResponse.success) {
            throw new Error('Strategy fetch did not succeeed');
        }
        const priceFormatter = getPriceFormatter();
        const leverage = calculateLeverage(positionsResponse.value, marketsResponse.value, strategyResponse.value);
        const freeCollateral = new BigNumber(strategyResponse.value.availCollateral);
        const frozenCollateral = new BigNumber(strategyResponse.value.lockedCollateral);
        const totalCollateralString = `$${priceFormatter.format(freeCollateral.plus(frozenCollateral).toNumber())}`;
        const strategyCostBasis = computeStrategyCostBasis(positionsResponse.value);
        const strategyNotionalValue = computeStrategyNotionalValue(positionsResponse.value, marketsResponse.value);
        const isPnlPositive = strategyCostBasis.isLessThanOrEqualTo(strategyNotionalValue);
        const strategyPnlString = computePercentageGainOrLossToString(strategyCostBasis, strategyNotionalValue);
        const unrealizedPnlString = `$${priceFormatter.format(
            strategyNotionalValue.minus(strategyCostBasis).abs().toNumber(),
        )}`;
        return new ImageResponse(
            (
                <div
                    style={{
                        backgroundImage: `url(${nextUrl.origin}/assets/pnl_${
                            isPnlPositive ? 'positive' : 'negative'
                        }_bg.png)`,
                        height: '100%',
                        width: '100%',
                        display: 'flex',
                        textAlign: 'center',
                        alignItems: 'flex-start',
                        justifyContent: 'center',
                        flexDirection: 'column',
                        flexWrap: 'nowrap',
                        fontSize: 40,
                        fontStyle: 'normal',
                        fontFamily: 'SF Pro Display',
                        color: '#EBEBED',
                        letterSpacing: '-0.025em',
                        lineHeight: 2,
                        whiteSpace: 'pre-wrap',
                    }}
                >
                    <div style={{ display: 'flex', flexDirection: 'row', alignItems: 'center', gap: '-80px' }}>
                        <div
                            style={{
                                display: 'flex',
                                flexDirection: 'column',
                                marginTop: 0,
                                padding: '0 80px',
                            }}
                        >
                            <div style={{ display: 'flex', flexDirection: 'row' }}>
                                <img
                                    height="33.82px"
                                    src={`${nextUrl.origin}/icons/card_logo.png`}
                                    style={{ margin: '40px 5px' }}
                                    width="96px"
                                />
                                <div
                                    style={{
                                        display: 'flex',
                                        fontSize: '48px',
                                        fontFamily: 'TT Firs Neue',
                                        top: '4px',
                                    }}
                                >
                                    {'DerivaDEX Explorer'}
                                </div>
                            </div>
                            <div style={{ display: 'flex' }}>STRATEGY ID: {strategyId}</div>
                            <div style={{ display: 'flex' }}>
                                LEVERAGE: {`${leverage.toNumber() < 0.01 ? '<0.01' : leverage.toFixed(2)}x`}
                            </div>
                            <div style={{ display: 'flex', color: `${isPnlPositive ? '#54C438' : '#FF5935'}` }}>
                                UNREALIZED PNL: {isPnlPositive ? unrealizedPnlString : `-${unrealizedPnlString}`}
                            </div>
                            <div style={{ display: 'flex' }}>COLLATERAL: {totalCollateralString}</div>
                            <div style={{ display: 'flex', fontSize: 20 }}>{getDateAndTime()}</div>
                        </div>
                        <div
                            style={{
                                display: 'flex',
                                fontSize: 80,
                                fontWeight: 400,
                                fontFamily: 'SF Pro Rounded',
                                top: '30px',
                                left: '50px',
                            }}
                        >
                            {strategyPnlString}
                        </div>

                        {
                            <img
                                src={`${nextUrl.origin}/assets/pnl_${
                                    isPnlPositive ? 'positive' : 'negative'
                                }_arrow.png`}
                                style={{
                                    display: 'flex',
                                    boxSizing: 'border-box',
                                    position: 'absolute',
                                    width: '100px',
                                    height: `${isPnlPositive ? '340px' : '432px'}`,
                                    left: '1040px',
                                    top: `${isPnlPositive ? '235px' : '-60px'}`,
                                }}
                            />
                        }
                        {isPnlPositive && (
                            <div
                                style={{
                                    display: 'flex',
                                    position: 'absolute',
                                    boxSizing: 'border-box',
                                    height: '630px',
                                    width: '1200px',
                                }}
                            >
                                <img
                                    src={`${nextUrl.origin}/assets/star.png`}
                                    height="164.78px"
                                    width="164.78px"
                                    style={{
                                        display: 'flex',
                                        boxSizing: 'border-box',
                                        position: 'absolute',
                                        width: '164.78px',
                                        height: '164.78px',
                                        left: '840px',
                                        top: '20px',
                                    }}
                                />
                                <img
                                    src={`${nextUrl.origin}/assets/star.png`}
                                    height="69.56px"
                                    width="69.56px"
                                    style={{
                                        display: 'flex',
                                        boxSizing: 'border-box',
                                        position: 'absolute',
                                        width: '69.56px',
                                        height: '69.56px',
                                        left: '920px',
                                        top: '200px',
                                    }}
                                />
                                <img
                                    src={`${nextUrl.origin}/assets/star.png`}
                                    height="97.52px"
                                    width="97.52px"
                                    style={{
                                        display: 'flex',
                                        boxSizing: 'border-box',
                                        position: 'absolute',
                                        width: '97.52px',
                                        height: '97.52px',
                                        left: '1020px',
                                        top: '150px',
                                    }}
                                />
                                <img
                                    src={`${nextUrl.origin}/assets/star.png`}
                                    height="97.52px"
                                    width="97.52px"
                                    style={{
                                        display: 'flex',
                                        boxSizing: 'border-box',
                                        position: 'absolute',
                                        width: '97.52px',
                                        height: '97.52px',
                                        left: '970px',
                                        top: '540px',
                                    }}
                                />
                                <img
                                    src={`${nextUrl.origin}/assets/star.png`}
                                    height="164.78px"
                                    width="164.78px"
                                    style={{
                                        display: 'flex',
                                        boxSizing: 'border-box',
                                        position: 'absolute',
                                        width: '74px',
                                        height: '74px',
                                        left: '1100px',
                                        top: '430px',
                                    }}
                                />
                            </div>
                        )}
                    </div>
                </div>
            ),
            {
                width: 1200,
                height: 630,
                fonts: [
                    {
                        name: 'TT_Firs_Neue_Regular',
                        data: fontData,
                        style: 'normal',
                    },
                ],
                headers: { 'cache-control': 'public, max-age=60, s-maxage=60' },
            },
        );
    } catch (e: any) {
        return new Response('Failed to generate the image', {
            status: 500,
        });
    }
}
