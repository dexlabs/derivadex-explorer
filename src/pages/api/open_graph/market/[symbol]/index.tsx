import { extractUrlSearchParam, getDateAndTime, getPriceFormatter, statsApiListFetcher } from '@utils';
import { ImageResponse } from '@vercel/og';
import BigNumber from 'bignumber.js';
import { NextRequest } from 'next/server';

import { MarketResponseItem } from '@/types/api';

export const config = {
    runtime: 'edge',
};

const font = fetch(new URL('../../../../../../public/assets/TT_Firs_Neue_Regular.ttf', import.meta.url)).then((res) =>
    res.arrayBuffer(),
);

export default async function handler(request: NextRequest) {
    try {
        const fontData = await font;
        const nextUrl = request.nextUrl;
        const symbol = extractUrlSearchParam(nextUrl.href, 'market');
        const marketResponse = await statsApiListFetcher<MarketResponseItem>(`/markets?symbol=${symbol}`, nextUrl.host);
        if (symbol === null || !marketResponse.success) {
            throw new Error('Market fetch did not succeeed');
        }
        const priceFormatter = getPriceFormatter();
        const volume = `$${Intl.NumberFormat('en', { notation: 'compact' }).format(
            new BigNumber(marketResponse.value[0]['volume']).toNumber(),
        )}`;
        const price = new BigNumber(marketResponse.value[0]['price']);
        const priceString = `$${priceFormatter.format(price.toNumber())}`;
        const fundingRate = new BigNumber(marketResponse.value[0]['fundingRate']);
        const openInterest = `$${Intl.NumberFormat('en', { notation: 'compact' }).format(
            new BigNumber(marketResponse.value[0]['openInterest']).multipliedBy(price).toNumber(),
        )}`;
        return new ImageResponse(
            (
                <div
                    style={{
                        backgroundImage: `url(${nextUrl.origin}/assets/social_card_default_bg.png)`,
                        height: '100%',
                        width: '100%',
                        display: 'flex',
                        textAlign: 'center',
                        alignItems: 'flex-start',
                        justifyContent: 'center',
                        flexDirection: 'column',
                        flexWrap: 'nowrap',
                        fontSize: 40,
                        fontStyle: 'normal',
                        fontFamily: 'SF Pro Display',
                        color: '#EBEBED',
                        letterSpacing: '-0.025em',
                        lineHeight: 2,
                        whiteSpace: 'pre-wrap',
                    }}
                >
                    <div style={{ display: 'flex', flexDirection: 'row', alignItems: 'center', gap: '-80px' }}>
                        <div
                            style={{
                                display: 'flex',
                                flexDirection: 'column',
                                marginTop: 0,
                                padding: '0 80px',
                            }}
                        >
                            <div style={{ display: 'flex', flexDirection: 'row' }}>
                                <img
                                    height="33.82px"
                                    src={`${nextUrl.origin}/icons/card_logo.png`}
                                    style={{ margin: '40px 5px' }}
                                    width="96px"
                                />
                                <div
                                    style={{
                                        display: 'flex',
                                        fontSize: '48px',
                                        fontFamily: 'TT Firs Neue',
                                        top: '4px',
                                    }}
                                >
                                    {'DerivaDEX Explorer'}
                                </div>
                            </div>
                            <div style={{ display: 'flex' }}>{symbol} MARKET</div>
                            <div style={{ display: 'flex' }}>VOLUME (24H): {volume}</div>
                            <div style={{ display: 'flex' }}>OPEN INTEREST: {openInterest}</div>
                            <div style={{ display: 'flex' }}>
                                FUNDING RATE: {fundingRate.decimalPlaces(4).toNumber()}
                            </div>
                            <div style={{ display: 'flex', fontSize: 20 }}>{getDateAndTime()}</div>
                        </div>
                        <div
                            style={{
                                display: 'flex',
                                fontSize: 96,
                                fontWeight: 400,
                                fontFamily: 'SF Pro Rounded',
                                top: '30px',
                            }}
                        >
                            {priceString}
                        </div>
                    </div>
                </div>
            ),
            {
                width: 1200,
                height: 630,
                fonts: [
                    {
                        name: 'TT_Firs_Neue_Regular',
                        data: fontData,
                        style: 'normal',
                    },
                ],
                headers: { 'cache-control': 'public, max-age=60, s-maxage=60' },
            },
        );
    } catch (e: any) {
        return new Response('Failed to generate the image', {
            status: 500,
        });
    }
}
