import { NextRequest } from 'next/server';
import { statsApiOrderHistoryFetcher } from 'src/data/order/fetchers';

export const config = {
    runtime: 'edge',
};

export default async function handler(request: NextRequest) {
    const nextUrl = request.nextUrl;

    const { searchParams } = new URL(nextUrl);
    const orderHash = searchParams.get('orderHash');
    const lookbackAmount = searchParams.get('lookbackAmount') || undefined;
    const limit = searchParams.get('limit');
    const epoch = searchParams.get('epoch');
    const txOrdinal = searchParams.get('txOrdinal');
    const ordinal = searchParams.get('ordinal');

    if (!orderHash) {
        return new Response(
            JSON.stringify({
                success: false,
                errorMsg: 'Failure',
            }),
            {
                status: 400,
                headers: {
                    'content-type': 'application/json',
                    'cache-control': 'public, s-maxage=1200, stale-while-revalidate=600',
                },
            },
        );
    } else {
        // Defaults limit to 50
        const response =
            epoch && txOrdinal && ordinal
                ? await statsApiOrderHistoryFetcher(orderHash, limit ? +limit : 50, nextUrl.host, lookbackAmount, {
                      epoch,
                      txOrdinal,
                      ordinal,
                  })
                : await statsApiOrderHistoryFetcher(orderHash, limit ? +limit : 50, nextUrl.host);
        if (!response) {
            return new Response(
                JSON.stringify({
                    success: false,
                    errorMsg: 'Failure',
                }),
                {
                    status: 400,
                    headers: {
                        'content-type': 'application/json',
                        'cache-control': 'public, s-maxage=1200, stale-while-revalidate=600',
                    },
                },
            );
        }
        return new Response(
            JSON.stringify({
                timestamp: Math.floor(Date.now() / 1000),
                success: true,
                nextEpoch: response.length > 0 ? response[response.length - 1].epochId : 0,
                nextTxOrdinal: response.length > 0 ? response[response.length - 1].txOrdinal : 0,
                nextOrdinal: response.length > 0 ? response[response.length - 1].ordinal : 0,
                value: response,
            }),
            {
                status: 200,
                headers: {
                    'content-type': 'application/json',
                    'cache-control': 'public, s-maxage=1200, stale-while-revalidate=600',
                },
            },
        );
    }
}
