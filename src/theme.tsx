import { extendTheme } from '@chakra-ui/react';

const fonts = { mono: "'Menlo', monospace" };

const breakpoints = {
    sm: '40em',
    md: '52em',
    lg: '64em',
    xl: '80em',
};

const theme = extendTheme({
    semanticTokens: {
        colors: {
            dark: {
                default: '#0d1126',
            },
            text: {
                default: '#16161D',
                _dark: '#ade3b8',
            },
            heroGradientStart: {
                default: '#7928CA',
                _dark: '#e3a7f9',
            },
            heroGradientEnd: {
                default: '#FF0080',
                _dark: '#fbec8f',
            },
        },
        radii: {
            button: '12px',
        },
    },
    colors: {
        black: '#16161D',
        table: {
            50: '#070A22',
            100: '#070A22',
            200: '#070A22',
            300: '#070A22',
            400: '#070A22',
            500: '#070A22',
            600: '#070A22',
            700: '#070A22',
            800: '#070A22',
            900: '#070A22',
        },
    },
    fonts,
    breakpoints,
});

export default theme;
